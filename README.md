xPace --- $x$-Particle arrangement code

xPace is a C++/Python library that generates random packings of
arbritrary shaped particles (sphere, circle, polyhedron).


# Dependencies

A list of dependencies for xPace:

- a //C++ compiler// 14 and above 
- //Eigen// (included as submodule)
- //boost//
- //pybind11// (included as submodule)
- //python 3+// with //numpy//
- //FFTW3//
- //googletest// (for tests)
- //Doxygen// (for documentation)


# Build

You should first clone the git submodules that are dependencies to xPace (eigen, pybind11):

	git submodule update --init --recursive

The build system uses CMake to construct the library.

      mkdir build
      ccmake ..
      make
Make sure to set `XPACE_USE_PYTHON` ON to generate pybind bindings.

# Optional build options
The build system has various options that can be activated based on user preference.

- `XPACE_POLYHEDRON` Set On if polyhedral shape of particle is required, requires QHULLCPP library if set ON
- `XPACE_USE_MPI` Set On if parallelization option is required, requires HDF5 library to write dump files in parallel
- `XPACE_USE_VTK` Set on if dumper output is required in VTK format, requires VTK library

By default all the above options are set to OFF. In defaults setting,
xPace generates spherical or circular space particles and dumps them
in .txt format.

# Examples

Python example simulations can be found in `examples/python`.

- `generate_paacking.py` generates 2D packing of volume fraction$\aprrox$0.4

# Visualization

.txt files are generated from the above scripts. To visualize the packings, `Paraview` can be used. A script `paraview_script.py` is also provided to generate the paraview visualization. Run the following command to visualize a .txt file
`
python paraview_script.py --filename *.txt
`
