#include "take_place_algorithm.hh"
#include "system_evolution.hh"
#include "sphere_factory.hh"
#include "creation_fuller_algorithm.hh"
#include "hybrid_algorithm.hh"
#include "surface_generator.hh"

#include "chrono"
#include <stdlib.h>

using namespace xpace;

const UInt dim = 3;

int main(int argc, char * argv[]) {

  auto t_start = std::chrono::high_resolution_clock::now();

  Real density = 0.2;
 
  Vector origin(    -50.0, -50.0, -50.0);
  Vector dimensions( 100.0, 100.0, 100.0);
  std::vector<Real> diameters  = {16.0, 8.0, 4.0, 2.0};

  SphereFactory::getInstance();
    
  CreationFullerAlgorithm fuller;
  fuller.setOrigin(origin);
  fuller.setDiameters(diameters);
  fuller.setDimensions(dimensions);
  fuller.setPackingDensity(density);
  fuller.create();
   
  TakePlaceAlgorithm algorithm;
  algorithm.setGamma(0.01);
  algorithm.setNsteps(20000);
  algorithm.setDimensions(dimensions);
  algorithm.setCoordination(0.0);
  algorithm.setConvergenceCriteria(ConvergenceCriteria::contact);

  
  UInt dump_freq = 1;
  std::string dumper_name = "geometric-65";
  SystemEvolution system;
  system.setDumper(dumper_name, dump_freq);
  system.create(fuller);

  system.evolve(algorithm);

  auto t_end = std::chrono::high_resolution_clock::now();
  std::cout << std::chrono::duration<double>(t_end-t_start).count()
	    << " s\n";
  //system.dump();

  std::cout << fuller.size() << std::endl; 

  /*Dumper dumper;
  dumper.setBaseNameToDumper("spheres-geometric");
  dumper.dumpToTxt(particles);*/

  SurfaceGenerator generator;
  generator.setSize(1024);
  generator.setDegree(0.0);
  generator.setDirection(_z);
  generator.setHeight(0.0);
  generator.setDimensions(dimensions);

  auto surface = generator.build(system);
  generator.dump("surface");
  
  return 0;
}
