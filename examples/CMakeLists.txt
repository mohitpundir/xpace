add_executable (hybrid    hybrid_algorithm.cc)
add_executable (geometric geometric_algorithm.cc)
add_executable (dynamic   dynamic_algorithm.cc)

target_include_directories (hybrid    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${QHULL_INCLUDE_PATH})
target_include_directories (geometric PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} /home/mohit/Documents/Code/xPace/src/libqhullcpp/)
target_include_directories (dynamic   PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${QHULL_INCLUDE_PATH})

target_link_libraries(hybrid    xPace ${QHULL_LIBRARY} ${QHULLCPP_LIBRARY})
target_link_libraries(geometric xPace ${QHULL_LIBRARY} ${QHULLCPP_LIBRARY})
target_link_libraries(dynamic xPace ${QHULL_LIBRARY} ${QHULLCPP_LIBRARY})
