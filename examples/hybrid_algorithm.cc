#include "system_evolution.hh"
#include "compute_lennard_jones.hh"
#include "compute_boundary.hh"
#include "sphere_factory.hh"
#include "creation_fuller_algorithm.hh"
#include "hybrid_algorithm.hh"

#include "chrono"
#include <stdlib.h>
#include <iostream>
#include <sstream>

using namespace xpace;

/* -------------------------------------------------------------------------- */
void particleFunctor(std::shared_ptr<CreationAlgorithm> algo) {
   Vector dimensions( 100.0, 100.0, 100.0);

   algo->setDimensions(dimensions);

   Real   radius = 8.0;
   Vector center(0., 0., 0.);
   Vector force(0., 0., 0.);
   Vector velocity(0., 0., 0.);

   algo->addShape(radius, center, force, velocity);

   Vector center2(4., 0., 0.);
   algo->addShape(radius, center2, force, velocity);

   Vector center3(4., 4., 0.);
   algo->addShape(radius, center3, force, velocity);

   Vector center4(4., 4., 4.);
   algo->addShape(radius, center4, force, velocity);

}


int main(int argc, char * argv[]) {

  Real density = 0.6;
 
  Vector origin(    -50.0, -50.0, -50.0);
  Vector dimensions( 100.0, 100.0, 100.0);
  std::vector<Real> diameters  = {16.0, 8.0, 4.0, 2.0};

  SphereFactory::getInstance();
  
  CreationFullerAlgorithm fuller;
  fuller.setOrigin(origin);
  fuller.setDiameters(diameters);
  fuller.setDimensions(dimensions);
  fuller.setPackingDensity(density);
  fuller.create();
 
  HybridAlgorithm algorithm;
  algorithm.setTimeStep(0.01);
  algorithm.setNsteps(1000);
  algorithm.setDimensions(dimensions);
  algorithm.setCoordination(0.0);
  algorithm.setConvergenceCriteria(ConvergenceCriteria::contact);

  Real gamma = 1.0;
  auto lennard = std::make_shared<ComputeLennardJones>(gamma, dimensions, _hard_aperiodic);
  algorithm.addCompute(lennard);

  auto boundary = std::make_shared<ComputeBoundary>(dimensions, _hard_aperiodic);
  algorithm.addBoundary(boundary);
  
  UInt dump_freq = 1;
  std::string dumper_name = "hybrid";
  SystemEvolution system;
  system.setDumper(dumper_name, dump_freq);
  system.create(fuller);

  auto t_start = std::chrono::high_resolution_clock::now();

  system.evolve(algorithm);
  
  auto t_end = std::chrono::high_resolution_clock::now();
  std::cout << std::chrono::duration<double>(t_end-t_start).count()
  << " s\n";  

  UInt start = 0;
  UInt nb_particles = system.size();
  auto cn = system.coordinationNumber(start, nb_particles);
  auto max_overlap = system.maximumOverlap(start, nb_particles);
  auto avg_overlap = system.averageOverlap(start, nb_particles);

  std::cerr << cn << std::endl;
  std::cerr << max_overlap << std::endl;
  std::cerr << avg_overlap << std::endl;
  
  return 0;
}
