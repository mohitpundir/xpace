#include "system_evolution.hh"
#include "compute_lennard_jones.hh"
#include "compute_boundary.hh"
#include "sphere_factory.hh"
#include "creation_fuller_algorithm.hh"
#include "dynamic_algorithm.hh"

#include "chrono"
#include <stdlib.h>
#include <iostream>
#include <sstream>

using namespace xpace;

/* -------------------------------------------------------------------------- */
Real pFuller(Real & d, Real & dmax) {
  auto ratio = d/dmax;
  return std::pow(ratio, 0.5);
}

/* -------------------------------------------------------------------------- */
Real getVolume(Real & dia) {
  auto radius = dia/2.;
  auto r3 = radius * radius * radius;
  return (4./3) * M_PI * r3;
}

/* -------------------------------------------------------------------------- */
std::vector<Real> getRadii(Real & pk, std::vector<Real> & dbins, Vector & dimensions) {

  Real v_exact, v_approx, v_rem = 0.;
  
  Real volume = 1.0;
  for (auto v : dimensions) {
    volume *= v;
  }

  auto dmax = *(std::max_element(dbins.begin(), dbins.end()));
  auto dmin = *(std::min_element(dbins.begin(), dbins.end()));

  std::random_device random_seed;
  std::mt19937 gen(random_seed());  
  std::vector<Real> radii;
  
  for ( UInt i = 0; i < dbins.size() - 1; ++i ) {
    auto vp = (pFuller(dbins[i], dmax) - pFuller(dbins[i + 1], dmax)) /
              (pFuller(dmax, dmax) - pFuller(dmin, dmax)) * pk * volume;
    v_exact += vp;
    v_rem += vp;

    std::uniform_real_distribution<> distribution(dbins[i], dbins[i+1]);
   
    while (v_rem >= getVolume(dbins[i+1])) {
      auto d_agg = distribution(gen);
      radii.push_back(d_agg/2.0);
      vp = getVolume(d_agg);
      v_rem -= vp;
    }

    return radii;
  }

}

/* -------------------------------------------------------------------------- */
void createTwoParticles(std::shared_ptr<CreationAlgorithm> algo) {
   Vector dimensions( 100.0, 100.0, 100.0);

   algo->setDimensions(dimensions);

   Real   radius = 8.0;
   Vector center(0., 0., 0.);
   Vector force(0., 0., 0.);
   Vector velocity(0., 0., 0.);

   algo->addShape(radius, center, force, velocity);

   Vector center2(4., 0., 0.);
   algo->addShape(radius, center2, force, velocity);

   Vector center3(4., 4., 0.);
   algo->addShape(radius, center3, force, velocity);

   Vector center4(4., 4., 4.);
   algo->addShape(radius, center4, force, velocity);

}

/* -------------------------------------------------------------------------- */
int main(int argc, char * argv[]) {

  auto t_start = std::chrono::high_resolution_clock::now();

  Real density = 0.2;
 
  Vector origin(    -50.0, -50.0, -50.0);
  Vector dimensions( 100.0, 100.0, 100.0);
  std::vector<Real> diameters  = {16.0, 8.0, 4.0, 2.0};

  SphereFactory::getInstance();
  
  CreationFullerAlgorithm fuller;
  fuller.setOrigin(origin);
  fuller.setDiameters(diameters);
  fuller.setDimensions(dimensions);
  fuller.setPackingDensity(density);
  fuller.create();

  std::vector<UInt> layers;
  layers.push_back(fuller.size());
  
  DynamicAlgorithm algorithm;
  algorithm.setTimeStep(0.01);
  algorithm.setNsteps(1000);
  algorithm.setLayers(layers);
  algorithm.setKillVelocity(false);
  algorithm.setDimensions(dimensions);
  algorithm.setCoordination(0.0);
  algorithm.setConvergenceCriteria(ConvergenceCriteria::contact);
  algorithm.setDampingFreq(1);
  algorithm.setDampingRatio(1e-3);
  
  Real gamma = 1.0;
  auto lennard = std::make_shared<ComputeLennardJones>(gamma, dimensions, _periodic);
  algorithm.addCompute(lennard);

  auto boundary = std::make_shared<ComputeBoundary>(dimensions, _periodic);
  algorithm.addBoundary(boundary);
  
  UInt dump_freq = 1;
  std::string dumper_name = "dynamic";

  SystemEvolution system;
  system.setDumper(dumper_name, dump_freq);
  system.create(fuller);
  system.evolve(algorithm);
  
  auto t_end = std::chrono::high_resolution_clock::now();
  std::cout << std::chrono::duration<double>(t_end-t_start).count()
  << " s\n";  

  return 0;
}
