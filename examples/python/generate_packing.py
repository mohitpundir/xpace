import numpy as np
import xpace as xp

# Parameters considered for generating digital concrete sample
parameters = {'spatial_dimension' : 2,
        'dimensions' : [50., 20., 0.],
        'packing' : 0.7,
        'origin' : [-25., -10., 0.],
        'diameters' : [8., 4., 2., 1.]}

# Shape of the aggregate
xp.CircleFactory.getInstance()

# Fuller algorithm to generate aggregate distribution
creation = xp.CreationFullerAlgorithm()
creation.spatial_dimension = parameters['spatial_dimension']
creation.origin = parameters['origin']
creation.dimensions = parameters['dimensions']
creation.diameters = parameters['diameters']
creation.packing_density = parameters['packing']
creation.create()

# Take and place algorithm to place aagregates
evolution = xp.TakePlaceAlgorithm()
evolution.spatial_dimension = parameters['spatial_dimension']
evolution.gamma = 0.01
evolution.nsteps = 50000
evolution.dimensions = parameters['dimensions']
evolution.convergence = xp.contact
evolution.coordination = 0

sample = xp.SystemEvolution()
sample.create(creation)
sample.evolve(evolution)

# dumper to create txt file of particles 
sample.setDumper('packing-2d')
sample.dump()
