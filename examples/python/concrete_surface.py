#!/usr/bin/env python
import xpace as xp
import matplotlib.pyplot as plt


def create_packing(**parameters):
    xp.SphereFactory.getInstance()

    creation = xp.CreationFullerAlgorithm()
    creation.origin = parameters['origin']
    creation.dimensions = parameters['dimensions']
    creation.diameters = parameters['diameters']
    creation.packing_density = parameters['packing']
    creation.create()

    evolution = xp.TakePlaceAlgorithm()
    evolution.gamma = 0.01
    evolution.nsteps = 50000
    evolution.dimensions = parameters['dimensions']
    evolution.convergence = xp.contact
    evolution.coordination = 0

    sample = xp.SystemEvolution()
    sample.create(creation)
    sample.evolve(evolution)
    return sample


def create_surface(cut_height, **parameters):
    if 'sample' in parameters:
        sample = parameters['sample']
    else:
        sample = create_packing(**parameters)

    generator = xp.SurfaceGenerator()
    generator.size = parameters['grid_size']
    generator.height = cut_height
    generator.degree = 0.0
    generator.direction = xp.z
    generator.dimensions = parameters['dimensions']
    surface = generator.build(sample)
    surface -= generator.height

    return surface, sample


if __name__ == '__main__':
    data = {'grid_size' : 1024,
            'length' : 50.0,
            'dimensions' : [50., 50., 50.],
            'packing' : 0.2,
            'origin' : [-25., -25., -25.],
            'diameters' : [8., 6., 4., 2., 1., 0.3]}

    surface, sample = create_surface(cut_height=0.0, **data)
    plt.figure()
    plt.imshow(surface)
    plt.colorbar()
    plt.show()
