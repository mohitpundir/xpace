import argparse
import os


def renderParaview(args):
    filename = args.filename
    # import the simple module from the paraview
    temp_path = 'paraview.py'
    with open(temp_path, 'w') as f:
        f.write('''\
from paraview.simple import *

# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'CSV Reader'
spherestxt = CSVReader(FileName=[\'''' + str(filename) + '''\'])

# Properties modified on spherestxt
spherestxt.FieldDelimiterCharacters = ' '

# Create a new 'SpreadSheet View'
spreadSheetView1 = CreateView('SpreadSheetView')
spreadSheetView1.ColumnToSort = ''
spreadSheetView1.BlockSize = 1024L
# uncomment following to set a specific view size
# spreadSheetView1.ViewSize = [400, 400]

# get layout
layout1 = GetLayout()

# place view in the layout
layout1.AssignView(2, spreadSheetView1)

# show data in view
spherestxtDisplay = Show(spherestxt, spreadSheetView1)

# trace defaults for the display properties.
spherestxtDisplay.FieldAssociation = 'Row Data'

# update the view to ensure updated data information
spreadSheetView1.Update()

# create a new 'Table To Points'
tableToPoints1 = TableToPoints(Input=spherestxt)
tableToPoints1.XColumn = '#x'
tableToPoints1.YColumn = '#x'
tableToPoints1.ZColumn = '#x'

# Properties modified on tableToPoints1
tableToPoints1.YColumn = 'y'
tableToPoints1.ZColumn = 'z'
tableToPoints1.KeepAllDataArrays = 1
                               
# show data in view
tableToPoints1Display = Show(tableToPoints1, spreadSheetView1)

# hide data in view
Hide(spherestxt, spreadSheetView1)

# update the view to ensure updated data information
spreadSheetView1.Update()

# destroy spreadSheetView1
Delete(spreadSheetView1)
del spreadSheetView1

# close an empty frame
layout1.Collapse(2)

# find view
renderView1 = FindViewOrCreate('RenderView1', viewtype='RenderView')
# uncomment following to set a specific view size
# renderView1.ViewSize = [1216, 957]

# set active view
SetActiveView(renderView1)

# create a new 'Glyph'
glyph1 = Glyph(Input=tableToPoints1,
               GlyphType='Arrow')
glyph1.Scalars = ['POINTS', '#x']
glyph1.Vectors = ['POINTS', 'None']
glyph1.ScaleFactor = 9.6751
glyph1.GlyphTransform = 'Transform2'

# Properties modified on glyph1
glyph1.GlyphType = 'Sphere'
glyph1.Scalars = ['POINTS', 'radius']
glyph1.ScaleMode = 'scalar'
glyph1.ScaleFactor = 2.0
glyph1.GlyphMode = 'All Points'

# Properties modified on glyph1.GlyphType
glyph1.GlyphType.ThetaResolution = 25
glyph1.GlyphType.PhiResolution = 25

# show data in view
glyph1Display = Show(glyph1, renderView1)

# get color transfer function/color map for 'radius'
radiusLUT = GetColorTransferFunction('radius')

# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', 'radius']
glyph1Display.LookupTable = radiusLUT
glyph1Display.OSPRayScaleArray = 'radius'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = '#x'
glyph1Display.ScaleFactor = 9.998897933959961
glyph1Display.SelectScaleArray = 'radius'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'radius'
glyph1Display.GaussianRadius = 0.4999448966979981
glyph1Display.SetScaleArray = ['POINTS', 'radius']
glyph1Display.ScaleTransferFunction = 'PiecewiseFunction'
glyph1Display.OpacityArray = ['POINTS', 'radius']
glyph1Display.OpacityTransferFunction = 'PiecewiseFunction'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.SelectionCellLabelFontFile = ''
glyph1Display.SelectionPointLabelFontFile = ''
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
glyph1Display.ScaleTransferFunction.Points = [1.5001699924468994, 0.0, 0.5, 0.0, 2.999799966812134, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
glyph1Display.OpacityTransferFunction.Points = [1.5001699924468994, 0.0, 0.5, 0.0, 2.999799966812134, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
glyph1Display.DataAxesGrid.XTitleFontFile = ''
glyph1Display.DataAxesGrid.YTitleFontFile = ''
glyph1Display.DataAxesGrid.ZTitleFontFile = ''
glyph1Display.DataAxesGrid.XLabelFontFile = ''
glyph1Display.DataAxesGrid.YLabelFontFile = ''
glyph1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
glyph1Display.PolarAxes.PolarAxisTitleFontFile = ''
glyph1Display.PolarAxes.PolarAxisLabelFontFile = ''
glyph1Display.PolarAxes.LastRadialAxisTextFontFile = ''
glyph1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# reset view to fit data
renderView1.ResetCamera()

# show color bar/color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# update the view to ensure updated data information
renderView1.Update()

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 0

# Properties modified on renderView1
renderView1.OrientationAxesVisibility = 0

# Properties modified on renderView1
renderView1.OrientationAxesInteractivity = 0

# get color legend/bar for radiusLUT in view renderView1
radiusLUTColorBar = GetScalarBar(radiusLUT, renderView1)

# change scalar bar placement
radiusLUTColorBar.Position = [0.6079276315789474, 0.038808777429467076]
radiusLUTColorBar.ScalarBarLength = 0.15000000000000024

# change scalar bar placement
radiusLUTColorBar.Position = [0.6572697368421053, 0.04612330198537093]

# current camera placement for renderView1
renderView1.CameraPosition = [-156.32414161116844, 95.49543370095773, 207.82308756846533]
renderView1.CameraFocalPoint = [30.619129233076947, -30.478975647538828, -39.38366698875945]
renderView1.CameraViewUp = [0.22373759128167311, 0.926391752780054, -0.3028858046006355]
renderView1.CameraParallelScale = 86.58977416644358

# save screenshot
# SaveScreenshot('/home/mohit/Documents/repositories/studies/packing-algorithm/paraview/packing.png', renderView1, ImageResolution=[1216, 957])

#### saving camera placements for all active views

# current camera placement for renderView1
renderView1.CameraPosition = [-156.32414161116844, 95.49543370095773, 207.82308756846533]
renderView1.CameraFocalPoint = [30.619129233076947, -30.478975647538828, -39.38366698875945]
renderView1.CameraViewUp = [0.22373759128167311, 0.926391752780054, -0.3028858046006355]
renderView1.CameraParallelScale = 86.58977416644358''')


def main():
    parser = argparse.ArgumentParser(description='xPace code')
    parser.add_argument('--filename', type=str,
                        help='filename', default='periodic.txt')
    args = parser.parse_args()
    renderParaview(args)
    os.system('paraview --script=paraview.py')
    os.system('rm paraview.py')


if __name__ == "__main__":
    main()
