#ifndef __WRAP_HH__
#define __WRAP_HH__
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "cast.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace xpace {

namespace wrap {

namespace py = pybind11;

  /// For naming classes templated with dimesion
  inline std::string makeDimensionName(const std::string& name, UInt dim) {
    std::stringstream str;
    str << name << dim << "D";
    return str.str();
  }

  /* -------------------------------------------------------------------------- */
  /* Prototypes for wrapping of xpace components */
  /* -------------------------------------------------------------------------- */
  void wrapSystem(py::module& mod);
  void wrapAlgorithms(py::module& mod);
  void wrapSurface(py::module& mod);
  void wrapGeometry(py::module& mod);
  void wrapComputes(py::module& mod);
  void wrapFilters(py::module& mod);   
}

}


#endif
