#include <pybind11/functional.h>
/* -------------------------------------------------------------------------- */
#include "particles.hh"
#include "system_evolution.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

namespace wrap {

using namespace py::literals;

void wrapSystem(py::module &mod) {
  py::class_<SystemEvolution>(mod, "SystemEvolution")
      .def(py::init<>())
      .def("size", &SystemEvolution::size)
      .def("create",
           [](SystemEvolution& sys, CreationAlgorithm& creation){
             sys.create(creation);
             })
      .def("evolvePerStep",
           [](SystemEvolution &sys, EvolutionAlgorithm &evolution, UInt &start,
              UInt &end) { sys.evolvePerStep(evolution, start, end); })
      .def("evolve",
           py::overload_cast<EvolutionAlgorithm &>(&SystemEvolution::evolve))
      .def("evolve",
           py::overload_cast<EvolutionAlgorithm &, UInt &, UInt &>(
               &SystemEvolution::evolve),
           "evolve the system from start index to end index", "evolution"_a,
           "start"_a, "end"_a)
      .def("setDumper", &SystemEvolution::setDumper,
           "set the base name and dump frequency", "basename"_a = "default",
           "dump_freq"_a = 10)
      .def("computeForceInteraction", &SystemEvolution::computeForceInteraction)
      .def("computeBoundaryInteraction",
           &SystemEvolution::computeBoundaryInteraction)
      .def("coordinationNumber", &SystemEvolution::coordinationNumber)
      .def("numberOfContacts", &SystemEvolution::numberOfContacts)
      .def("maximumOverlap", &SystemEvolution::maximumOverlap)
      .def("averageOverlap", &SystemEvolution::averageOverlap)
      .def("packingDensity", &SystemEvolution::packingDensity)
      .def("dump", py::overload_cast<>(&SystemEvolution::dump))
      .def("dump", py::overload_cast<UInt &, UInt &>(&SystemEvolution::dump))
      .def("getCenter", &SystemEvolution::getCenter,
           py::return_value_policy::reference)
      .def("getForce", &SystemEvolution::getForce,
           py::return_value_policy::reference)
      .def("getEnergy", &SystemEvolution::getEnergy,
           py::return_value_policy::reference)
      .def("getRadius", &SystemEvolution::getRadius,
           py::return_value_policy::reference);
}
} // namespace wrap

}
