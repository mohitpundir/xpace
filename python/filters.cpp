#include "wrap.hh"
#include "filters.hh"
#include "filters_reduce.hh"
#include "filters_refill.hh"

namespace xpace {

namespace wrap {

  void wrapFilters(py::module& mod) {
    py::class_<Filters, std::shared_ptr<Filters>>(mod, "Filters")
      .def_property("dimensions", &Filters::getDimensions,
		    &Filters::setDimensions);

    py::class_<FiltersReduce, Filters, std::shared_ptr<FiltersReduce>>(mod, "FiltersReduce")
      .def(py::init<>());

    py::class_<FiltersRefill, Filters, std::shared_ptr<FiltersRefill>>(mod, "FiltersRefill")
      .def(py::init<Vector&, Vector&, UInt&>());

  }



} // namespace wrap


}
