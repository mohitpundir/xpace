#include <pybind11/functional.h>
/* -------------------------------------------------------------------------- */
#include "creation_algorithm.hh"
#include "creation_fuller_algorithm.hh"
#include "creation_from_file.hh"
#include "dynamic_algorithm.hh"
#include "evolution_algorithm.hh"
#include "hybrid_algorithm.hh"
#include "particles.hh"
#include "take_place_algorithm.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

namespace wrap {

void wrapAlgorithms(py::module &mod) {
  py::class_<CreationAlgorithm, std::shared_ptr<CreationAlgorithm>>(mod, "CreationAlgorithm")
    .def_property("origin", &CreationAlgorithm::getOrigin,
		   &CreationAlgorithm::setOrigin)
    .def_property("spatial_dimension", &CreationAlgorithm::getSpatialDimension,
		   &CreationAlgorithm::setSpatialDimension)
    .def_property("dimensions", &CreationAlgorithm::getDimensions,
		  &CreationAlgorithm::setDimensions)
    .def("size", &CreationAlgorithm::size)
    .def_property("particleRadii", &CreationAlgorithm::getParticleRadii,
		  &CreationAlgorithm::setParticleRadii)
    .def("addShape", &CreationAlgorithm::addShape);

  py::class_<CreationFullerAlgorithm, CreationAlgorithm, std::shared_ptr<CreationFullerAlgorithm>>(mod, "CreationFullerAlgorithm")
    .def(py::init<>())
    .def_property("diameters", &CreationFullerAlgorithm::getDiameters,
		  &CreationFullerAlgorithm::setDiameters)
    .def_property("packing_density", &CreationFullerAlgorithm::getPackingDensity,
		  &CreationFullerAlgorithm::setPackingDensity)
    .def("create", py::overload_cast<>(&CreationFullerAlgorithm::create));

  py::class_<CreationFromFile, CreationAlgorithm, std::shared_ptr<CreationFromFile>>(mod, "CreationFromFile")
    .def(py::init<>())
    .def_property("filename", &CreationFromFile::getFilename,
		  &CreationFromFile::setFilename)
    .def("create", py::overload_cast<>(&CreationFromFile::create));

  py::class_<EvolutionAlgorithm>(mod, "EvolutionAlgorithm")
      .def_property("dimensions", &EvolutionAlgorithm::getDimensions,
                    &EvolutionAlgorithm::setDimensions)
      .def_property("spatial_dimension", &EvolutionAlgorithm::getSpatialDimension,
		   &EvolutionAlgorithm::setSpatialDimension)
      .def_property("nsteps", &EvolutionAlgorithm::getNsteps,
                    &EvolutionAlgorithm::setNsteps)
      .def_property("dt", &EvolutionAlgorithm::getTimeStep,
                    &EvolutionAlgorithm::setTimeStep)
      .def_property("coordination", &EvolutionAlgorithm::getCoordination,
                    &EvolutionAlgorithm::setCoordination)
      .def_property("tolerance", &EvolutionAlgorithm::getTolerance,
                    &EvolutionAlgorithm::setTolerance)
      .def_property("damping_ratio", &EvolutionAlgorithm::getDampingRatio,
		     &EvolutionAlgorithm::setDampingRatio)
      .def_property("damping_freq", &EvolutionAlgorithm::getDampingFreq,
		    &EvolutionAlgorithm::setDampingFreq)
       .def_property("convergence", &EvolutionAlgorithm::getConvergenceCriteria,
                    &EvolutionAlgorithm::setConvergenceCriteria)
      .def_property("layers", &EvolutionAlgorithm::getLayers,
                    &EvolutionAlgorithm::setLayers)
      .def("addCompute", &EvolutionAlgorithm::addCompute)
      .def("addTimeIntegration", &EvolutionAlgorithm::addTimeIntegration)
      .def("addBoundary", &EvolutionAlgorithm::addBoundary);

  py::class_<TakePlaceAlgorithm, EvolutionAlgorithm>(mod, "TakePlaceAlgorithm")
      .def(py::init<>())
      .def_property("gamma", &TakePlaceAlgorithm::getGamma,
                    &TakePlaceAlgorithm::setGamma);

  py::class_<HybridAlgorithm, EvolutionAlgorithm>(mod, "HybridAlgorithm")
      .def(py::init<>());

  py::class_<DynamicAlgorithm, EvolutionAlgorithm>(mod, "DynamicAlgorithm")
      .def(py::init<>())
      .def_property("killVelocity", &DynamicAlgorithm::getKillVelocity,
                    &DynamicAlgorithm::setKillVelocity);
}

} // namespace wrap

}
