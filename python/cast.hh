#ifndef __CAST_HH__
#define __CAST_HH__
/* -------------------------------------------------------------------------- */
#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <pybind11/eigen.h>
/* -------------------------------------------------------------------------- */
#include "grid_data.hh"

namespace py = pybind11;

namespace pybind11{
namespace detail{

/**
 * Type Caster : GridData <-> NumPy-array
 */ 

template<>
struct type_caster<xpace::GridData> {
  using type = xpace::GridData;
  template<typename U>
  using array_type = py::array_t<U, array::c_style | array::forcecast>;
  
public:
    
  PYBIND11_TYPE_CASTER(type, _("GridDataWrap"));

  /// Conversion part 1 ---- (Python -> C++)
  bool load(py::handle src, bool convert) {
    if (!convert and !array_type<xpace::Real>::check_(src)) 
      return false;
    
    auto buf = array_type<xpace::Real>::ensure(src);
    if (!buf)
      return false;

    auto dims = buf.ndim();
    if (dims != 2)
      return false;

    std::vector<size_t> shape(dims);

    for (unsigned int i = 0; i < dims; ++i) {
      shape[i] = buf.shape()[i];
    }

    value = type(shape[0], buf.data());
    
    return true;
  }

  /// Conversion part 2 --- (C++ -> python)
  static py::handle cast(const type & src,
			 py::return_value_policy policy, py::handle parent) {

    py::array a(std::move(src.shape()),
		src.getInternalData(),
		parent);
      
    return a.release();
  }
}; 
 
} // namespace detail
} // namespace pybind11


#endif
