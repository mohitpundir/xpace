#include "wrap.hh"
#include "sphere.hh"
#include "circle.hh"
#include "sphere_factory.hh"
#include "circle_factory.hh"

#ifdef XPACE_USE_POLYHEDRON  
#include "polyhedron.hh"
#include "polyhedron_factory.hh"
#endif // XPACE_USE_POLYHEDRON


namespace xpace {


namespace wrap {
  
void wrapGeometry(py::module& mod) {
  py::class_<Shape>(mod, "Shape")
    .def(py::init<>())
    .def_property("radius", &Shape::getRadius,
		  &Shape::setRadius)
    .def_property("volume", &Shape::getVolume,
		  &Shape::setVolume);

  py::class_<Sphere>(mod, "Sphere")
    .def(py::init<>())
    .def("create",
	 &Sphere::create);

  py::class_<ShapeFactoryInterface>(mod, "ShapeFactoryInterface")
    .def("getInstance", &ShapeFactoryInterface::getInstance,
	 py::return_value_policy::reference);

  py::class_<SphereFactory, ShapeFactoryInterface>(mod, "SphereFactory")
    .def("getInstance", &SphereFactory::getInstance,
	 py::return_value_policy::reference);

  
  py::class_<CircleFactory, ShapeFactoryInterface>(mod, "CircleFactory")
    .def("getInstance", &CircleFactory::getInstance,
	 py::return_value_policy::reference);

  
#ifdef XPACE_USE_POLYHEDRON  
  py::class_<Polyhedron>(mod, "Polyhedron")
    .def(py::init<>())
    .def("create",
	 &Polyhedron::create);*/

  py::class_<PolyhedronFactory, ShapeFactoryInterface>(mod, "PolyhedronFactory")
      .def("getInstance", &PolyhedronFactory::getInstance,
    py::return_value_policy::reference);
#endif // XPACE_USE_POLYHEDRON
}

}

}
