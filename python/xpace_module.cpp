#include "xpace.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/pybind11.h>
#include <pybind11/functional.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */


namespace xpace {

namespace py = pybind11;

PYBIND11_MODULE(xpace, mod) {
  mod.doc() = "xpace module for python";

  py::enum_<SpatialDirection>(mod, "SpatialDirection")
    .value("x", _x)
    .value("y", _y)
    .value("z", _z)
    .value("all", _all)
    .export_values();

  py::enum_<Boundary>(mod, "Boundary")
    .value("periodic", _periodic)
    .value("soft_aperiodic", _soft_aperiodic)
    .value("hard_aperiodic", _hard_aperiodic)
    .export_values();
  
  py::enum_<ConvergenceCriteria>(mod, "Convergence")
    .value("contact", ConvergenceCriteria::contact)
    .value("packing_density", ConvergenceCriteria::packing_density)
    .export_values();

  // Wrapping xpace components
  wrap::wrapAlgorithms(mod);
  wrap::wrapSystem(mod);
  wrap::wrapComputes(mod);
  wrap::wrapSurface(mod);
  wrap::wrapGeometry(mod);
}


}
