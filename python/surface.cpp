#include <pybind11/functional.h>
/* -------------------------------------------------------------------------- */
#include "surface_generator.hh"
#include "surface_statistics.hh"
#include "grid_data.hh"
/* -------------------------------------------------------------------------- */
#include "wrap.hh"

namespace xpace {

namespace wrap{

void wrapSurface(py::module& mod) {
  py::class_<SurfaceGenerator>(mod, "SurfaceGenerator")
    .def(py::init<>())
    .def_property("size", &SurfaceGenerator::getSize,
		  &SurfaceGenerator::setSize)
    .def_property("height", &SurfaceGenerator::getHeight,
		  &SurfaceGenerator::setHeight)
    .def_property("degree", &SurfaceGenerator::getDegree,
		  &SurfaceGenerator::setDegree)
    .def_property("direction", &SurfaceGenerator::getDirection,
		  &SurfaceGenerator::setDirection)
    .def_property("dimensions", &SurfaceGenerator::getDimensions,
		  &SurfaceGenerator::setDimensions)
    .def("build",
	 &SurfaceGenerator::build,
	 py::return_value_policy::reference_internal)
    .def("dump",
	 &SurfaceGenerator::dump);

  py::class_<SurfaceStatistics>(mod, "SurfaceStatistics")
    .def(py::init<>())
    .def_static("computeStructureFunction",
		&SurfaceStatistics::computeStructureFunction)
    .def_static("computeRMSHeight",
		&SurfaceStatistics::computeRMSHeight)
    .def_static("computeNormals",
		&SurfaceStatistics::computeNormals)
    .def_static("computeNormal",
		&SurfaceStatistics::computeNormal)
    .def_static("computePowerSpectrum1D",
		&SurfaceStatistics::computePowerSpectrum1D,
		py::return_value_policy::copy)
    .def("computePowerSpectrum2D",
	 &SurfaceStatistics::computePowerSpectrum2D,
	 py::return_value_policy::reference_internal)
    .def_static("computePhaseSpectrum1D",
		&SurfaceStatistics::computePhaseSpectrum1D,
		py::return_value_policy::copy)
    .def("computePhaseSpectrum2D",
	 &SurfaceStatistics::computePhaseSpectrum2D,
	 py::return_value_policy::reference_internal)
    .def("computeAutocorrelation",
	  &SurfaceStatistics::computeAutocorrelation,
	  py::return_value_policy::reference_internal)
    .def_static("computeAutocorrelation1D",
		&SurfaceStatistics::computeAutocorrelation1D,
		py::return_value_policy::copy);
}



}
}
