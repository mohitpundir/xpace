#ifndef __NUMPY_HH__
#define __NUMPY_HH__

#include "xpace.hh"
#include "grid_data.hh"


#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace xpace {

namespace wrap {
namespace py = pybind11;

  
using numpy = py::array_t<Real, py::array::c_style | py::array::forecast>;

  
class GridDataNumpy : public GridData {
public:
  GridDataNumpy(numpy & buffer) : GridData {
    this->nb_components = 1;
    this->data.wrapMemory(buffer.mutable_data(), buffer.size());
  }
};


  
}// namespace wrap


}

#endif
