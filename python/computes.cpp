#include "wrap.hh"
#include "vector.hh"
#include "compute.hh"
#include "compute_boundary.hh"
#include "compute_lennard_jones.hh"
#include "compute_viscocity.hh"
#include "compute_vertical_gravity.hh"
#include "compute_newton.hh"
#include "compute_time_integration.hh"

#include <pybind11/functional.h>


namespace xpace {

namespace wrap {

using namespace py::literals;

void wrapComputes(py::module& mod) {
  py::class_<Compute, std::shared_ptr<Compute>>(mod, "Compute");

  py::class_<ComputeBoundary, Compute, std::shared_ptr<ComputeBoundary>>(mod, "ComputeBoundary")
    .def(py::init<>())
    .def(py::init<UInt&, Vector&, Boundary>(),
	 "spatial_dimension"_a,  "dimensions"_a, "type"_a)
    .def("compute",
	 &ComputeBoundary::compute);

  py::class_<ComputeLennardJones, Compute, std::shared_ptr<ComputeLennardJones>>(mod, "ComputeLennardJones")
    .def(py::init<>())
    .def(py::init<Real&, Vector&, Boundary>(),
	 "gamma"_a, "dimensions"_a, "type"_a)
    .def_property("gamma", &ComputeLennardJones::getGamma,
		  &ComputeLennardJones::setGamma,
		  py::return_value_policy::move)
    .def("computeForce",
	 &ComputeLennardJones::computeForce, "distance"_a,
	 "computes the force vector for a given distance",
	 py::return_value_policy::move)
    .def("computePotential",
	 &ComputeLennardJones::computePotential, "distance"_a,
	 "computes the potential energy for a given distance",
	 py::return_value_policy::move);
    
    
  py::class_<ComputeViscocity, Compute, std::shared_ptr<ComputeViscocity>>(mod, "ComputeViscocity")
    .def(py::init<>())
    .def(py::init<Real&>(),
	 "eta"_a)
    .def_property("eta", &ComputeViscocity::getEta,
		  &ComputeViscocity::setEta,
		  "viscocity or drag coefficient");
  
  py::class_<ComputeNewton, Compute, std::shared_ptr<ComputeNewton>>(mod, "ComputeNewton")
    .def(py::init<Real&>());
  
  py::class_<ComputeVerticalGravity, Compute, std::shared_ptr<ComputeVerticalGravity>>(mod, "ComputeVerticalGravity")
    .def(py::init<Vector&, Real&>(),
	 "direction"_a, "gravity"_a=1.0)
    .def_property("gravity", &ComputeVerticalGravity::getGravity,
		  &ComputeVerticalGravity::setGravity,
		  "value of gravity")
    .def_property("direction", &ComputeVerticalGravity::getDirection,
		  &ComputeVerticalGravity::setDirection,
		  "direction vector for the gravity");

  py::class_<ComputeTimeIntegration, std::shared_ptr<ComputeTimeIntegration>>(mod, "ComputeTimeIntegration")
    .def(py::init<Real&>(),
	 "time"_a=0.1);
}
  

} // namespace wrap


}
