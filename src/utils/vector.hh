#ifndef __VECTOR__HH__
#define __VECTOR__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include <Eigen/Dense>
/* -------------------------------------------------------------------------- */

namespace xpace {

using Vector = Eigen::Matrix<Real, 3, 1>;

/* -------------------------------------------------------------------------- */
/// standard input stream operator
inline std::istream& operator>>(std::istream& stream, Vector& _this) {
  for (auto&& v : _this)
    stream >> v;
  return stream;
}

}

#endif // __VECTOR__HH__


