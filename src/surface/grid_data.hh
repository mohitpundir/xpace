#ifndef __GRIDDATA__HH__
#define __GRIDDATA__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "plane.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


const UInt dim = 2;

/* -------------------------------------------------------------------------- */
/* GridData Iterator                                                          */ 
/* -------------------------------------------------------------------------- */

struct GridDataFieldIterator {
  GridDataFieldIterator(UInt index, UInt size, Real* grid, Real* field)
    : index(index), size(size), grid(grid), field(field) {};
 
  std::tuple<Real&, Real&, Real&> operator*() {
    return std::tuple<Real&, Real&, Real&>(this->grid[index*dim], this->grid[index*dim + 1], this->field[index]);
  };

  void operator++() {index++;};

  bool operator!=(GridDataFieldIterator & other) { return this->index != other.index; };

  int index, size;
  Real* field;
  Real* grid;
};


/* -------------------------------------------------------------------------- */
/* GridData Field Iterator                                                    */ 
/* -------------------------------------------------------------------------- */

struct IndexedFieldIterator {
  IndexedFieldIterator(UInt index, UInt size, Real* field)
    : index(index), size(size), field(field) {};
 
  std::tuple<UInt, UInt, Real&> operator*() {
    UInt i = index % size;
    UInt j = index / size;
    return std::tuple<UInt, UInt, Real&>(i, j, this->field[index]);
  };

  void operator++() {index++;};

  bool operator!=(IndexedFieldIterator & other) { return this->index != other.index; };

  int index, size;
  Real* field;
};


/* -------------------------------------------------------------------------- */
/* GridData Grid Iterator                                                     */ 
/* -------------------------------------------------------------------------- */

struct IndexedGridIterator {
  IndexedGridIterator(UInt index, UInt size, Real* grid)
    : index(index), size(size), grid(grid) {};
 
  std::tuple<UInt, UInt, Real&, Real&> operator*() {
    UInt i = index % size;
    UInt j = index / size;
    return std::tuple<UInt, UInt, Real&, Real&>(i, j, this->grid[index*dim],
					 this->grid[index*dim + 1]);
  };

  void operator++() {index++;};

  bool operator!=(IndexedGridIterator & other) { return this->index != other.index; };

  int index, size;
  Real* grid;
};


/* -------------------------------------------------------------------------- */

template<typename T>
struct IndexedField {

  IndexedField(T& grid) : grid(grid){};

  IndexedFieldIterator begin() {
    return IndexedFieldIterator(0, grid.getSize(),
			 grid.field());
  };
  IndexedFieldIterator end() {
    return IndexedFieldIterator(grid.getSize()*grid.getSize(), grid.getSize(),
			      grid.field());
  };

private:
  T& grid;
};



/* -------------------------------------------------------------------------- */

template<typename T>
struct IndexedGrid {

  IndexedGrid(T& grid) : grid(grid){};

  IndexedGridIterator begin() {
    return IndexedGridIterator(0, grid.getSize(),
			       grid.coords());
  };
  IndexedGridIterator end() {
    return IndexedGridIterator(grid.getSize()*grid.getSize(),
			       grid.getSize(), grid.coords());
  };

private:
  T& grid;
};


/* -------------------------------------------------------------------------- */
/* Grid Data Class                                                            */
/* -------------------------------------------------------------------------- */

class GridData {

public:
  /// Constructor by default
  GridData() = default;
  /// Constructor with size
  GridData(UInt);
  /// Copy constructor
  GridData(UInt, const Real* );

  
public:
  /// resizes the grid data
  void resize(UInt &);
  /// get the grid point
  inline void getGridPoint(UInt &, Vector &);
  /// dumps the grid data as paraview file
  void dump(const std::string filename, SpatialDirection ); 
  /// get the size of grid data
  inline UInt getSize() const {return this->size;}
  /// adds to the connectivity
  inline void addToConnectivity(UInt, UInt, UInt);
  /* -------------------------------------------------------------------------- */
  inline const Real& operator()(UInt i) const;
  /* -------------------------------------------------------------------------- */
  inline Real& operator[](UInt i);
  /// gets the const reference to the address of data
  const Real* getInternalData() const {return data.data(); }
  /// gets the const reference of the address of grid
  const Real* getInternalGrid() const {return grid.data(); }
  /// gets the dimensions of the grid
  inline UInt dimensions() {return dim; }
  /* -------------------------------------------------------------------------- */
  inline Real* field() {return &data[0]; }
  /* -------------------------------------------------------------------------- */
  inline Real* coords() {return &grid[0]; }
  /* -------------------------------------------------------------------------- */
  inline std::vector<size_t> strides(bool) const;
  /* -------------------------------------------------------------------------- */
  inline std::vector<size_t> shape() const;
  /* -------------------------------------------------------------------------- */
  inline void clear();

  /* -------------------------------------------------------------------------- */
  /* Mathematical operation                                                     */
  /* -------------------------------------------------------------------------- */
public:
  /// returns the sum of the data
  inline Real sum() const  { return std::accumulate(data.begin(), data.end(), 0.0); }
  /// returns the mean of the data
  inline Real mean() const { return this->sum()/this->data.size(); }
      
public:
  /// builds data from a plane
  void buildDataFromPlane(Plane &,
			  SpatialDirection );

  void addToData(std::vector<Real> & );

  void buildGrid(std::vector<Real> &, std::vector<Real> &);
  /* -------------------------------------------------------------------------- */
  /* Iterators Method                                                           */
  /* -------------------------------------------------------------------------- */
public:
  /// begin iterator
  GridDataFieldIterator begin() {
    return GridDataFieldIterator(0, size,
				 this->coords(), this->field()); }
  /// end iterator
  GridDataFieldIterator end() {
    return GridDataFieldIterator(size*size, size,
				 this->coords(), this->field()); }
  /// Indexed Field iterator with begin and end
  IndexedField<GridData> indexedField() {
    return IndexedField<GridData>(*this); }
  /// Indexed Grid iterator with begin and end 
  IndexedGrid<GridData> indexedGrid() {
    return IndexedGrid<GridData>(*this); }

  /* ------------------------------------------------------------------------ */
  /* Data Accessor                                                            */
  /* ------------------------------------------------------------------------ */
  
protected:
  /// number of points/data along each dim
  UInt size;
  /// offset for connectivity
  UInt offset;
  /// to store spatial data of surface
  std::vector<Real> data;
  /// base or grid points
  std::vector<Real> grid;
  /// connectivity for dumping to paraview
  std::vector<UInt> connectivities;
};

/* -------------------------------------------------------------------------- */

inline void GridData::addToConnectivity(UInt t1, UInt t2, UInt t3) {
  connectivities.push_back(t1);
  connectivities.push_back(t2);
  connectivities.push_back(t3);
}

/* -------------------------------------------------------------------------- */
inline Real & GridData::operator[](UInt i) {
  return this->data[i];
}

/* -------------------------------------------------------------------------- */

inline const Real & GridData::operator()(UInt i) const {
  return this->data[i];
}

/* -------------------------------------------------------------------------- */

inline std::vector<size_t> GridData::shape() const {
  std::vector<size_t> ret(dim);
  ret[0] = size;
  ret[1] = size;

   
  return ret;
}

/* -------------------------------------------------------------------------- */

inline std::vector<size_t> GridData::strides(bool bytes=false) const {
  std::vector<size_t> ret(dim);
  /*ret[0] = 1;

  if (bytes) {
    ret[0] *= sizeof(Real);
  }*/

  std::vector<size_t> m_strides; 
  m_strides[0] = size;
  m_strides[1] = size;

  for ( size_t i = 0 ; i < dim ; ++i )
    ret[i] = m_strides[i];

  if ( bytes )
    for ( size_t i = 0 ; i < dim ; ++i )
      ret[i] *= sizeof(Real);
  
  return ret;
}

/* -------------------------------------------------------------------------- */

inline void GridData::clear() {
  grid.clear();
  data.clear();
  connectivities.clear();
}

/* -------------------------------------------------------------------------- */

inline void GridData::getGridPoint(UInt & i, Vector & grid_point) {
  grid_point[0] = grid[i*dim    ];
  grid_point[1] = grid[i*dim + 1];
}



}


#endif
