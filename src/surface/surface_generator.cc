#include "surface_generator.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


/* -------------------------------------------------------------------------- */

GridData & SurfaceGenerator::build(SystemEvolution & system) {
  
  this->surface.clear();
  this->surface.resize(this->size);
  
  for (UInt i = 0; i < dim + 1; ++i) {
    max[i] =  this->dimensions[i]/2.;
    min[i] = -this->dimensions[i]/2.;
  }
  
  switch (direction) {
  case _x: {
    this->buildGrid<_y, _z, _x>();
    this->buildData<_y, _z, _x>(system);
    break;
  }
  case _y: {
    this->buildGrid<_z, _x, _y>();
    this->buildData<_z, _x, _y>(system);
    break;
  }
  default:
    this->buildGrid<_x, _y, _z>();
    this->buildData<_x, _y, _z>(system);
    break;
  }

  return surface;
}

/* -------------------------------------------------------------------------- */
template<SpatialDirection ii, SpatialDirection jj, SpatialDirection kk>
void SurfaceGenerator::buildGrid() {

  std::cerr << " ----- Building Grid --------- " << std::endl;
  
  Real di = (max[ii] - min[ii]) / (size - 1);
  Real dj = (max[jj] - min[jj]) / (size - 1);

  for (auto && grid: surface.indexedGrid()) {
    UInt i = std::get<0>(grid);
    UInt j = std::get<1>(grid);
    std::get<2>(grid) = min[ii] + di*i;
    std::get<3>(grid) = min[jj] + dj*j;
  }

  for (UInt i = 0; i < (size - 1); ++i) {
    for (UInt j = size*i; j < size*i + (size - 1); ++j) {
      surface.addToConnectivity(j     , j + 1            , j + size);
      surface.addToConnectivity(j + 1 , j + size + 1     , j + size);
    }
  }
}



/* -------------------------------------------------------------------------- */
template<SpatialDirection ii, SpatialDirection jj, SpatialDirection kk>
void SurfaceGenerator::buildData(SystemEvolution & system) {

  std::cerr << " ----- Building Data ------ " << std::endl;

  Plane plane;
  plane.create<kk>(degree, height);
  surface.buildDataFromPlane(plane, kk);
    
  bool is_above       = false;
  bool is_intersected = false;
  bool is_inside      = false;
  UInt index;
  
  auto & particles = system.getParticles();
  for (UInt i = 0; i < particles.size(); ++i) {    

    auto && shape = particles.getShape(i);
    is_intersected = shape->intersect(plane);
    
    if (is_intersected) {
      for (auto && points: surface) {
	auto & x = std::get<0>(points);
	auto & y = std::get<1>(points);
	auto & z = std::get<2>(points);

	if (z != height)
	  continue;

	Vector point(x, y, z);
	std::tie(is_inside, index) = shape->contains(point);

	if (is_inside) {
	  auto && center = shape->getCenter(index);
	  is_above = plane.above(center);
	  z = shape->computeProjection(point, is_above, index);
	}
      } 

    }
  }
}
  

}
