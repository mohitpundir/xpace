#ifndef __SURFACE_STATISTICS_HH__
#define __SURFACE_STATISTICS_HH__
/* -------------------------------------------------------------------------- */
#include <map>
#include <tuple>
#include <omp.h>
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "fft.hh"
#include "matrix.hh"
#include "grid_data.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

using RowMatrixXd = Eigen::Matrix<Real, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
using VectorXd = Eigen::Matrix<Real, Eigen::Dynamic, 1>; 
using PairXi = Eigen::Matrix<int, Eigen::Dynamic, 2, Eigen::RowMajor>;

class SurfaceStatistics {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  /// Constructor
  SurfaceStatistics() = default;
  /// Destructor
  ~SurfaceStatistics() = default;
  
public:
  /// computes power spectrum from matrix type
  inline static Matrix<complex> computePowerSpectrum(Matrix<complex> &);
  /// computes 2D power spectrum from griddata type 
  inline GridData & computePowerSpectrum2D(GridData &);
  /// computes 1D power spectrum from gridData type
  inline static std::map<int, Real> computePowerSpectrum1D(GridData &);
  /// computes phase spectrum from matrix type
  inline static Matrix<complex> computePhaseSpectrum(Matrix<complex> &);
  /// computes 2D phase spectrum from griddata type 
  inline GridData & computePhaseSpectrum2D(GridData &);
  /// computes 1D phase spectrum from gridData type
  inline static std::map<int, Real> computePhaseSpectrum1D(GridData &);
  /// computes auto correlation function from gridData type
  inline GridData & computeAutocorrelation(GridData &);
  /// computes auto correlation function in 1d
  inline static std::map<int, Real> computeAutocorrelation1D(GridData &);
  /// computes rms height
  inline static Real computeRMSHeight(GridData &);
  /// computes average amplitude
  inline static Real computeAvgAmplitude(GridData &);
  /// computes the frequencies
  inline static Matrix<std::complex<Int>> computeFrequencies(UInt);
  /// computes effective normal for contact patches
  inline static Vector computeNormals(Eigen::Ref<RowMatrixXd> , Eigen::Ref<VectorXd>,
				      Eigen::Ref<VectorXd>, const std::vector<std::pair<int, int>> &);
  /// compute effective normal fora contact point
  inline static Vector computeNormal(Eigen::Ref<RowMatrixXd> , Eigen::Ref<VectorXd>,
				     Eigen::Ref<VectorXd>, const std::pair<int, int> &);
  
  /// computes height-height correlation function
  inline static void computeStructureFunction(Eigen::Ref<VectorXd>, Eigen::Ref<VectorXd>,
					      Eigen::Ref<VectorXd>);
  
private :

  GridData data_out;
  
};


/* -------------------------------------------------------------------------- */
void SurfaceStatistics::computeStructureFunction(Eigen::Ref<VectorXd> surface, Eigen::Ref<VectorXd> x, Eigen::Ref<VectorXd> y) {

  int row = surface.rows();
  int col = surface.cols();

  auto nb_points = row * col;

  std::map<int, Real> deltas;
  std::map<int, Real> counts;

  for (UInt i = 0; i < nb_points; i++) {    
    for (UInt j = i ; j < nb_points; j++) {

      Real delta_x = x[i] - x[j];
      Real delta_y = y[i] - y[j];
      Real delta = delta_x*delta_x + delta_y*delta_y;
      
      Real delta_h = surface[i] - surface[j];
      delta_h *= delta_h;

      int index = (int)delta;
      /*deltas[index    ] += delta_h;
      deltas[index - 1] += delta_h;
      deltas[index + 1] += delta_h;

      counts[index    ] += 1.0;
      counts[index - 1] += 1.0;
      counts[index + 1] += 1.0;*/
    }
  }

  /*for (auto & entry: deltas) {
    auto & key   = entry.first;
    auto & value = entry.second;
    value /= counts[key];
  }*/

}


/* -------------------------------------------------------------------------- */
Vector
SurfaceStatistics::computeNormals(Eigen::Ref<RowMatrixXd> surface, Eigen::Ref<VectorXd> x,
				  Eigen::Ref<VectorXd> y, const std::vector<std::pair<int, int>> & nodes) {

  int row = surface.rows();
  std::map<int, Real> dx;
  std::map<int, Real> dy;
    
  dx[0] = 0;
  dy[0] = 0;

  dx[1] = (*std::max_element(x.begin(), x.end()) - *std::min_element(x.begin(), x.end()))/row;
  dy[1] = (*std::max_element(y.begin(), y.end()) - *std::min_element(y.begin(), y.end()))/row;

  dx[-1] = -dx[1];
  dy[-1] = -dy[1];

  auto get_point = [&](std::tuple<int, int> nodes,
		       std::tuple<int, int> neighbors) {
    auto [i, j] = nodes;
    auto [ii, jj] = neighbors;
    auto xx = x[i] + dx[ii - i];
    auto yy = y[j] + dy[jj - j];

    if (ii >= row)
      ii = ii - row;
    else if (ii < 0)
      ii = row + ii;

    if (jj >= row)
      jj = jj - row;
    else if (jj < 0)
      jj = jj + row;

    auto ss = surface(ii, jj);

    Vector point(xx, yy, ss);
    return point;
  };

  auto get_normal = [](const Vector & vec1, const Vector & vec2,
			Vector & normal) {
    auto tmp = vec1.cross(vec2);
    tmp /= tmp.norm();
    normal += tmp;
  };

  auto nb_elements = 0;
  Vector eff_normal(0., 0., 0.);

  for (auto & node: nodes) {
    auto i = std::get<0>(node);
    auto j = std::get<1>(node);
    
    auto coord1 = get_point({i, j}, {i - 1, j - 1});
    auto coord2 = get_point({i, j}, {i    , j - 1});
    auto coord3 = get_point({i, j}, {i + 1, j - 1});
    auto coord4 = get_point({i, j}, {i - 1, j    });
    auto coord5 = get_point({i, j}, {i    , j    });
    auto coord6 = get_point({i, j}, {i + 1, j    });
    auto coord7 = get_point({i, j}, {i - 1, j + 1});
    auto coord8 = get_point({i, j}, {i    , j + 1});

    auto vec21 = coord2 - coord1;
    auto vec41 = coord4 - coord1;

    auto vec32 = coord3 - coord2;
    auto vec52 = coord5 - coord2;

    auto vec54 = coord5 - coord4;
    auto vec74 = coord7 - coord4;

    auto vec65 = coord6 - coord5;
    auto vec85 = coord8 - coord5;
    
    Vector normal(0., 0., 0.);
    get_normal(vec21, vec41, normal);
    get_normal(vec32, vec52, normal);
    get_normal(vec54, vec74, normal);
    get_normal(vec65, vec85, normal);
    normal /= 4.;

    eff_normal += normal;
    nb_elements++;
  }

  return eff_normal/nb_elements; 
}


/* -------------------------------------------------------------------------- */
Vector
SurfaceStatistics::computeNormal(Eigen::Ref<RowMatrixXd> surface, Eigen::Ref<VectorXd> x,
				  Eigen::Ref<VectorXd> y, const std::pair<int, int> & node) {

  int row = surface.rows();
  std::map<int, Real> dx;
  std::map<int, Real> dy;
    
  dx[0] = 0;
  dy[0] = 0;

  dx[1] = (*std::max_element(x.begin(), x.end()) - *std::min_element(x.begin(), x.end()))/row;
  dy[1] = (*std::max_element(y.begin(), y.end()) - *std::min_element(y.begin(), y.end()))/row;

  dx[-1] = -dx[1];
  dy[-1] = -dy[1];

  auto get_point = [&](std::tuple<int, int> nodes,
		       std::tuple<int, int> neighbors) {
    auto [i, j] = nodes;
    auto [ii, jj] = neighbors;
    auto xx = x[i] + dx[ii - i];
    auto yy = y[j] + dy[jj - j];

    if (ii >= row)
      ii = ii - row;
    else if (ii < 0)
      ii = row + ii;

    if (jj >= row)
      jj = jj - row;
    else if (jj < 0)
      jj = jj + row;

    auto ss = surface(ii, jj);

    Vector point(xx, yy, ss);
    return point;
  };

  auto get_normal = [](const Vector & vec1, const Vector & vec2,
			Vector & normal) {
    auto tmp = vec1.cross(vec2);
    tmp /= tmp.norm();
    normal += tmp;
  };

  auto nb_elements = 0;
  Vector eff_normal(0., 0., 0.);

  auto i = std::get<0>(node);
  auto j = std::get<1>(node);
    
  auto coord1 = get_point({i, j}, {i - 1, j - 1});
  auto coord2 = get_point({i, j}, {i    , j - 1});
  auto coord3 = get_point({i, j}, {i + 1, j - 1});
  auto coord4 = get_point({i, j}, {i - 1, j    });
  auto coord5 = get_point({i, j}, {i    , j    });
  auto coord6 = get_point({i, j}, {i + 1, j    });
  auto coord7 = get_point({i, j}, {i - 1, j + 1});
  auto coord8 = get_point({i, j}, {i    , j + 1});
  
  auto vec21 = coord2 - coord1;
  auto vec41 = coord4 - coord1;

  auto vec32 = coord3 - coord2;
  auto vec52 = coord5 - coord2;
  
  auto vec54 = coord5 - coord4;
  auto vec74 = coord7 - coord4;

  auto vec65 = coord6 - coord5;
  auto vec85 = coord8 - coord5;
    
  Vector normal(0., 0., 0.);
  get_normal(vec21, vec41, normal);
  get_normal(vec32, vec52, normal);
  get_normal(vec54, vec74, normal);
  get_normal(vec65, vec85, normal);
  normal /= 4.;

  return normal; 
}



/* -------------------------------------------------------------------------- */
Matrix<complex>
SurfaceStatistics::computePowerSpectrum(Matrix<complex> & mat) {
  Matrix<complex> power_spectrum = FFT::transform(mat);
  power_spectrum.makeItRealBySquare();
  auto size = mat.size();
  power_spectrum /= size * size;
  return power_spectrum;
}

/* -------------------------------------------------------------------------- */

GridData &
SurfaceStatistics::computePowerSpectrum2D(GridData & data_in) {

  UInt k = 0;
  Matrix<complex> m_in(data_in.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    val = data_in[k];
    k++;
  }

  auto m_out = computePowerSpectrum(m_in);
  
  UInt m = 0;
  UInt size = data_in.getSize();
  data_out.clear();
  data_out.resize(size);

  for (auto && entry : index(m_out)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto val = std::get<2>(entry);

    data_out[m] = abs(val);
    m++;
  }

  return data_out;
}

/* -------------------------------------------------------------------------- */
std::map<int, Real>
SurfaceStatistics::computePowerSpectrum1D(GridData & data_in) {

  UInt k = 0;
  Matrix<complex> m_in(data_in.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    std::get<2>(entry) = data_in(k);
    k++;
  }

  auto m_out = computePowerSpectrum(m_in);

  std::map<int, Real> psd_1d;
  std::map<int, Real> psd_count;
   
  for (auto && entry: index(m_out)) {
    UInt kx = std::get<0>(entry);
    UInt ky = std::get<1>(entry);
    auto & psd_2d = std::get<2>(entry);

    Real k = abs(sqrt(kx*kx + ky*ky));
    if (k > m_in.size())
      continue;

    UInt index = (int)k;
    psd_1d[index    ] += psd_2d.real();
    psd_1d[index - 1] += psd_2d.real();
    psd_1d[index + 1] += psd_2d.real();

    psd_count[index    ] += 1.0;
    psd_count[index - 1] += 1.0;
    psd_count[index + 1] += 1.0;
  }

  for (auto & entry: psd_1d) {
    auto & key   = entry.first;
    auto & value = entry.second;
    value /= psd_count[key];
  }
  
  return psd_1d;
}

/* -------------------------------------------------------------------------- */
Matrix<complex>
SurfaceStatistics::computePhaseSpectrum(Matrix<complex> & mat) {
  Matrix<complex> phase_spectrum = FFT::transform(mat);
  auto size = mat.size();
  phase_spectrum /= size * size;
  phase_spectrum.makeItPhase();
  return phase_spectrum;
}

/* -------------------------------------------------------------------------- */

GridData &
SurfaceStatistics::computePhaseSpectrum2D(GridData & data_in) {

  UInt k = 0;
  Matrix<complex> m_in(data_in.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    val = data_in[k];
    k++;
  }

  auto m_out = computePhaseSpectrum(m_in);
  
  UInt m = 0;
  UInt size = data_in.getSize();
  data_out.clear();
  data_out.resize(size);

  for (auto && entry : index(m_out)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto val = std::get<2>(entry);

    data_out[m] = abs(val);
    m++;
  }

  return data_out;
}



/* -------------------------------------------------------------------------- */
std::map<int, Real>
SurfaceStatistics::computePhaseSpectrum1D(GridData & data_in) {

  UInt k = 0;
  Matrix<complex> m_in(data_in.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    std::get<2>(entry) = data_in(k);
    k++;
  }

  auto m_out = computePhaseSpectrum(m_in);

  std::map<int, Real> phase_1d;
  std::map<int, Real> phase_count;
  
  for (auto && entry: index(m_out)) {
    UInt kx = std::get<0>(entry);
    UInt ky = std::get<1>(entry);
    auto & phase_2d = std::get<2>(entry);

    Real k = abs(sqrt(kx*kx + ky*ky));
    if (k > m_in.size())
      continue;

    UInt index = (int)k;
    phase_1d[index    ] += phase_2d.real();
    phase_1d[index - 1] += phase_2d.real();
    phase_1d[index + 1] += phase_2d.real();

    phase_count[index    ] += 1.0;
    phase_count[index - 1] += 1.0;
    phase_count[index + 1] += 1.0;
  }

  for (auto & entry: phase_1d) {
    auto & key   = entry.first;
    auto & value = entry.second;
    value /= phase_count[key];
  }
  
  return phase_1d;
}


/* -------------------------------------------------------------------------- */
GridData &
SurfaceStatistics::computeAutocorrelation(GridData & data_in) {
  UInt k = 0;
  Matrix<complex> m_in(data_in.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    std::get<2>(entry) = data_in(k++);
  }

  auto m_out = computePowerSpectrum(m_in);
  auto m_heights = FFT::itransform(m_out);

  UInt m = 0;
  UInt size = data_in.getSize();
  data_out.clear();
  data_out.resize(size);

  for (auto && entry : index(m_heights)) {
    UInt i = std::get<0>(entry);
    UInt j = std::get<1>(entry);
    auto val = std::get<2>(entry);

    data_out[m++] = abs(val);
  }

  return data_out;
}

/* -------------------------------------------------------------------------- */

std::map<int, Real>
SurfaceStatistics::computeAutocorrelation1D(GridData & input) {

  UInt k = 0;
  Matrix<complex> m_in(input.getSize());
  for (auto && entry : index(m_in)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    std::get<2>(entry) = input(k++);
  }

  auto m_out = computePowerSpectrum(m_in);
  auto m_heights = FFT::itransform(m_out);

  std::map<int, Real> correlation;
  std::map<int, Real> heights_count;

  
  for (auto && entry: index(m_heights)) {
    UInt kx = std::get<0>(entry);
    UInt ky = std::get<1>(entry);
    auto & correlation_2d = std::get<2>(entry);

    Real k = abs(sqrt(kx*kx + ky*ky));

    UInt index = (int)k;
    correlation[index    ] += correlation_2d.real();
    correlation[index - 1] += correlation_2d.real();
    correlation[index + 1] += correlation_2d.real();

    heights_count[index     ] += 1.0;
    heights_count[index - 1] += 1.0;
    heights_count[index + 1] += 1.0;
  }

  for (auto & entry: correlation) {
    auto & key   = entry.first;
    auto & value = entry.second;
    value /= heights_count[key];
  }

  return correlation;
}

/* -------------------------------------------------------------------------- */

Real SurfaceStatistics::computeRMSHeight(GridData & data) {

  UInt size= data.getSize();
  GridData temp(size, data.field());
  auto square_it = [](Real & x) {
    x *=x;
  };

  std::for_each(temp.field(), temp.field() + size*size, square_it);
  return sqrt(temp.mean());
}

/* -------------------------------------------------------------------------- */

Real SurfaceStatistics::computeAvgAmplitude(GridData & data_in) {
  return data_in.mean();
}

/* -------------------------------------------------------------------------- */

Matrix<std::complex<Int>>
SurfaceStatistics::computeFrequencies(UInt size) {

  Matrix<std::complex<Int>> freqs(size);
  constexpr UInt dim = 2;
  
  for (auto && entry : index(freqs)) {
    auto i = std::get<0>(entry);
    auto j = std::get<1>(entry);
    auto& freq = std::get<2>(entry);

    std::array<Int, 2> wavevector;
    std::array<Int, 2> tuple{Int(i), Int(j)};

    for (UInt d = 0; d < dim; ++d) {
      auto td = tuple[d];
      auto nd = size;
      auto q  = (tuple[d] < size / 2.0) ? td : td - nd;
      wavevector[d] = q;
    }
    freq = std::complex<Int>{wavevector[0], wavevector[1]};
  }

  return freqs;
}

}
/* -------------------------------------------------------------------------- */

#endif
