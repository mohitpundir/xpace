#ifndef __SURFACE__GENERATOR__HH__
#define __SURFACE__GENERATOR__HH__

/* -------------------------------------------------------------------------- */
#include <vector>
#include <array>
#include <algorithm>
#include <iostream>
#include <fstream>
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "system_evolution.hh"
#include "plane.hh"
#include "grid_data.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */

class SurfaceGenerator {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  /// Default Constructor
  SurfaceGenerator() = default;
  /// Destructor
  virtual ~SurfaceGenerator() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */

public:
  /// builds a grid_data for given points and angle
  GridData & build(SystemEvolution & system);

  /// dump the surface to the paraview
  inline void dump(const std::string);

public:
  XPACE_ACCESSOR(size,       UInt,   Size);
  XPACE_ACCESSOR(height,     Real,   Height);
  XPACE_ACCESSOR(degree,     Real,   Degree);
  XPACE_ACCESSOR(dimensions, Vector, Dimensions);
  XPACE_ACCESSOR(direction,  SpatialDirection, Direction);
    
private:
  /// builds heights based on the grid
  template<SpatialDirection, SpatialDirection, SpatialDirection>
  void buildData(SystemEvolution &);

  /// builds a grid along a given angle and number of points
  template<SpatialDirection, SpatialDirection, SpatialDirection>
  void buildGrid();
     
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

private:
  /// GridData
  GridData surface;
  ///
  Vector min;
  ///
  Vector max;
  /// random height at which to be cut
  Real height;
  /// random angle of the grid_data
  Real degree;
  /// Grid sizeadd python code to pybind compiled pythn binding
  UInt size;
  /// Plane direction from which angle is measured 
  SpatialDirection direction;
  /// dimensions of the surface
  Vector dimensions;
  
};

/* -------------------------------------------------------------------------- */
inline void SurfaceGenerator::dump(const std::string filename) {
  this->surface.dump(filename, direction);
}


/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */

#endif
