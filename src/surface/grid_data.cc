#include "grid_data.hh"
/* -------------------------------------------------------------------------- */  
namespace xpace {

/* -------------------------------------------------------------------------- */
GridData::GridData(UInt grid_size) {
  this->size = grid_size;
  this->data.resize(size*size);
  this->grid.resize(size*size*dim);
  this->offset = dim + 1;
}

/* -------------------------------------------------------------------------- */
GridData::GridData(UInt m_size, const Real* m_data) {

  XPACE_ASSERT(m_size != 0,
    "Surface doesnot have any data");
  
  this->size = m_size;
  this->data.resize(this->size*this->size);
  this->grid.resize(this->size*this->size * dim);
  this->offset = dim + 1;
  
  if (m_data != NULL) {
    for (UInt i = 0; i < m_size * m_size; ++i) {
      this->data[i] = m_data[i];
    }
  }
}

/* -------------------------------------------------------------------------- */
void GridData::resize(UInt & size) {
  this->size = size;
  this->data.resize(size*size);
  this->grid.resize(size*size*dim);
  this->offset = dim + 1;
}


/* -------------------------------------------------------------------------- */
/*Vector GridData::getPoint(UInt & i, SpatialDirection direction) {

  Vector point;
  
  switch (direction) {
  case _x: {
    point[_y] = grid[i*dim    ];
    point[_z] = grid[i*dim + 1];
    point[_x] = data[i        ];
    break;
  }
  case _y: {
    point[_z] = grid[i*dim    ];
    point[_x] = grid[i*dim + 1];
    point[_y] = data[i        ];
    break;
  }
  default:
    point[_x] = grid[i*dim    ];
    point[_y] = grid[i*dim + 1];
    point[_z] = data[i        ];
    break;
  }

  return point;
  }*/

/* -------------------------------------------------------------------------- */
void GridData::addToData(std::vector<Real> & m_data) {
  this->size = m_data.size();
  this->data.resize(this->size*this->size);
  this->grid.resize(this->size*this->size * dim);
  this->offset = dim + 1;
  
  if (m_data.size() != 0) {
    for (UInt i = 0; i < this->size * this->size; ++i) {
      this->data[i] = m_data[i];
    }
  }
}

/* -------------------------------------------------------------------------- */
void GridData::buildGrid(std::vector<Real> & x, std::vector<Real> & y) {
  this->size = x.size();
  
  std::cerr << " ----- Building Grid --------- " << std::endl;
  
  for (auto && grid: this->indexedGrid()) {
    UInt i = std::get<0>(grid);
    UInt j = std::get<1>(grid);
    std::get<2>(grid) = x[i];
    std::get<3>(grid) = y[j];
  }

  for (UInt i = 0; i < (size - 1); ++i) {
    for (UInt j = size*i; j < size*i + (size - 1); ++j) {
      this->addToConnectivity(j     , j + 1            , j + size);
      this->addToConnectivity(j + 1 , j + size + 1     , j + size);
    }
  }
}

/* -------------------------------------------------------------------------- */
void GridData::buildDataFromPlane(Plane & plane, SpatialDirection direction) {

  Vector point;
  for (UInt i = 0; i < size*size; ++i) {
    this->getGridPoint(i, point);
    switch (direction) {
    case _x: {
      data[i] = plane.coordinate<_x>(point);
      break;
    }
    case _y: {
      data[i] = plane.coordinate<_y>(point);
      break;
    }
    default:
      data[i] = plane.coordinate<_z>(point);
      break;
    }
  }
}


/* -------------------------------------------------------------------------- */
void GridData::dump(const std::string filename, SpatialDirection dir) {

  std::cerr << "------ Dumping Surface ------- " << std::endl;
  
  std::stringstream ss;

  ss << filename << ".vtk";
  
  std::ofstream os(ss.str().c_str());

  if (os.is_open() == false) {
    std::cerr << "cannot open file" << filename << std::endl;
    throw;
  } 

  os << "# vtk DataFile Version 2.0" << std::endl;
  os << "Voxelisation Data Visualization" << std::endl;
  os << "ASCII" << std::endl;
  os << "DATASET UNSTRUCTURED_GRID" << std::endl;

  os << "POINTS " << size*size << " float " << std::endl;

  for (UInt i = 0; i < size*size; ++i) {    
    switch (dir) {
    case _x: {
      os << data[i] << " " << grid[i*dim] << " " << grid[i*dim+1] << "\n";
      break;
    }
    case _y: {
      os << grid[i*dim+1] << " " << data[i] << " " << grid[i*dim] << "\n";
      break;
    }
    default: {
      os << grid[i*dim] << " " << grid[i*dim+1] << " " << data[i] << "\n";
      break;
    }
    }
  }

  UInt nb_elements = (size-1)*(size-1)*2;
    
  os << "CELLS " << nb_elements << " " << nb_elements*4 << std::endl;

  for (UInt i = 0; i < nb_elements*offset; i+=offset) {
    os << "3 ";
    os << connectivities[i] << " " << connectivities[i+1] << " " << connectivities[i+2] << std::endl;
  }
  
  os << "CELL_TYPES " << nb_elements << std::endl;
  for (UInt i =0 ; i < nb_elements; ++i) {
    os << "5 \n";
  }

  os << "CELL_DATA " << nb_elements << std::endl;

}

}

