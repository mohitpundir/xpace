#ifndef __POLYHEDRON_FACTORY__HH__
#define __POLYHEDRON_FACTORY__HH__
/* -------------------------------------------------------------------------- */
#include "shape_factory_interface.hh"
#include "polyhedron.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class PolyhedronFactory : public ShapeFactoryInterface {

private:
  PolyhedronFactory() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  std::unique_ptr<Shape> createShape() override;
  
  static ShapeFactoryInterface& getInstance();
 

};

}


#endif
