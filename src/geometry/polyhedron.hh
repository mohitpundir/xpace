#ifndef __POLYHEDRON_HH__
#define __POLYHEDRON_HH__
/* ------------------------------------------------------------------------ */
#include "shape.hh"
/* ------------------------------------------------------------------------ */
#include <tuple>
#include <algorithm>
#include <random>
/* ------------------------------------------------------------------------ */
#include <libqhullcpp/Qhull.h>
#include <libqhullcpp/QhullPoint.h>
#include <libqhullcpp/RboxPoints.h>
#include <libqhullcpp/QhullFacetList.h>
#include <libqhullcpp/QhullFacet.h>
#include <libqhullcpp/QhullVertexSet.h>
#include <libqhullcpp/QhullVertex.h>
/* -------------------------------------------------------------------------- */


/* ------------------------------------------------------------------------ */
namespace xpace {
/* ------------------------------------------------------------------------ */


class Polyhedron : public Shape {
    

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  /// Default Constructor
  Polyhedron() : Shape() {}
  /// Default Destructor
  ~Polyhedron() {
  }

private:
  /* ------------------------------------------------------------------------ */
  inline void computePoints();
  
public: 
      
  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & shape) {
    auto polyhedron = static_cast<Polyhedron&>(shape);
    
    Real minimum  = this->radius + polyhedron.getRadius();
    Real distance = (this->center - polyhedron.getCenter()).norm();

    if (distance < minimum) {      
      return this->intersectsBoundingBox(polyhedron);
    }
    return false;
  }

  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & other_shape, Real & tolerance) {
    auto other_polyhedron = static_cast<Polyhedron&>(other_shape);
    Real minimum  = this->radius + other_polyhedron.getRadius();
    Real distance = (this->center - other_polyhedron.getCenter()).norm();

    //minimum += tolerance * this->radius;
    
    if (distance < minimum) {
      return this->intersectsBoundingBox(other_polyhedron);
    }
    return false;
  }

  
  /* ------------------------------------------------------------------------ */
  virtual inline bool intersect(Plane & plane) {
    bool point_above;
    UInt nb_vertex_above   = 0;
    UInt nb_vertex_below   = 0;
  
    for (UInt i = 0; i < this->nb_vertices; ++i) {
      Vector vertex(this->points[i*strides + 0],
		    this->points[i*strides + 1],
		    this->points[i*strides + 2]);

      point_above = plane.above(vertex);
      
      if (point_above) 
	nb_vertex_above++;
      else
	nb_vertex_below++;
    }

    if (nb_vertex_above == vertices.size() || nb_vertex_below == vertices.size()) { 
      return false;
    }
    else {
      return true;
    }
  }
  
  /* ------------------------------------------------------------------------ */
  virtual inline std::tuple<bool,UInt> contains(Vector & point) {
    Real epsilon = std::numeric_limits<Real>::epsilon();

    UInt nb_coords = 0;
    UInt index = 0;
    for (UInt i= 0; i < 3; i++) { 
      if (  point[i] >= this->min[i] && point[i] <= this->max[i])
	nb_coords++;
    }
    
    if (nb_coords == 3) {
      return std::make_tuple(true, index);
    }
    return std::make_tuple(false, index);
  }
  
  /* ------------------------------------------------------------------------ */
  virtual inline void create(Real & radius, Vector & center) {

    UInt dim = 3;
 
    this->radius = radius;
    this->center = center;

    if (this->points.size() == 0) {
      this->computePoints();
    }

    this->nb_vertices = this->points.size() / dim;

    qhull = std::make_shared<orgQhull::Qhull>();
    qhull->runQhull("", dim, this->nb_vertices, this->points.data(), "Qt");
  
    this->volume = qhull->volume();
    
    this->requires_computation = true;
    this->computeBoundingBox();

    this->centers.clear();
    centers.push_back(this->center);
  }
  
  /* ------------------------------------------------------------------------ */
  virtual inline void translate(const Vector & vector) {
    this->center += vector;
    
    for (UInt n = 0; n < this->nb_vertices; ++n) {
      for (UInt s = 0; s < strides; ++s) {
	this->points[n*strides + s] += vector[s];
      }
    }

    this->max += vector;
    this->min += vector;
    this->increment += vector;

    this->centers.clear();
    centers.push_back(this->center);
  }

  /* ------------------------------------------------------------------------ */
  virtual inline void printself(std::ostream & stream) {
    Shape::printself(stream);
  }
  
  /* ------------------------------------------------------------------------ */
  virtual inline Real computeProjection(Vector & point, bool & center_above, UInt & index) {
    Vector             dummy;
    bool              _contains;
    Real              _projection;
    Real              _height      = point[2]; 
    std::vector<Real>  projections;

    orgQhull::QhullFacetList facetlist = this->qhull->facetList();
    auto facets = facetlist.toStdVector();
 
    for (auto & facet : facets ) {

      auto qh_normal = facet.hyperplane().coordinates();   
      Vector normal(qh_normal[0], qh_normal[1], qh_normal[2]);

      orgQhull::QhullVertexSet vSet = facet.vertices();
      auto vertices = vSet.toStdVector();
      auto qh_coord = vertices[0].point().coordinates();

      Vector coord0(qh_coord[0], qh_coord[1], qh_coord[2]);
      auto offset = - normal.dot(coord0);
    
      Real q = -(normal[0] * point[0] + normal[1] * point[1] + offset) / normal[2];

      dummy[0] = point[0];
      dummy[1] = point[1];
      dummy[2] = q;

      bool inside = insideFacet(facet, dummy);
      if (inside) {
	projections.push_back(q);
      }
    }

    Real max_projection;
    Real min_projection;
  
    if (projections.size() != 0) {
      max_projection = *std::max_element(projections.begin(), projections.end());
      min_projection = *std::min_element(projections.begin(), projections.end());
    }
    else {
      _height = point[2];
      return _height;
    }
    
    if (!center_above) {
      if (point[2] < max_projection && point[2] > min_projection) {
	_height = max_projection;
      }
      else if (point[2] < max_projection && point[2] < min_projection) {
	_height = max_projection;
      }
      else {
	_height = point[2];
      }
    }

    if (center_above) {
      if (point[2] < max_projection && point[2] > min_projection) {
	_height = min_projection;
      }
      else if (point[2] > max_projection && point[2] > min_projection) {
	_height = min_projection;
      }
      else {
	_height = point[2];
      }  
    }

    return _height;
  }

  /* -------------------------------------------------------------------------- */
  inline bool intersectsBoundingBox(Polyhedron &);
  /* ------------------------------------------------------------------------ */
  inline std::tuple<bool, UInt> intersect(std::vector<Polyhedron *> );
  /* ------------------------------------------------------------------------ */
  inline std::vector<Real>& getVertices();
  /* ------------------------------------------------------------------------ */
  inline std::vector<UInt>& getConnectivity();
  /* -------------------------------------------------------------------------- */
  inline bool insideFacet(orgQhull::QhullFacet & facet, Vector &);
  /* ------------------------------------------------------------------------ */
  inline void computeBoundingBox();
  /* -------------------------------------------------------------------------- */
  inline void reCompute();
  
  /* ------------------------------------------------------------------------ */
  /* Data Accessor                                                            */
  /* ------------------------------------------------------------------------ */
  XPACE_ACCESSOR(max     , Vector   , Max);
  XPACE_ACCESSOR(min     , Vector   , Min);
  XPACE_ACCESSOR(points  , std::vector<Real>,                Points);
    
private:
  ///
  bool requires_computation;
  /// connectivities of facets
  std::vector<UInt> connectivity;
  /// vertices
  std::vector<std::vector<Real> > vertices;
  /// points of polyhedron
  std::vector<Real> points;
  ///
  UInt nb_vertices;
  /// equations
  std::vector<std::vector<Real> > equations;  
  /// strides
  UInt strides = 3;
  /// minimum point of bounding box
  Vector min;
  /// maximum point of bounding box
  Vector max;
  /// computes the cumulative increment
  Vector increment;
  /// qhull object 
  std::shared_ptr<orgQhull::Qhull> qhull;

  //orgQhull::Qhull * qhull;
};


/* ------------------------------------------------------------------------ */
inline void Polyhedron::computeBoundingBox() {

  for (UInt i = 0; i < strides; ++i) {
    this->min[i] = this->points[i];
    this->max[i] = this->points[i];
  }
  
  for (UInt n = 0; n < this->nb_vertices; ++n) {
    for (UInt s = 0; s < strides; ++s) {
      this->min[s] = std::min(this->min[s], this->points[n*strides + s]);
      this->max[s] = std::max(this->max[s], this->points[n*strides + s]);
    }
  }
  
}

/* ------------------------------------------------------------------------ */
inline void Polyhedron::reCompute() {

  this->requires_computation = false;
}


/* ------------------------------------------------------------------------ */
inline void Polyhedron::computePoints() { 
  
  std::random_device rd;         // Will be used to obtain a seed for the random number engine
  std::mt19937 generator(rd());  // Standard mersenne_twister_engine seeded with rd()
  
  std::uniform_int_distribution<> distribution(8, 20);

  this->nb_vertices = distribution(generator);
    
  std::uniform_real_distribution<> dist_angle (  0., 2*M_PI);
  std::uniform_real_distribution<> dist_shrink(  1., 1.    );
  
  for (UInt i = 0; i < this->nb_vertices; ++i) {
        
    auto theta     = dist_angle ( generator );
    auto varphi    = dist_angle ( generator );
    auto shrinkage = dist_shrink( generator );

    this->points.push_back(sin(theta) * cos(varphi) * this->radius * shrinkage
			   + this->center[0]);
    this->points.push_back(sin(theta) * sin(varphi) * this->radius / sqrt(shrinkage)
			   + this->center[1]);
    this->points.push_back(cos(theta) * this->radius / sqrt(shrinkage)
			   + this->center[2]);
  }
}

/* ------------------------------------------------------------------------ */
inline std::tuple<bool, UInt> Polyhedron::intersect(std::vector<Polyhedron *> polyhedrons) {

  auto status = std::make_tuple(false, 0);

  UInt size = polyhedrons.size();
  bool intersects = false;
  for (UInt i = 0; i < size; ++i) {
    intersects = this->intersect(*polyhedrons[i]);
    if (intersects) {
      std::get<0>(status) = true;
      std::get<1>(status) = i;
      return status;
    }
  }
  return status;
}

/* -------------------------------------------------------------------------- */
inline bool Polyhedron::intersectsBoundingBox(Polyhedron & polyhedron) {

  Vector query;
  bool inside = false;
  UInt index;
  for (UInt n = 0; n < this->nb_vertices; ++n) {
    for (UInt s = 0; s < strides; ++s) {
      query[s] = this->points[n*strides + s];
    }

    std::tie(inside, index) = polyhedron.contains(query);
    if (inside) {
      return true;
    }
  }
  
  return false;
}

/* ------------------------------------------------------------------------ */
inline std::vector<Real>& Polyhedron::getVertices() {
  return this->points;
}

/* ------------------------------------------------------------------------ */
inline std::vector<UInt> & Polyhedron::getConnectivity() {

  orgQhull::QhullFacetList facetlist = qhull->facetList();
  auto facets = facetlist.toStdVector();
  
  for (auto & facet : facets ) {
      orgQhull::QhullVertexSet vSet = facet.vertices();
      auto it  = vSet.begin();
      auto end = vSet.end(); 
      for (; it != end; it++) {
	orgQhull::QhullVertex v = *it;
	orgQhull::QhullPoint  p = v.point(); 
	this->connectivity.push_back(p.id());
      }
    }
  
  return this->connectivity;
}



/* -------------------------------------------------------------------------- */
inline bool Polyhedron::insideFacet(orgQhull::QhullFacet & facet, Vector & query) {
  orgQhull::QhullVertexSet vSet = facet.vertices();

  auto vertices = vSet.toStdVector(); 
  auto qh_coord0 = vertices[0].point().coordinates();
  auto qh_coord1 = vertices[1].point().coordinates();
  auto qh_coord2 = vertices[2].point().coordinates();
  
  Vector coord0(qh_coord0[0], qh_coord0[1], qh_coord0[2]);
  Vector coord1(qh_coord1[0], qh_coord1[1], qh_coord1[2]);
  Vector coord2(qh_coord2[0], qh_coord2[1], qh_coord2[2]);
  
  auto v0 = coord1 - coord0;
  auto v1 = coord2 - coord0;
  auto v2 = query  - coord0;
      
  auto dot00 = v0.dot(v0);
  auto dot01 = v0.dot(v1);
  auto dot02 = v0.dot(v2);
  auto dot11 = v1.dot(v1);
  auto dot12 = v1.dot(v2);
    
  auto invDenom = 1. / (dot00 * dot11 - dot01 * dot01);
  auto u = (dot11 * dot02 - dot01 * dot12) * invDenom;
  auto v = (dot00 * dot12 - dot01 * dot02) * invDenom;
  
  return (u >= 0) && (v >= 0) && (u + v <= 1);
}



/* ------------------------------------------------------------------------ */
}
/* ------------------------------------------------------------------------ */


#endif
