#include "polyhedron_factory.hh"
#include "polyhedron.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

/* -------------------------------------------------------------------------- */
std::unique_ptr<Shape> PolyhedronFactory::createShape() {
  return std::make_unique<Polyhedron>();
}

/* -------------------------------------------------------------------------- */

ShapeFactoryInterface& PolyhedronFactory::getInstance() {
  if (not ShapeFactoryInterface::factory) {
    ShapeFactoryInterface::factory = new PolyhedronFactory;
  }

  return *factory;
}

}
