#include "sphere_factory.hh"
#include "sphere.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

/* -------------------------------------------------------------------------- */
std::unique_ptr<Shape> SphereFactory::createShape() {
  return std::make_unique<Sphere>();
}

/* -------------------------------------------------------------------------- */

ShapeFactoryInterface& SphereFactory::getInstance() {
  if (not ShapeFactoryInterface::factory) {
    ShapeFactoryInterface::factory = new SphereFactory;
  }

  return *factory;
}

}
