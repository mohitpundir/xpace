#ifndef __PLANE__HH__
#define __PLANE__HH__

/* -------------------------------------------------------------------------- */
#include <array>
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "point.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */

class Plane {
 
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:

  ///
  Plane() {}
  ///
  ~Plane() {}     

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:

  /* -------------------------------------------------------------------------- */
  template<SpatialDirection direction>
  inline void create(const Real & degree, const Real & height) {

    Vector point;
    point[direction] = height;
    
    switch (direction) {
    case _x: {
      this->normal[1] = 0;
      this->normal[2] = -sin(degree * M_PI/180);
      this->normal[0] =  cos(degree * M_PI/180);
      break;
    }
    case _y: {
      this->normal[2] = 0;
      this->normal[0] = -sin(degree * M_PI/180);
      this->normal[1] =  cos(degree * M_PI/180);
      break;
    }

    default:
      this->normal[0] = 0;
      this->normal[1] = -sin(degree * M_PI/180);
      this->normal[2] =  cos(degree * M_PI/180);
      break;
    }
        
    this->d = -this->normal.dot(point);  
  }
  
  /* -------------------------------------------------------------------------- */
  template<typename T>
  inline bool intersect(T & geometry) {
    Real distance  = this->distance(geometry->getCenter() );
    if (std::abs(distance) < geometry->getRadius() ) {
      return true;
    }
    return false;
  }

  /* -------------------------------------------------------------------------- */
  inline Real distance(Vector & point) {
    Real num = this->normal.dot(point) + d;
    Real den = this->normal.squaredNorm();
    den = sqrt(den);
    return num/den;
  }

  /* -------------------------------------------------------------------------- */
  template<SpatialDirection direction>
  inline Real coordinate(Vector & point) {

    switch (direction) {
    case _x: {
      return (-d - normal[1]*point[1] -normal[2]*point[2])/normal[0];
    }
    case _y: {
      return (-d - normal[0]*point[0] -normal[2]*point[2])/normal[1];
    }
    default:
      return (-d - normal[0]*point[0] -normal[1]*point[1])/normal[2];
    }
  }

  /* -------------------------------------------------------------------------- */
  template<SpatialDirection direction>
  inline void coordinate(Real & i, Real & j, Real & k) {

    switch (direction) {
    case _x: {
      k = (-d - normal[1]*i -normal[2]*j)/normal[0];
      break;
    }
    case _y: {
      k = (-d - normal[0]*i -normal[2]*j)/normal[1];
      break;
    }
    default:
      k = (-d - normal[0]*i -normal[1]*j)/normal[2];
      break;
    }
  }

  /* -------------------------------------------------------------------------- */
  inline bool above(Vector & point) {

    Real position = this->normal.dot(point) + d;

    if (position >= 0) {
      return true;
    }

    return false;
  }

  /* -------------------------------------------------------------------------- */
  inline bool above(std::vector<Real> & point) {

    Vector pt;
    for (UInt i = 0; i < point.size(); ++i) {
      pt[i] = point[i];
    }
    
    Real position = this->normal.dot(pt) + d;

    if (position >= 0) {
      return true;
    }

    return false;
  }

  
private:
  /// normal to the plane
  Vector normal;
  /// constant term d = -(a*x0 + b*y0 + c*z0)
  Real d; 
};

/*-------------------------------------------------------------------------- */ 
}
/*-------------------------------------------------------------------------- */
  

#endif
