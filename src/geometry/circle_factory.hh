#ifndef __CIRCLE_FACTORY__HH__
#define __CIRCLE_FACTORY__HH__

/* -------------------------------------------------------------------------- */
#include "shape_factory_interface.hh"
#include "shape.hh"
#include "circle.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class CircleFactory :  public ShapeFactoryInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructor                                                   */
  /* ------------------------------------------------------------------------ */
private:
  CircleFactory() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  std::unique_ptr<Shape> createShape() override;
  
  static ShapeFactoryInterface& getInstance();

};

}

#endif
