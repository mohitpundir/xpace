#include "shape_factory_interface.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

ShapeFactoryInterface& ShapeFactoryInterface::getInstance() {
  return *factory;
}


/* -------------------------------------------------------------------------- */
ShapeFactoryInterface* ShapeFactoryInterface::factory = nullptr;

}
