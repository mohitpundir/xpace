#ifndef __SPHERE_FACTORY__HH__
#define __SPHERE_FACTORY__HH__

/* -------------------------------------------------------------------------- */
#include "shape_factory_interface.hh"
#include "shape.hh"
#include "sphere.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class SphereFactory :  public ShapeFactoryInterface {
  /* ------------------------------------------------------------------------ */
  /* Constructor/Destructor                                                   */
  /* ------------------------------------------------------------------------ */
private:
  SphereFactory() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  std::unique_ptr<Shape> createShape() override;
  
  static ShapeFactoryInterface& getInstance();

};

}

#endif
