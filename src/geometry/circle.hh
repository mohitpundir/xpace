#ifndef __CIRCLE__HH__
#define __CIRCLE__HH__
/* ------------------------------------------------------------------------ */
#include "shape.hh"
/* ------------------------------------------------------------------------ */
#include <tuple>
/* ------------------------------------------------------------------------ */


namespace xpace {


class Circle : public Shape {
  
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  
  Circle() : Shape() {}

  ~Circle() = default;

  /* ------------------------------------------------------------------------ */
  /* Inline Functions : Public                                                */
  /* ------------------------------------------------------------------------ */
public:
  
  /* ------------------------------------------------------------------------ */
  inline bool intersect(Circle & );

  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & shape, Vector & dimensions) {
    auto circle = static_cast<Circle&>(shape);
    Real minimum  = this->radius + circle.getRadius();
    Real distance = (this->center - circle.getCenter()).norm();

    if (distance < minimum) {
      return true;
    }

    return false;
  }
  
  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & other_shape, Real & tolerance) {
    auto other_circle = static_cast<Circle&>(other_shape);
    Real minimum  = this->radius + other_circle.getRadius();
    Real distance = (this->center - other_circle.getCenter()).norm();

    minimum += tolerance * this->radius;
    
    if (distance < minimum) {
      return true;
    }
    return false;

  }

  /* -------------------------------------------------------------------------- */
  virtual inline void intersect(Shape & other_shape, Real & tolerance,
				std::vector<bool> & intersects) {

    Real distance;
    std::vector<Real> tolerances;
    
    auto circle = static_cast<Circle&>(other_shape);
    Real minimum  = this->radius + circle.getRadius();

    auto & other_centers =  circle.getCenters();
    for (auto c1 : this->centers) {
      for (auto c2 : other_centers) {

	distance = (c1 - c2).norm();
	tolerance = distance / minimum;
	
	if (distance < minimum) {
	  intersects.push_back(true);
	  tolerances.push_back(tolerance);
	}
      }
    }

    if (tolerances.size() > 0) {
      tolerance = *max_element(tolerances.begin(), tolerances.end());
    }
    else {
      tolerance = std::numeric_limits<Real>::max();
    }
  }

  
  /* ------------------------------------------------------------------------ */
  virtual inline bool intersect(Plane & plane) {
    for (auto c: centers) {
      Real distance = plane.distance(c);
      if (std::abs(distance) < this->radius) {
	return true;
      }
    }
    return false;
  }

  /* ------------------------------------------------------------------------ */
  virtual inline std::tuple<bool,UInt> contains(Vector & point) {

    Real distance;
    UInt index = 0;
    for (auto c : this->centers) {
      distance = (c - point).norm();
      if (std::abs(distance) <= this->radius) {
	return std::make_tuple(true, index);
      }
      ++index;
    }

    return std::make_tuple(false, index);
  }

  /* ------------------------------------------------------------------------ */
  virtual inline bool contains(Vector & point, Real tolerance) {

    Real distance;
    
    for (auto c : this->centers) {
      distance = (c - point).norm();
      if (std::abs(distance) <= this->radius + tolerance) {
	return true;
      }
    }

    return false;
  }

  
  /* ------------------------------------------------------------------------ */
  virtual inline void create(Real & radius, Vector & center) {
    this->radius = radius;
    this->center = center;
    this->volume = M_PI * pow(this->radius, 2.0);
    this->centers.clear();
    centers.push_back(this->center);
  }
  
  /* ------------------------------------------------------------------------ */
  virtual inline void translate(const Vector & vector) {
    this->center += vector;
    this->centers.clear();
    centers.push_back(this->center);
  }
 
  /* ------------------------------------------------------------------------ */
  inline std::tuple<bool, UInt> intersect(std::vector<Circle *> );

  /* ------------------------------------------------------------------------ */
  virtual inline void printself(std::ostream & stream) {
    Shape::printself(stream);
  }

   /* ------------------------------------------------------------------------ */

  virtual inline Real computeProjection(Vector & point, bool & center_above, UInt & index) {
    Real projection;
    
    projection = sqrt(this->radius*this->radius - pow(point[0] - centers[index][0], 2) - pow(point[1] - centers[index][1], 2));

     
    if (center_above) {
      point[2] = centers[index][2] - projection;
      return centers[index][2] - projection;
    }
    else {
      point[2] = centers[index][2] + projection;
      return centers[index][2] + projection;
    }
  }
};

/* ------------------------------------------------------------------------ */
inline bool Circle::intersect(Circle & circle) {
  Real minimum  = this->radius + circle.getRadius();
  Real distance = (this->center - circle.getCenter()).norm();
  
  if (distance < minimum) {
    return true;
  }
  return false;
}

/* ------------------------------------------------------------------------ */

inline std::tuple<bool, UInt> Circle::intersect(std::vector<Circle *> spheres) {

  auto status = std::make_tuple(false, 0);

  UInt size = spheres.size();
  bool intersects = false;
  for (UInt i = 0; i < size; ++i) {
    intersects = this->intersect(*spheres[i]);
    if (intersects) {
      std::cout << "Intersects" << std::endl;
      std::get<0>(status) = true;
      std::get<1>(status) = i;
      return status;
    }
  }
  return status;
}

/* ------------------------------------------------------------------------ */

inline std::ostream &operator<<(std::ostream & sstr, Circle & _this) {
  _this.printself(sstr);
  return sstr;
}


}

#endif
