#ifndef __SHAPE_FACTORY_INTERFACE__HH__
#define __SHAPE_FACTORY_INTERFACE__HH__

/* -------------------------------------------------------------------------- */
#include "shape.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class ShapeFactoryInterface {

protected:
  ///
  ShapeFactoryInterface() = default;

public:
  virtual ~ShapeFactoryInterface() = default;

public:
  virtual std::unique_ptr<Shape> createShape() = 0;

  static ShapeFactoryInterface & getInstance();

protected:
  static ShapeFactoryInterface * factory;
};

}

#endif
