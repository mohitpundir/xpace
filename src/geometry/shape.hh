#ifndef __SHAPE__HH__
#define __SHAPE__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "plane.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {


class Shape {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  
  Shape() = default;

  ~Shape() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */  
public:
  
  /* ------------------------------------------------------------------------ */
  virtual inline void printself(std::ostream & stream) {
    stream << "Center = " << this->center << std::endl;
    stream << "Radius = " << this->radius << std::endl;
    stream << "Volume = " << this->volume << std::endl;
  }

  inline Vector getCenter(UInt index) {
    return this->centers[index];
  }
  /* -------------------------------------------------------------------------- */
  virtual inline bool contains(Shape & othr_shape) {}
  
  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & other_shape, Vector & dimensions) {}

  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Shape & other_shape, Real & tolerance) {}

  /* -------------------------------------------------------------------------- */
  virtual inline void intersect(Shape & other_shape, Real & tolerance,
				std::vector<bool> & intersects) {}
  
  /* -------------------------------------------------------------------------- */
  virtual inline bool intersect(Plane & plane) {}

  /* -------------------------------------------------------------------------- */
  virtual inline std::tuple<bool,UInt> contains(Vector & point) {}

  /* -------------------------------------------------------------------------- */
  virtual inline bool contains(Vector & point, Real & tolerance) {}
  
  /* -------------------------------------------------------------------------- */
  virtual inline void create(Real & radius, Vector & center) {}

  /* -------------------------------------------------------------------------- */
  virtual inline void translate(const Vector & vector) {}
  
  /* -------------------------------------------------------------------------- */
  virtual inline Real computeProjection(Vector & point,
					bool & center_above, UInt & index) {}
  
  /* ------------------------------------------------------------------------ */
  /* Data Accessor                                                            */
  /* ------------------------------------------------------------------------ */
  XPACE_ACCESSOR(center  , Vector       , Center);
  XPACE_ACCESSOR(radius  , Real         , Radius);
  XPACE_ACCESSOR(volume  , Real         , Volume);
  XPACE_ACCESSOR(centers, std::vector<Vector>, Centers);

  
  /* ------------------------------------------------------------------------ */
  /* Data Members: Protected                                                  */
  /* ------------------------------------------------------------------------ */
protected:
  /// radius of the shape
  Real radius;
  /// volume of the shape
  Real volume;
  /// center of the shape
  Vector center;
  /// periodic centers
  std::vector<Vector> centers;   
};


/* -------------------------------------------------------------------------- */
inline std::ostream &operator<<(std::ostream &sstr, Shape &_this) {
  _this.printself(sstr);
  return sstr;
}


}

#endif
