#include "circle_factory.hh"
#include "circle.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

/* -------------------------------------------------------------------------- */
std::unique_ptr<Shape> CircleFactory::createShape() {
  return std::make_unique<Circle>();
}

/* -------------------------------------------------------------------------- */

ShapeFactoryInterface& CircleFactory::getInstance() {
  if (not ShapeFactoryInterface::factory) {
    ShapeFactoryInterface::factory = new CircleFactory;
  }

  return *factory;
}

}
