#ifndef __POINT_HH__
#define __POINT_HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */


/// Class representing a point in space
template <UInt dim>
class Point {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  
  Point() {
    for (UInt i=0; i < dim; i++)
      coords[i] = 0.;
  }
  
  Point(Real point[dim]) {
    for (UInt i=0; i < dim; i++)
      coords[i] = point[i];
  }

  Point(UInt m_size, const Real * m_data) {
    if (m_data != NULL) {
      for (UInt i = 0; i < m_size; ++i) {
	coords[i] = m_data[i];
      }
    } 
  }

  Point(Real x, Real y, Real z)  {
    coords[0] = x;
    coords[1] = y;
    coords[2] = z;
  }

  ~Point() =default;  
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
  
public:
  /* -------------------------------------------------------------------------- */
  inline Real x () const {
    return coords[0];
  }

  /* -------------------------------------------------------------------------- */
  inline Real y () const {
    return coords[1];
  }

  /* -------------------------------------------------------------------------- */
  inline Real z () const {
    return coords[2];
  }

  /* -------------------------------------------------------------------------- */
  inline Real operator[] (UInt index) const {
    return coords[index];
  }

  /* -------------------------------------------------------------------------- */
  inline Real& operator[] (UInt index) {
    return coords[index];
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator*=(const Real & scalar) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] *= scalar;
    }
    return *this;
  }
  
  /* -------------------------------------------------------------------------- */
  inline Point& operator/=(const Real & scalar) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] /= scalar;
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator+=(const Point & p) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] += p.coords[i];
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator+=(const double * p) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] += p[i];
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator-=(const Point & p) {
    for (UInt i =0 ; i < dim; i++) {
      coords[i] -= p.coords[i];
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator=(const Point & p) {
    for (UInt i = 0; i < dim; i++) {
      coords[i] = p.coords[i];
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Point& operator=(Real s) {
    for (UInt i = 0; i < dim; ++i) {
      coords[i] == s;
    }
    return *this;
  }

  /* -------------------------------------------------------------------------- */
  inline Real distance(const Point & p) {
    Real distance = 0.0;
    for (UInt i = 0; i < dim; i++) {
      distance += pow(coords[i] - p.coords[i], 2);
    }

    return sqrt(distance);
  }

  /* -------------------------------------------------------------------------- */
  inline void normalize() {
    Real norm = this->norm();
    for (UInt i=0; i < dim; i++) {
      coords[i] /= norm;
    }    
  }

  /* -------------------------------------------------------------------------- */
  inline Real norm() {
    Real norm = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      norm += pow(coords[i], 2);
    }
    return sqrt(norm);    
  }

  /* -------------------------------------------------------------------------- */
  inline Real squaredNorm() {
    Real squared_norm = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      squared_norm += pow(coords[i], 2);
    }
    return squared_norm;
  }

  /* -------------------------------------------------------------------------- */
  inline Point direction(const Point & p) {

    Point<dim> dummy = p - *this;
    dummy.normalize();      
    return dummy;
  }
  
  /* -------------------------------------------------------------------------- */
  inline void initself(std::istream & sstr) {
    for (UInt i = 0; i < dim; i++)
      sstr >> coords[i];
  }

  /* -------------------------------------------------------------------------- */
  inline void printself(std::ostream & stream) const {
    for (UInt i= 0; i < dim -1 ; i++)
      stream << coords[i] << " ";
    stream << coords[dim - 1];
  }

  /* -------------------------------------------------------------------------- */
  inline Real size() {
    return dim;
  }

  /* -------------------------------------------------------------------------- */
  inline Real dot(const Point & p) {
    Real product = 0.0;
    for (UInt i = 0; i < dim; ++i) {
      product += this->coords[i] * p.coords[i];
    }
    return product;
  }

  /* ------------------------------------------------------------------------ */
  inline void cross(const Point & p, Point & dummy) {
    
    dummy[0] = this->coords[1]*p.coords[2] - this->coords[2]*p.coords[1];
    dummy[1] = this->coords[2]*p.coords[0] - this->coords[0]*p.coords[2];
    dummy[2] = this->coords[0]*p.coords[1] - this->coords[1]*p.coords[0];
  }

  /* -------------------------------------------------------------------------- */
  inline std::vector<size_t> getStrides(bool bytes) const {
    std::vector<size_t> ret(1);
    ret[0] = 1;

    if (bytes) {
      ret[0] *= sizeof(Real);
    }

    return ret;
  }

  /* -------------------------------------------------------------------------- */
  inline std::vector<size_t> getShape() const {
    std::vector<size_t> ret(1);
    ret[0] = dim;

    return ret;
  }

  /* -------------------------------------------------------------------------- */
  inline Point operator+(Point & q) {
    Point r(*this);
    return r += q;
  }

  inline Point operator-(Point & q) {
  Point r(*this);
  return r -= q;
  
  }

  inline Point operator*(const Real & scalar) {
    Point r(*this);
    return r *= scalar;
  }

  inline Point operator/(const Real & scalar) {
  Point r(*this);
  return r /= scalar;
}


  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
  
private:

  Real coords[dim];
};


/* -------------------------------------------------------------------------- */
inline std::ostream &operator<<(std::ostream & sstr, Point<3> & _this)
{
  _this.printself(sstr);
  return sstr;
}

/* -------------------------------------------------------------------------- */
inline std::istream &operator>>(std::istream & sstr, Point<3> & _this)
{
  _this.initself(sstr);
  return sstr;
}

/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */


#endif //__POINT_HH__
