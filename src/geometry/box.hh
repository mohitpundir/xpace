#ifndef __BOX__HH__
#define __BOX__HH__
/* -------------------------------------------------------------------------- */
#include "shape.hh"
/* -------------------------------------------------------------------------- */


namespace xpace{

class Box : public Shape {

public:

  Box(ArgMap attributes) : Shape() {
    this->dimensions = std::any_cast<Vector>(attributes["dimensions"]);
    this->spatial_dimension = std::any_cast<UInt>(attributes["spatial_dimension"]);
  }

  ~Box() = default;

public:

  virtual inline bool contains(Shape & shape) {
    auto sphere = static_cast<Sphere&>(shape);
    auto radius = sphere.getRadius();
    auto center = sphere.getCenter();

    for (UInt i = 0; i < spatial_dimension; ++i) {
      Real min = origin + radius;
      Real max = origin + dimensions[i] - radius;
      
    }
      
  }

  
private:
  /// dimensions of the box
  Vector dimensions;
  /// spatial dimension of box
  UInt spatial_dimension;
  
};

}


#endif
