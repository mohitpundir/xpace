#ifndef __DUMPER_HELPER__HH__
#define __DUMPER_HELPER__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "particle.hh"
#include "particles.hh"
#include "shape.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
#include <vtkSphereSource.h>
#include <vtkPolyhedron.h>
#include <vtkCubeSource.h>
#include <vtkPolyData.h>
#include <vtkIdList.h>
#include <vtkCellArray.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkNamedColors.h>
#include <vtkProperty.h>
#include <vtkXMLUnstructuredGridWriter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkAppendFilter.h>
/* -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */

using vtkPointsP                    = vtkSmartPointer<vtkPoints>;
using vtkSphereSourceP              = vtkSmartPointer<vtkSphereSource>;
using vtkPolyhedronSourceP          = vtkSmartPointer<vtkPolyhedron>;
using vtkCellArraySourceP           = vtkSmartPointer<vtkCellArray>;
using vtkUnstructuredGridP          = vtkSmartPointer<vtkUnstructuredGrid>;
using vtkHexahedronP                = vtkSmartPointer<vtkHexahedron>;
using vtkXMLUnstructuredGridWriterP = vtkSmartPointer<vtkXMLUnstructuredGridWriter>;
using vtkAppendFilterP              = vtkSmartPointer<vtkAppendFilter>;

class DumperHelper {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:

  DumperHelper();

  virtual ~DumperHelper();


private:
  /// creates spherical particles 
  vtkSphereSourceP makeSphere(Vector &, Real &);
  /// creates polyhedron particles
  vtkUnstructuredGridP makePolyhedron(std::vector<Real> &, std::vector<UInt>&, Real &);
  
  
public:
  ///
  inline void setBaseNameToDumper(std::string filename) {
    this->dumper_name = filename;
  }
  /// 
  template<typename T>
  void dump(std::vector<T*> &);
  ///
  template<typename T>
  void dump(std::vector<T*> &, UInt);
  ///
  template<typename T>
  void dumpToIndex(std::vector<T*> &, UInt);
  ///
  template<typename T>
  void write(std::vector<T*> &);
  ///
  void write(Particles<Shape *, Vector, Vector, Real, Real> &);
  ///
  template<typename T>
  void writeToIndex(std::vector<T*> &, UInt);
  
  
  
  
  
private:
  ///
  UInt step;
  ///
  std::string dumper_name;
};

/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */


#include "dumper_helper_tmpl.hh"
/* -------------------------------------------------------------------------- */
#endif
