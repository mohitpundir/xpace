#ifndef __PARTICLES_HH__
#define __PARTICLES_HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "shape.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */
#include <type_traits>
/* -------------------------------------------------------------------------- */

namespace xpace {


#define FOR_EACH_ARRAY(expression)					\
  size_t array_index = 0;						\
  int dummy[] = {							\
    (get_array<Props>(array_index)->expression, ++array_index, 0)...};	\
  (void)dummy

/* -------------------------------------------------------------------------- */

// Structure of Array
template<typename... Props>
class Particles {

public:
  /// Helper to deduce the type of the Nth property.
  template <size_t N>
  using NthTypeOf =
    typename std::tuple_element<N, std::tuple<Props...>>::type;
  
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
  Particles() {
    size_t array_index = 0;
    int dummy_init[] = {(new (get_array<Props>(array_index))
			 std::vector<Props>(),
                         ++array_index, 0)...};
    (void)dummy_init;  // avoids unused variable compiler warnings. 
  }

  ~Particles() {
    using std::vector;
    FOR_EACH_ARRAY(~vector());
  }

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  /// Adds a property to the end of the arrays via rvalue.
  void push(Props&&... elements) {
    FOR_EACH_ARRAY(emplace_back(std::forward<Props>(elements)));

    ++nb_particles;
  }

  /// Adds an element to the end of the arrays, as a copy of the const ref value.
  void push_back(const Props&... elements) {
    FOR_EACH_ARRAY(push_back(elements));

    ++nb_particles;
  }

  /// Returns a pointer to the |ArrayIndex|th array.
  template <size_t ArrayIndex>
  NthTypeOf<ArrayIndex>* array() {
    static_assert(ArrayIndex < nb_arrays, "Requested invalid array index.");

    using ElementType = NthTypeOf<ArrayIndex>;
    std::vector<ElementType>* array = get_array<ElementType>(ArrayIndex);

    return array->data();
  }

  // Returns a const pointer to the |ArrayIndex|th array.
  template <size_t ArrayIndex>
  const NthTypeOf<ArrayIndex>* array() const {
    static_assert(ArrayIndex < nb_arrays, "Requested invalid array index.");

    using ElementType = NthTypeOf<ArrayIndex>;
    const std::vector<ElementType>* array = get_array<ElementType>(ArrayIndex);

    return array->data();
  }

  /// Returns a refernce ti the |index|th element from the
  /// |ArrayIndex|th array as type |ElementType|
  template <size_t ArrayIndex>
  //NthTypeOf<ArrayIndex>& get(size_t index) {
  inline decltype(auto) get(size_t index) {  
    static_assert(ArrayIndex < nb_arrays,
                  "Requested invalid array index in get().");

    using ElementType = NthTypeOf<ArrayIndex>;
    std::vector<ElementType>* array = get_array<ElementType>(ArrayIndex);

    return (*array)[index];
  }
  
  /// Returns a const reference to the |index|th element from the |ArrayIndex|th
  // array as type |ElementType|.
  template <size_t ArrayIndex>
  inline const NthTypeOf<ArrayIndex>& get(size_t index) const {
    static_assert(ArrayIndex < nb_arrays,
                  "Requested invalid array index in get().");

    using ElementType = NthTypeOf<ArrayIndex>;
    const std::vector<ElementType>* array = get_array<ElementType>(ArrayIndex);

    return (*array)[index];
    }

  /// Returns the number of property arrays.
  inline size_t num_properties() const { return nb_arrays; }

  /// Returns the number of elements in the arrays.
  inline size_t size() const { return nb_particles; }

protected:
  template <class Type>
  inline std::vector<Type>* get_array(size_t array_index) {
    static_assert(sizeof(std::vector<Type>) == sizeof(std::vector<void*>),
                  "Structure of Array assumes of vector<Type> is same size as "
                  "vector<void*> however the assumption failed.");
    static_assert(alignof(std::vector<Type>) == alignof(std::vector<void*>),
                  "Structure of Array assumes of vector<Type> is same "
                  "alignment as vector<void*> however the assumption failed.");

    return reinterpret_cast<std::vector<Type>*>(&prop_arrays[array_index]);
  }
  
  template <class Type>
  inline const std::vector<Type>* get_array(size_t array_index) const {
    static_assert(sizeof(std::vector<Type>) == sizeof(std::vector<void*>),
                  "Structure of Array assumes of vector<Type> is same size as "
                  "vector<void*> however the assumption failed.");
    static_assert(alignof(std::vector<Type>) == alignof(std::vector<void*>),
                  "Structure of Array assumes of vector<Type> is same "
                  "alignment as vector<void*> however the assumption failed.");

    return reinterpret_cast<const std::vector<Type>*>(&prop_arrays[array_index]);
  }
  
  /* ------------------------------------------------------------------------ */
  /* Data Members: Protected                                                  */
  /* ------------------------------------------------------------------------ */
protected:
  /// number of property arrays
  static const size_t nb_arrays = sizeof...(Props);

  /// number of particles
  size_t nb_particles = 0;
  
  /// defining array type
  using ArrayType = std::aligned_storage<sizeof(std::vector<void*>),
					 alignof(std::vector<void*>)>::type;
  ArrayType prop_arrays[nb_arrays]; 
};

/* -------------------------------------------------------------------------- */
/* Derived classes for specific particles
/* It derives from Particles class 
/* Derived class should have a create function to create the particles 
/* -------------------------------------------------------------------------- */
class AggregateParticles : public Particles<std::unique_ptr<Shape>/* aggregate shape */,
					    Vector /* force    */,
					    Vector /* velocity */,
					    Real   /* mass */,
					    Real   /* energy */> {

  
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  AggregateParticles() = default;

  ~AggregateParticles() = default;

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  /// accessor to get shape for particle at index
  inline Shape* getShape(size_t index) {
    return this->get<0>(index).get();
  }

  /// accessor to get force for particle at index
  inline Vector & getForce(size_t index) {
    return this->get<1>(index);
  }

  /// accessor to get velocity for particle at index
  inline Vector & getVelocity(size_t index) {
    return this->get<2>(index);
  }

  /// accessor to get mass for particle at index
  inline Real & getMass(size_t index) {
    return this->get<3>(index);
  }  

  /// accessor to get energy for particle at index
  inline Real & getEnergy(size_t index) {
    return this->get<4>(index);
  }  

};

}

#endif 
