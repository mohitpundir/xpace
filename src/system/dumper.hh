#ifndef __DUMPER_HH__
#define __DUMPER_HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "particles.hh"
#include "system_statistics.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class Dumper{

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  Dumper() = default;

  ~Dumper() = default;

public:
  /// sets the base dumper name
  inline void setBaseNameToDumper(std::string filename) {
    this->dumper_name = filename;
    this->step = 0;
  }

  /// set the fequency of the dump
  inline void setDumpFrequency(UInt & freq) {
    this->freq = freq;
  }

  /// dump to txt format
  void dumpToTxt(AggregateParticles &, Vector &);

  /// dump to txt format
  void dumpToTxt(AggregateParticles &);

  /// dump to txt format
  void dumpToTxt(AggregateParticles &, UInt, UInt);
  
  /// dump to txt format
  void dumpToCsv();
  
  /// dump to vtk format
  void dumpToParaview();


public:
  ///
  std::vector<std::vector<Real> > getData() {
    return data;
  }
  
private:
  /// counter to keep track of number of times dump is called
  UInt step;

  /// frequency at which dumper should dump the particles
  UInt freq;

  ///
  std::vector<std::vector<Real> > data;
  
  /// base dumper name
  std::string dumper_name;
  
};


}

#endif
