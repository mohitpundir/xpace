#include "dumper.hh"
#include <iomanip>
/* -------------------------------------------------------------------------- */
#ifdef XPACE_USE_MPI
#include "hdf5.h"
#endif
/* -------------------------------------------------------------------------- */
namespace xpace {

/* -------------------------------------------------------------------------- */
#ifndef XPACE_USE_MPI

/* -------------------------------------------------------------------------- */
void Dumper::dumpToTxt(AggregateParticles &particles, Vector & dimensions) {

  std::stringstream filename;
  filename << dumper_name << "-00" << step << ".txt";

  std::ofstream os(filename.str());
  os << "#x y z radius fx fy fz vx vy vz contacts" << std::endl;

  UInt size = particles.size();

  for (UInt i = 0; i < size; i++) {

    auto centers = particles.getShape(i)->getCenters();
    auto force = particles.getForce(i);
    auto velocity = particles.getVelocity(i);

    UInt nb_contacts = 0;
    for (UInt j = 0; j < size; ++j) {
      if (i == j)
	continue;
      
      bool intersect =
	particles.getShape(i)->intersect(*particles.getShape(j), dimensions);
      if (intersect)
	nb_contacts++;
    }
    
    for (auto center : centers) {
      os << center[0] << " " << center[1] << " " << center[2] << " "
         << particles.getShape(i)->getRadius() << " " << force[0] << " "
         << force[1] << " " << force[2] << " " << velocity[0] << " "
         << velocity[1] << " " << velocity[2] << " " << nb_contacts <<std::endl;
    }
  }

  os.close();
  ++step;
}

/* -------------------------------------------------------------------------- */
void Dumper::dumpToTxt(AggregateParticles &particles) {

  std::stringstream filename;
  filename << dumper_name << "-00" << step << ".txt";

  std::ofstream os(filename.str());
  os << "#x y z radius fx fy fz vx vy vz contacts" << std::endl;

  UInt size = particles.size();

  Vector dimensions;
  for (UInt i = 0; i < size; i++) {

    auto centers = particles.getShape(i)->getCenters();
    auto force = particles.getForce(i);
    auto velocity = particles.getVelocity(i);

    
    UInt nb_contacts = 0;
    for (UInt j = 0; j < size; ++j) {
      if (i == j)
	continue;
      
       bool intersect =
	 particles.getShape(i)->intersect(*particles.getShape(j), dimensions);
      if (intersect)
	nb_contacts++;
    }
    
    for (auto center : centers) {
      os << center[0] << " " << center[1] << " " << center[2] << " "
         << particles.getShape(i)->getRadius() << " " << force[0] << " "
         << force[1] << " " << force[2] << " " << velocity[0] << " "
         << velocity[1] << " " << velocity[2] << " " << nb_contacts <<std::endl;
    }
  }

  os.close();
  ++step;
}

/* -------------------------------------------------------------------------- */
void Dumper::dumpToTxt(AggregateParticles &particles, UInt start, UInt end) {

  if (step % freq == 0) {

    std::stringstream filename;
    filename << dumper_name << "-00" << step << ".txt";

    std::ofstream os(filename.str());
    os << "#x y z radius fx fy fz vx vy vz energy contacts" << std::endl;

    /*for (UInt i = start; i < end; i++) {

      auto center = particles.getShape(i)->getCenter();
      auto force = particles.getForce(i);
      auto velocity = particles.getVelocity(i);

      UInt nb_contacts = 0;
      for (UInt j = start; j < end; ++j) {
        if (i == j)
          continue;

        bool intersect =
            particles.getShape(i)->intersect(*particles.getShape(j));
        if (intersect)
          nb_contacts++;
      }

      os << center[0] << " " << center[1] << " " << center[2] << " "
         << particles.getShape(i)->getRadius() << " " << force[0] << " "
         << force[1] << " " << force[2] << " " << velocity[0] << " "
         << velocity[1] << " " << velocity[2] << " " << particles.getEnergy(i)
         << " " << nb_contacts << std::endl;
	 }*/

    os.close();
  }

  ++step;
}

#endif

/* -------------------------------------------------------------------------- */
#ifdef XPACE_USE_MPI

void Dumper::dumpToTxt(AggregateParticles &particles) {

  std::stringstream filename;
  filename << dumper_name << "-" << std::setfill('0') << std::setw(4) << step
           << ".h5";

  hid_t plist_id;
  hid_t file_id, dset_id;
  hsize_t count[2]; /* hyperslab selection parameters */
  hsize_t offset[2];
  hid_t filespace, memspace;
  herr_t status;
  double *data;
  hsize_t dimsf[2];

  int prank, psize;
  MPI_Comm_rank(MPI_COMM_WORLD, &prank);
  MPI_Comm_size(MPI_COMM_WORLD, &psize);

  plist_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);

  file_id =
      H5Fcreate(filename.str().c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, plist_id);
  H5Pclose(plist_id);

  int local_size = particles.size();
  int row_sizes[psize];

  MPI_Allgather(&local_size, 1, MPI_INT, &row_sizes, 1, MPI_INT,
                MPI_COMM_WORLD);

  int global_size = 0;
  for (int i = 0; i < psize; i++) {
    global_size += row_sizes[i];
  }

  dimsf[0] = global_size;
  dimsf[1] = 4;
  filespace = H5Screate_simple(2, dimsf, NULL);

  dset_id = H5Dcreate(file_id, "Particles", H5T_NATIVE_DOUBLE, filespace,
                      H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
  H5Sclose(filespace);

  count[0] = local_size;
  count[1] = dimsf[1];

  offset[0] = 0;
  for (int i = 0; i < prank; i++) {
    offset[0] += row_sizes[i];
  }

  offset[1] = 0;

  int dataset_dim = 2;
  memspace = H5Screate_simple(dataset_dim, count, NULL);

  filespace = H5Dget_space(dset_id);
  H5Sselect_hyperslab(filespace, H5S_SELECT_SET, offset, NULL, count, NULL);

  data = (double *)malloc(sizeof(double) * count[0] * count[1]);

  int counter = 0;
  for (int i = 0; i < count[0]; i++) {
    auto center = particles.getShape(i)->getCenter();
    for (int j = 0; j < 3; j++) {
      data[counter] = center[j];
      counter++;
    }
    auto radius = particles.getShape(i)->getRadius();
    data[counter] = radius;
    counter++;
  }

  plist_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(plist_id, H5FD_MPIO_COLLECTIVE);

  status =
      H5Dwrite(dset_id, H5T_NATIVE_DOUBLE, memspace, filespace, plist_id, data);
  std::cerr << status << std::endl;
  free(data);

  H5Dclose(dset_id);
  H5Sclose(filespace);
  H5Sclose(memspace);
  H5Pclose(plist_id);
  H5Fclose(file_id);

  this->step++;
}
#endif

/* -------------------------------------------------------------------------- */
void Dumper::dumpToCsv() {

  std::stringstream filename;
  filename << dumper_name << "-"
           << "data"
           << ".csv";

  std::ofstream os(filename.str());

  for (auto d : this->data) {
    UInt i = 0;
    for (; i < d.size() - 1; i++) {
      os << d[i] << ",";
    }
    os << d[i] << std::endl;
  }

  os.close();
}

/* -------------------------------------------------------------------------- */
void Dumper::dumpToParaview() {}

}
