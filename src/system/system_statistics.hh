#ifndef __SYSTEM_STATISTICS__HH__
#define __SYSTEM_STATISTICS__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "particles.hh"
#include "sphere.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


struct SystemStatistics {

  
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

  static inline Vector centerOfGravity(AggregateParticles &);

  static inline Real numberOfContacts(AggregateParticles &, UInt &, UInt &);
  
  static inline Real coordinationNumber(AggregateParticles &);

  static inline Real coordinationNumber(AggregateParticles &, UInt &, UInt &);
  
  static inline Real averageRadius(AggregateParticles &);

  static inline Real packingDensity(AggregateParticles &, Vector &, Vector &, UInt & );

  static inline std::vector<std::vector<Real>> overlaps(AggregateParticles &);

  static inline Real getKineticEnergy(AggregateParticles &);
  
  static inline Real averageOverlap(AggregateParticles &, UInt &, UInt &);

  static inline Real maximumOverlap(AggregateParticles &, UInt &, UInt &);

};

/* -------------------------------------------------------------------------- */
inline Real SystemStatistics::getKineticEnergy(AggregateParticles & particles) {
  auto size = particles.size();

  Real kin_energy = 0.0;
  for (UInt i = 0; i < size; ++i) {
    auto & mass = particles.getMass(i);
    auto & velo = particles.getVelocity(i);

    auto v_norm = velo.norm();
    kin_energy += 0.5 * mass * v_norm * v_norm;
  }

  return kin_energy;
}

/* -------------------------------------------------------------------------- */

inline Vector SystemStatistics::centerOfGravity(AggregateParticles & particles) {

  auto size = particles.size();

  Vector cg(0., 0., 0.);

  Real total_mass = 0.;
  for (UInt i = 0; i < size; ++i) {
    auto center = particles.getShape(i)->getCenter();
    auto mass   = particles.getMass(i);
    cg += center * mass;
    total_mass += mass;
  }

  return cg/total_mass;
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::averageRadius(AggregateParticles & particles) {

  auto size = particles.size();

  Real avg_radius = 0.0;

  std::vector<Real> averages;

  for (UInt i = 0; i < size; ++i) {

    Real per_average = 0;
    auto & p1 = particles.getShape(i)->getCenter();
    auto & r1 = particles.getShape(i)->getRadius();
    
    for (UInt j = i; j < size; ++j) {
      
      auto & p2 = particles.getShape(j)->getCenter();
      auto & r2 = particles.getShape(j)->getRadius();

      auto distance = (p1 -p2).norm();
      
      auto radius = abs(distance - (r1 + r2));
      per_average += radius;
    }

    avg_radius += per_average  / size;
  }
  
  return avg_radius/size;
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::coordinationNumber(AggregateParticles & particles) {

  Real size = particles.size();

  Real nb_contacts = 0.0;
  for (UInt i = 0; i < size; ++i) {
    for (UInt j = 0; j < size; ++j) {
      if (i == j) 
	continue;

      Real tolerance; 
      bool intersect = false;
      std::vector<bool> intersects;
      particles.getShape(i)->intersect(*particles.getShape(j), tolerance, intersects);

      /*bool intersects = particles.getShape(i)->intersect(*particles.getShape(j));
      if (intersects) {
	nb_contacts++;
      }*/
      if (intersects.size() > 0) {
	intersect = true;
      }
      
      if (intersect) {
	nb_contacts += Real(intersects.size());
      }
    }
  }

  return nb_contacts/size;
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::coordinationNumber(AggregateParticles & particles, UInt & start, UInt & end) {

  Real nb_contacts = SystemStatistics::numberOfContacts(particles, start, end);

  auto nb_particles = end - start + 1;
  return nb_contacts/nb_particles;
}

/* -------------------------------------------------------------------------- */
inline Real SystemStatistics::numberOfContacts(AggregateParticles & particles, UInt & start, UInt & end) {

  Real nb_contacts = 0.0;
  
  for (UInt i = start; i < end; ++i) {
    for (UInt j = start; j < end; ++j) {

      if (i == j) {
 	continue;
      }
      
      Real tolerance; 
      bool intersect = false;
      intersect = particles.getShape(i)->intersect(*particles.getShape(j), tolerance);
     
      if (intersect) {
	nb_contacts++;
	break;
      }
    }
  }

  return nb_contacts;
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::packingDensity(AggregateParticles & particles, Vector & origin, Vector & dimensions, UInt & nb_grid) {

  auto size = particles.size();

  Real x, y, z;
  Real i, j, k;
  Vector grid = dimensions/nb_grid;

  auto get_point = [&](Vector & point, std::tuple<UInt, UInt, UInt> index) {
    std::tie(i, j, k) = index;
    x = i * grid[0] + origin[0] + grid[0]/2.;
    y = j * grid[1] + origin[1] + grid[1]/2.;
    z = k * grid[2] + origin[2] + grid[2]/2.;
    point[0] = x;
    point[1] = y;
    point[2] = z;
  };

  Vector point;
  Real volume = 0;
  bool contains;
  UInt index;
  for (UInt i = 0; i < nb_grid; ++i) {
    for (UInt j = 0; j < nb_grid; ++j) {
      for (UInt k = 0; k < nb_grid; ++k) {

	get_point(point, std::make_tuple(i,j,k));
	for (UInt index = 0; index < size; ++index) {
	  std::tie(contains, index) = particles.getShape(index)->contains(point);
	  if (contains) 
	    volume += grid[0] * grid[1] * grid[2] ;
	}
      }
    }
  }

  Real total_volume = dimensions[0] * dimensions[1] * dimensions[2];
  return volume/total_volume;
}

/* -------------------------------------------------------------------------- */

inline std::vector<std::vector<Real>> SystemStatistics::overlaps(AggregateParticles & particles) {
  /*auto size = particles.size();

  std::vector<std::vector<Real>> data;
  for (UInt i = 0; i < size; ++i) {
    
    auto radius = particles.getShape(i)->getRadius();
    auto center = particles.getShape(i)->getCenter();

    UInt phi = 0;
    Real max_overlap = std::numeric_limits<Real>::min();
    for (UInt j = 0; j < size; ++j) {

      Real temp;
      
      if (i == j) 
	continue;
      bool intersects = particles.getShape(i)->intersect(*particles.getShape(j));
      if (intersects) {
	Real minimum = radius + particles.getShape(j)->getRadius();
	Real distance = (center - particles.getShape(j)->getCenter()).norm();
	temp = minimum - distance;
	phi++;
      }
      
      if (intersects and temp > max_overlap) {
	max_overlap = temp;
      }
    }

    std::vector<Real> vec;
    vec.push_back(radius);
    vec.push_back(max_overlap);
    vec.push_back(phi);

    data.push_back(vec);
  }

  return data;*/
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::averageOverlap(AggregateParticles & particles, UInt & start, UInt & end) {
  
  /*Real overlaps = 0.0;
  Real total_overlaps= 0.0;

  for (UInt i = start; i < end; ++i) {
    
    auto radius = particles.getShape(i)->getRadius();
    auto center = particles.getShape(i)->getCenter();

    for (UInt j = i + 1; j < end; ++j) {
          
      bool intersects = particles.getShape(i)->intersect(*particles.getShape(j));

      if (intersects) {
	Real minimum = radius + particles.getShape(j)->getRadius();
	Real distance = (center - particles.getShape(j)->getCenter()).norm();
	Real temp = minimum - distance;  
      
	overlaps += std::abs(temp);
	total_overlaps++;
      }     
    }
  }

  if (total_overlaps == 0) {
    return 0;
  }
  
  return overlaps/total_overlaps;*/
}


/* -------------------------------------------------------------------------- */

inline Real SystemStatistics::maximumOverlap(AggregateParticles & particles, UInt & start, UInt & end) {
 
  /*std::vector<Real> data;
  for (UInt i = start; i < end; ++i) {
    
    auto radius = particles.getShape(i)->getRadius();
    auto center = particles.getShape(i)->getCenter();

    Real overlaps = 0.0;
    Real total_overlaps= 0.0;
    Real max_overlap = std::numeric_limits<Real>::min();

    for (UInt j = start; j < end; ++j) {
      
      if (i == j) 
	continue;
      
      Real temp;
      bool intersects = particles.getShape(i)->intersect(*particles.getShape(j));

      if (intersects) {
	Real minimum = radius + particles.getShape(j)->getRadius();
	Real distance = (center - particles.getShape(j)->getCenter()).norm();
	temp = minimum - distance;  
      
	overlaps += temp;
	total_overlaps++;
      }
      
      if (intersects and temp > max_overlap) {
	max_overlap = temp;
      }
    }
        
    data.push_back(max_overlap);
  }

  if (data.size() == 0) {
    return 0;
  }
  
  auto max = std::max_element(data.begin(), data.end());
  return *max;*/
}


}


#endif
