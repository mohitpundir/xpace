#ifndef __SYSTEM_EVOLUTION__HH__
#define __SYSTEM_EVOLUTION__HH__

/* -------------------------------------------------------------------------- */
#include "creation_algorithm.hh"
#include "dumper.hh"
#include "evolution_algorithm.hh"
#include "filters.hh"
#include "functional"
#include "particles.hh"
#include "xpace.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class CreationAlgorithm;
class EvolutionAlgorithm;

class SystemEvolution {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  /// Default Constructor
  SystemEvolution() = default;

  /// Default Destructor
  ~SystemEvolution() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  
  /// create the particles
  void create(CreationAlgorithm & );

  /// evolve the particles
  void evolve(EvolutionAlgorithm &);

  /// evolve to a given number of particles
  void evolve(EvolutionAlgorithm &, UInt &, UInt &);

  /// evolve per step
  void evolvePerStep(EvolutionAlgorithm &, UInt &, UInt &);

  /// apply filters
  void apply(std::vector<std::shared_ptr<Filters>> &);

  /// get coordination number
  Real coordinationNumber(UInt &, UInt &);

  /// get maximum overlap
  Real maximumOverlap(UInt &, UInt &);

  /// get maximum overlap
  Real averageOverlap(UInt &, UInt &);

  /// get the number of particles in contacts
  Real numberOfContacts(UInt &, UInt &);

  /// get the packing density
  Real packingDensity(Vector &, Vector &, UInt &);

  /// get the Particle
  AggregateParticles &getParticles() { return this->particles; }

  /// computes the boundary interaction for a given range of particles
  void computeBoundaryInteraction(EvolutionAlgorithm &, UInt &, UInt &);

  /// computes the force interaction for a given range of particles
  void computeForceInteraction(EvolutionAlgorithm &, UInt &, UInt &);

  /* ------------------------------------------------------------------------ */
  /* Inline Methods                                                           */
  /* ------------------------------------------------------------------------ */
public:
  /// get the number of particles
  inline UInt size() { return particles.size(); }

  /// get the center of a given particle
  inline Vector &getCenter(UInt &index) {
    return this->particles.getShape(index)->getCenter();
  }

  /// get the radius of a given particle
  inline Real &getRadius(UInt &index) {
    return this->particles.getShape(index)->getRadius();
  }

  /// get the force of a given particle
  inline Vector &getForce(UInt &index) {
    return this->particles.getForce(index);
  }

  /// get the potential energy of a given particle
  inline Real &getEnergy(UInt &index) {
    return this->particles.getEnergy(index);
  }

  inline void setDumper(std::string &basename, UInt &dump_freq) {
    dumper.setBaseNameToDumper(basename);
    dumper.setDumpFrequency(dump_freq);
  }

  /// dumps all the particles
  inline void dump() { dumper.dumpToTxt(particles); }

  /// dumps particles for a given range
  inline void dump(UInt &start, UInt &end) {
    dumper.dumpToTxt(particles, start, end);
  }

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

protected:
  AggregateParticles particles;

  Dumper dumper;

  
 
};

}

#endif
