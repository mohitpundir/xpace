#include "system_evolution.hh"
#include "system_statistics.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */

void SystemEvolution::create(CreationAlgorithm &creation) {
  creation.create();
  creation.create(particles);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::evolve(EvolutionAlgorithm &evolution) {
  evolution.evolve(particles, dumper);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::evolve(EvolutionAlgorithm &evolution, UInt &start,
                             UInt &end) {
  evolution.evolve(particles, dumper, start, end);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::evolvePerStep(EvolutionAlgorithm &evolution, UInt &start,
                                    UInt &end) {
  evolution.evolvePerStep(particles, start, end);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::computeBoundaryInteraction(EvolutionAlgorithm &evolution,
                                                 UInt &start, UInt &end) {
  evolution.computeBoundaryInteraction(particles, start, end);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::computeForceInteraction(EvolutionAlgorithm &evolution,
                                              UInt &start, UInt &end) {
  evolution.computeForceInteraction(particles, start, end);
}

/* -------------------------------------------------------------------------- */
void SystemEvolution::apply(std::vector<std::shared_ptr<Filters>> &filters) {
  for (auto &filter : filters) {
    filter->apply(particles);
  }
}

/* -------------------------------------------------------------------------- */
Real SystemEvolution::coordinationNumber(UInt &start, UInt &end) {
  return SystemStatistics::coordinationNumber(particles, start, end);
}

/* -------------------------------------------------------------------------- */
Real SystemEvolution::maximumOverlap(UInt &start, UInt &end) {
  return SystemStatistics::maximumOverlap(particles, start, end);
}

/* -------------------------------------------------------------------------- */
Real SystemEvolution::numberOfContacts(UInt &start, UInt &end) {
  return SystemStatistics::numberOfContacts(particles, start, end);
}

/* -------------------------------------------------------------------------- */
Real SystemEvolution::averageOverlap(UInt &start, UInt &end) {
  return SystemStatistics::averageOverlap(particles, start, end);
}

/* -------------------------------------------------------------------------- */
Real SystemEvolution::packingDensity(Vector &origin, Vector &dimensions,
                                     UInt &nb_grid) {
  return SystemStatistics::packingDensity(particles, origin, dimensions,
                                          nb_grid);
}

/* -------------------------------------------------------------------------- */

}
