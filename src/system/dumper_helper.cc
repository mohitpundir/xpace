#include "dumper_helper.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {


/* -------------------------------------------------------------------------- */
DumperHelper::DumperHelper() {

  this->step = 0;
}

/* -------------------------------------------------------------------------- */
DumperHelper::~DumperHelper() {

}

/* -------------------------------------------------------------------------- */
vtkSphereSourceP DumperHelper::makeSphere(Vector & c, Real & radius) {

  UInt size = c.size();
  double center[size];
  for (UInt i = 0; i < size; i++) {
    center[i] = c[i];
  }
  
  auto sphereSource = vtkSphereSourceP::New();
  sphereSource->SetCenter(center);
  sphereSource->SetRadius(radius);

  sphereSource->SetPhiResolution(15);
  sphereSource->SetThetaResolution(15);
  sphereSource->Update();
  
  return sphereSource;
}

/* -------------------------------------------------------------------------- */
void DumperHelper::write(Particles<Shape *, Vector, Vector, Real, Real> & particles) {
  std::stringstream filename;
  filename << dumper_name << "-00" << step << ".txt";

  std::ofstream os(filename.str());

  os << "#x y z radius" << std::endl;

  auto size = particles.size();
  
  for (UInt index = 0; index < size; ++index) {
    auto & center = particles.get<_shape>(index)->getCenter();
    auto & radius = particles.get<_shape>(index)->getRadius();
    os << center << " " << radius << std::endl;
  }

  os.close();

  this->step++;
}


/* -------------------------------------------------------------------------- */
vtkUnstructuredGridP DumperHelper::makePolyhedron(std::vector<Real> & vertices, std::vector<UInt> & connectivity, Real & radius) {

  auto nb_vertices = vertices.size() / 3;
  auto nb_faces    = connectivity.size() / 3;

  UInt strides = 3;

  vtkIdType pointIds[nb_vertices];
  auto pointSource = vtkPointsP::New();

  for (UInt i = 0; i < nb_vertices; ++i) {
    pointSource->InsertNextPoint(vertices[i*strides + 0], vertices[i*strides + 1],
				 vertices[i*strides + 2]);
    pointIds[i] = i;
  }

  auto cellSource = vtkCellArraySourceP::New();
  for (UInt i = 0; i < nb_faces; ++i) {
    vtkIdType face[3] = {connectivity[i * strides + 0], connectivity[i * strides + 1],
			 connectivity[i * strides + 2]};
    cellSource->InsertNextCell(strides, face);
  }

  auto uGridSource = vtkUnstructuredGridP::New();
  uGridSource->SetPoints(pointSource);
  uGridSource->InsertNextCell(VTK_POLYHEDRON, nb_vertices, pointIds, nb_faces, cellSource->GetPointer());

  return uGridSource;
}



}
/* -------------------------------------------------------------------------- */
