#ifndef __DUMPER_HELPER_TMPL__HH__
#define __DUMPER_HELPER_TMPL__HH__
/* -------------------------------------------------------------------------- */
#include "dumper_helper.hh"
#include <fstream>
/* -------------------------------------------------------------------------- */
namespace xpace {

/* -------------------------------------------------------------------------- */
template<typename T>
void DumperHelper::dump(std::vector<T*> & particles) {

  std::stringstream filename;
  
  filename << dumper_name << "-00" << step << ".vtu";
  auto writer       = vtkXMLUnstructuredGridWriterP::New();
  auto appendFilter = vtkAppendFilterP::New();

  auto it  = particles.begin();
  auto end = particles.end();

  for (; it != end; ++it) {
    auto particle = *it;

    switch (particle->getType()) {
    case _sphere: {
      auto sphere = makeSphere(particle->getCenter(),
			       particle->getRadius());
    
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(sphere->GetOutput());
#else
      appendFilter->AddInputData(sphere->GetOutput());
#endif
      break;
    }

    case _polyhedron: {
      auto polyhedron = makePolyhedron(particle->getPoints(),
				       particle->getConnectivity(),
				       particle->getRadius());

#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(polyhedron);
#else
      appendFilter->AddInputData(polyhedron);
#endif
            
      break;
    }
   
    }
  }
  appendFilter->Update();

  const std::string & tmp = filename.str();
  const char* cstr = tmp.c_str();

  writer->SetFileName(cstr);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(appendFilter->GetOutput());
#else
  writer->SetInputData(appendFilter->GetOutput());
#endif
  writer->Write();

  this->step++;
}



/* -------------------------------------------------------------------------- */
template<typename T>
void DumperHelper::dump(std::vector<T*> & particles, UInt level) {

  std::stringstream filename;
  
  filename << dumper_name << "-00" << step << ".vtu";
  auto writer       = vtkXMLUnstructuredGridWriterP::New();
  auto appendFilter = vtkAppendFilterP::New();

  auto it  = particles.begin();
  auto end = particles.end();
#pragma omp parallel shared(appendFilter)
  {
#pragma omp for
  for (; it != end; ++it) {
    auto particle = *it;
    if (particle->getLevel() > level) {
      continue;
    }

    switch (particle->getType()) {
    case _sphere: {
      auto sphere = makeSphere(particle->getCenter(), particle->getRadius());
    
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(sphere->GetOutput());
#else
      appendFilter->AddInputData(sphere->GetOutput());
#endif
      break;
    }

    case _polyhedron: {
      auto polyhedron = makePolyhedron(particle->getPoints(), particle->getConnectivity(), particle->getRadius());
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(polyhedron);
#else
      appendFilter->AddInputData(polyhedron);
#endif
      
      break;
    }
   
    }
  }
  }
  appendFilter->Update();

  const std::string & tmp = filename.str();
  const char* cstr = tmp.c_str();

  writer->SetFileName(cstr);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(appendFilter->GetOutput());
#else
  writer->SetInputData(appendFilter->GetOutput());
#endif
  writer->Write();

  this->step++;
}


/* -------------------------------------------------------------------------- */
template<typename T>
void DumperHelper::dumpToIndex(std::vector<T*> & particles, UInt index) {

  std::stringstream filename;
  
  filename << dumper_name << "-00" << step << ".vtu";
  auto writer       = vtkXMLUnstructuredGridWriterP::New();
  auto appendFilter = vtkAppendFilterP::New();

  auto it  = particles.begin();
  auto end = particles.end();
#pragma omp parallel shared(appendFilter)
  {
#pragma omp for
    for (UInt i = 0; i <= index; ++it, ++i) {
    auto particle = *it;
    
    switch (particle->getType()) {
    case _sphere: {
      auto sphere = makeSphere(particle->getCenter(), particle->getRadius());
    
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(sphere->GetOutput());
#else
      appendFilter->AddInputData(sphere->GetOutput());
#endif
      break;
    }

    case _polyhedron: {
      auto polyhedron = makePolyhedron(particle->getPoints(), particle->getConnectivity(), particle->getRadius());
#if VTK_MAJOR_VERSION <= 5
      appendFilter->AddInput(polyhedron);
#else
      appendFilter->AddInputData(polyhedron);
#endif
      
      break;
    }
   
    }
  }
  }
  appendFilter->Update();

  const std::string & tmp = filename.str();
  const char* cstr = tmp.c_str();

  writer->SetFileName(cstr);
#if VTK_MAJOR_VERSION <= 5
  writer->SetInput(appendFilter->GetOutput());
#else
  writer->SetInputData(appendFilter->GetOutput());
#endif
  writer->Write();

  this->step++;
}




/* -------------------------------------------------------------------------- */
template<typename T>
void DumperHelper::write(std::vector<T*> & particles) {

  std::stringstream filename;
  filename << dumper_name << "-00" << step << ".txt";

  std::ofstream os(filename.str());

  os << "#x y z radius" << std::endl;
  
  for (auto p : particles) {
    auto & center = p->getCenter();
    auto & radius = p->getRadius();
    os << center << " " << radius << std::endl;
  }

  os.close();
  
  this->step++;
  
}


/* -------------------------------------------------------------------------- */
template<typename T>
void DumperHelper::writeToIndex(std::vector<T*> & particles, UInt index) {

  std::stringstream filename;
  filename << dumper_name << "-00" << step << ".txt";

  std::ofstream os(filename.str());

  os << "#x y z radius" << std::endl;

  auto it  = particles.begin();
  auto end = particles.end();

  for (UInt i = 0; i <= index; ++it, ++i) {
    
    auto & center = (*it)->getCenter();
    auto & radius = (*it)->getRadius();
    os << center << " " << radius << std::endl;
  }

  os.close();
  
  this->step++;
  
}



}
/* -------------------------------------------------------------------------- */

#endif
