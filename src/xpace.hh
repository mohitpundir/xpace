#ifndef __xPACE__HH__
#define __xPACE__HH__

#include "xpace_config.hh"

/* -------------------------------------------------------------------------- */
#include <math.h>
#include <string>
#include <stdlib.h>
#include <vector>
#include <string>
#include <typeinfo>
#include <iostream>
#include <stdlib.h>
#include <cmath>
#include <random>
#include <fstream>
#include <sstream>
#include <array>
#include <exception>
#include <limits>
#include <memory>
#include <utility>
#include <omp.h>
#include <complex>
#include <map>
#include <any>
/* -------------------------------------------------------------------------- */
#ifdef XPACE_USE_MPI
#include <mpi.h>
#endif // XPACE_USE_MPI

/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
using UInt = unsigned int;  ///< default unsigned integer integer type
using Int  = int;
using Real = double;        ///< default floating point type
using complex = std::complex<Real>;
static constexpr Real zero_threshold = 1e-14;

/* -------------------------------------------------------------------------- */
using ArgMap = std::map<std::string, std::any>;

/* -------------------------------------------------------------------------- */
enum SpatialDirection {
  _x = 0,
  _y = 1,
  _z = 2,
  _all = 4
};

enum class ConvergenceCriteria {
  contact,
  packing_density
};

enum Integration {
  _position,
  _velocity
};

enum Boundary {
  _periodic,
  _soft_aperiodic,
  _hard_aperiodic
};

/* -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- */
template < class T >
std::ostream& operator << (std::ostream& os, const std::vector<T>& v) 
{
    os << "[";
    for (typename std::vector<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii)
    {
        os << " " << *ii;
    }
    os << "]";
    return os;
}
		 
/* -------------------------------------------------------------------------- */
class Exception : public std::exception {
public:
  /// Constructor
  Exception(const std::string & mesg) : msg(mesg) {}

  virtual const char* what() const noexcept { return msg.c_str(); }

  virtual ~Exception() = default;

private:
  std::string msg;   ///< message of exception
};


}

/* -------------------------------------------------------------------------- */

#define EIGEN_MATRIX_PLUGIN "matrix_eigen_addons.hh"

#define XPACE_EXCEPTION(mesg)						    \
  {									    \
    std::stringstream sstr;						    \
    sstr << __FILE__ << ":" << __LINE__ << ":FATAL: " << mesg << std::endl; \
    std::cerr.flush();							    \
    throw ::xpace::Exception(sstr.str());				    \
  }

#define XPACE_FATAL(mesg) XPACE_EXCEPTION(mesg)

#define XPACE_ASSERT(cond, reason)				\
  do {								\
    if (!(cond)) {						\
      XPACE_EXCEPTION(#cond "asser failed: " <<  reason);	\
    }								\
} while (0);


#define XPACE_ACCESSOR(var, type, name)                         \
  type& get##name() { return var; }                             \
  void set##name( const type& new_var) {var = new_var; }	
/* -------------------------------------------------------------------------- */


#endif // XPACE_HH
