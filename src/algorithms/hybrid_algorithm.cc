#include "hybrid_algorithm.hh"
#include "compute_boundary.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
void HybridAlgorithm::evolve(AggregateParticles &particles, Dumper &dumper) {

  auto size = particles.size();

  auto begin = 0;
  for (UInt index = 1; index < size; ++index) {

    std::cerr << "Placing particle : " << index << std::endl;

    //this->dt = 1.0/particles.getShape(index)->getRadius();

    auto shape      = particles.getShape(index);
    auto & velocity = particles.getVelocity(index);
    auto & mass     = particles.getMass(index);
    auto & force    = particles.getForce(index); 
    
    UInt iiter = 0;
    Vector increment;

    while (iiter < this->nsteps) {

      velocity += (force / mass) * (0.5 *this->dt);

      increment = velocity * this->dt;
      if (increment.norm() > shape->getRadius()) {
        Vector direction =
	  velocity / velocity.norm();
        increment = direction * shape->getRadius();
      }
      
      shape->translate(increment);
       
      for (UInt i = 0; i < size; ++i)
        particles.getForce(i) *= 0.0;

      for (UInt j = 0; j < index; ++j) {
        for (auto &interaction : this->computes) {
          interaction->computeInteraction(particles, index, j);
        }
      }

      velocity += (force / mass) * (0.5 * this->dt); 
      
      this->boundary->computeInteraction(particles, index, index);

      increment = velocity * this->dt;
      if (increment.norm() > shape->getRadius()) {
        Vector direction =
	  velocity / velocity.norm();
        increment = direction * shape->getRadius();
      }

      std::cout << increment << std::endl;
      
      if (iiter % damping_freq == 0) {
	velocity *= damping_ratio;
      }

      shape->translate(increment);
      bool converged = this->testConvergence(particles, index);

      if (converged) 
        break;

      iiter++;
    }

    if (iiter >= this->nsteps) {
      std::cerr << "Did not converge : " << iiter << std::endl;
    }
  }

  //dumper.dumpToTxt(particles, this->dimensions);
}

/* -------------------------------------------------------------------------- */
bool HybridAlgorithm::evolve(AggregateParticles &particles, Dumper &dumper,
                             UInt &start, UInt &end) {}

/* -------------------------------------------------------------------------- */
void HybridAlgorithm::evolvePerStep(AggregateParticles &particles, UInt &start,
                                    UInt &end) {}

/* -------------------------------------------------------------------------- */
template <typename T>
bool HybridAlgorithm::testConvergence(T &particles, UInt &index) {

  UInt dim = 3;
  bool converged = false;
  Real nb_contacts = 0;

  Real tolerance;
  for (UInt j = 0; j < index; ++j) {
    if (j == index)
      continue;

    bool intersect = false;
    intersect = particles.getShape(index)->intersect(*particles.getShape(j), this->dimensions);

    if (intersect) {
      nb_contacts++;
    }
  }

  std::cout << "nb contacts = " << nb_contacts << std::endl;
  auto nb_particles = index;
  Real cn = nb_contacts/nb_particles;
  if (cn <= this->coordination) {
    converged = true;
  }
  
  return converged;
}

}
