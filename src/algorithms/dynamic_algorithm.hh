#ifndef __DYNAMIC_ALGORITHM__HH__
#define __DYNAMIC_ALGORITHM__HH__
/* -------------------------------------------------------------------------- */
#include "evolution_algorithm.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


class DynamicAlgorithm
  : public EvolutionAlgorithm {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ///
  DynamicAlgorithm() = default;
  ///
  ~DynamicAlgorithm() = default;
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  void evolve(AggregateParticles &, Dumper & ) override;
  ///
  bool evolve(AggregateParticles &, Dumper &, UInt &, UInt &) override;
  /// 
  void evolvePerStep(AggregateParticles &, UInt &, UInt &) override;

public:

  XPACE_ACCESSOR(kill_velocity, bool, KillVelocity);
  
protected:
  /// check for the convergenceCriteria such as intersection and
  /// coordination number
  template<typename T>
  bool testConvergence(T &, UInt &);

  /// udpates the velocity to 0 if a particle is not intersecting with
  /// any other particle
  void updateVelocityToZero(AggregateParticles &, UInt &);

private:
  /// boolean to whether kill the velocity or not
  bool kill_velocity;
  
};


}



#endif
