#include "creation_fuller_algorithm.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {

/* ---------------------------------------------------------------------------*/
CreationFullerAlgorithm::CreationFullerAlgorithm()
  : CreationAlgorithm() {

}

/* -------------------------------------------------------------------------- */
CreationFullerAlgorithm::CreationFullerAlgorithm(ArgMap attributes)
  : CreationAlgorithm(attributes) {
  
  this->packing      = std::any_cast<Real>(attributes["packing"]);
  this->diameters    = std::any_cast<std::vector<Real>>(attributes["attributes"]);
}

  

/* -------------------------------------------------------------------------- */
void CreationFullerAlgorithm::create() {

  this->global_shapes.clear();
  this->global_radii.clear();
  this->radii_levels.clear();
  
  this->volume = 1.0;
  for (UInt i = 0; i < this->spatial_dimension; i++) {
    this->volume *= this->dimensions[i];
  }
    
  std::random_device random_seed;
  std::mt19937 gen(random_seed());
  
  auto it  = diameters.begin();
  auto end = diameters.end();

  Real prev_dia, next_dia;
  Real seg_vol, rem_vol;

  seg_vol = 0.0;
  rem_vol = 0.0;

  UInt nb_particles = 0;

  Vector center(0., 0., 0.);

  Real agg_vol = 0.0;

  for (; it != end - 1; ++it) {
    prev_dia = *it;
    next_dia = *(it+1);

    seg_vol = this->getSegmentVolume(prev_dia, next_dia);
    rem_vol += seg_vol;

    std::uniform_real_distribution<> distribution(next_dia/2., prev_dia/2.);
  
    while (rem_vol >= this->getParticleVolume(next_dia)) {

      Real radius = distribution(gen);
      
      auto shape = ShapeFactoryInterface::getInstance().createShape();
      shape->create(radius, center);
      rem_vol -= shape->getVolume();
      agg_vol += shape->getVolume();
      global_shapes.push_back(std::move(shape));
      global_radii.push_back(radius);
     
      nb_particles++;
    }
   radii_levels.push_back(nb_particles);
  }

  auto in_descending = [](const std::unique_ptr<Shape> & s1, const std::unique_ptr<Shape> & s2) {
    return s1->getRadius() > s2->getRadius(); 
  };
  
  std::sort(global_shapes.begin(), global_shapes.end(), in_descending);
  std::cout << "Packing density : " << agg_vol/volume <<std::endl;

}

/* -------------------------------------------------------------------------- */
void CreationFullerAlgorithm::create(AggregateParticles & particles) {
  
  for (UInt i = 0; i < global_shapes.size(); ++i) {
    
    Real   mass;
    Real   energy = 0.0;
    Vector force(    0, 0, 0);
    Vector center(   0, 0, 0);
    Vector velocity( 0, 0, 0);

    auto shape = std::move(global_shapes[i]);
    auto radius = shape->getRadius();
    this->computeCenter(center, radius, origin, dimensions);
    shape->translate(center);
    mass = shape->getVolume();

    particles.push(std::move(shape), std::move(force), std::move(velocity),
		   std::move(mass), std::move(energy));
  }
}

/* ---------------------------------------------------------------------------*/
Real CreationFullerAlgorithm::getSegmentVolume(Real & prev_dia, Real & next_dia) {

  Real seg_vol = 0.0;
  Real max_dia = *std::max_element(diameters.begin(), diameters.end());
  Real min_dia = *std::min_element(diameters.begin(), diameters.end());
    
  Real fuller_prev = this->fuller(prev_dia, max_dia);
  Real fuller_next = this->fuller(next_dia, max_dia);
  Real fuller_max  = this->fuller(max_dia , max_dia);
  Real fuller_min  = this->fuller(min_dia , max_dia);

  seg_vol = ((fuller_prev - fuller_next) / (fuller_max - fuller_min))*packing*volume;

  return seg_vol;
}

/* -------------------------------------------------------------------------- */
void CreationFullerAlgorithm::computeCenter(Vector & center, Real & radius,
					    Vector & origin, Vector & dimensions) {
    
  Real x_min = origin[0] + radius;
  Real x_max = origin[0] + dimensions[0] - radius;
  
  Real y_min = origin[1] + radius;
  Real y_max = origin[1] + dimensions[1] - radius;
      
  Real z_min = origin[2] + radius;
  Real z_max = origin[2] + dimensions[2] - radius;

  std::uniform_real_distribution<> distribution_x(x_min, x_max);
  std::uniform_real_distribution<> distribution_y(y_min, y_max);
  std::uniform_real_distribution<> distribution_z(z_min, z_max);
  
  std::random_device random_seed;
  std::mt19937 gen(random_seed());
  
  center[0] = distribution_x(gen);
  center[1] = distribution_y(gen);

  switch (spatial_dimension) {
  case 2: {
     center[2] = origin[2];
    break;
  }
  case 3: {
    center[2] = distribution_z(gen);
    break;
  }
  default:
    break;
  }
}

/* ---------------------------------------------------------------------------*/
Real CreationFullerAlgorithm::getParticleVolume(Real & diameter) {

  Real volume = 0;
  switch (spatial_dimension) {
  case 2: {
    volume = M_PI*pow(diameter/2., 2);
    break;
  }
  case 3: {
    volume = (4./3.)*M_PI*pow(diameter/2., 3);
    break;
  }
  default:
    break;
  }
  return volume;
}

/* ---------------------------------------------------------------------------*/
Real CreationFullerAlgorithm::fuller(Real & diameter, Real & max_diameter) {
  return 100.*pow(diameter/max_diameter, 0.5);
}

}
/* -------------------------------------------------------------------------- */
