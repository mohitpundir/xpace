#include "creation_from_generator.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
CreationFromGenerator::CreationFromGenerator() 
  : CreationAlgorithm() {
  createShapes = [&]() {
    this->create();
  };
}

/* -------------------------------------------------------------------------- */
void CreationFromGenerator::create() {
}


/* -------------------------------------------------------------------------- */
void CreationFromGenerator::addShape(Real & radius, Vector & center, Vector & force, Vector & velocity) {

  auto shape = ShapeFactoryInterface::getInstance().createShape();
  shape->create(radius, center);

  global_shapes.push_back(std::move(shape));

  global_forces.push_back(force);
  global_velocity.push_back(velocity);
}

/* -------------------------------------------------------------------------- */
void CreationFromGenerator::create(AggregateParticles & particles) {

  for (UInt i = 0; i < global_shapes.size(); ++i) {
    
    Real   mass;
    Real   energy = 0.0;
    Vector force = global_forces[i];
    Vector velocity = global_velocity[i];

    auto shape = std::move(global_shapes[i]);
    auto radius = shape->getRadius();
    mass = shape->getVolume();

    particles.push(std::move(shape), std::move(force), std::move(velocity), std::move(mass), std::move(energy));
  }
}

/* -------------------------------------------------------------------------- */
void CreationFromGenerator::distribute() {

}


}
