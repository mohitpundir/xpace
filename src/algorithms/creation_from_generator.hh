#ifndef __CREATION_FROM_GENERATOR__HH__
#define __CREATION_FROM_GENERATOR__HH__

/* -------------------------------------------------------------------------- */
#include "creation_algorithm.hh"
#include "functional"
/* -------------------------------------------------------------------------- */

namespace xpace {

class CreationFromGenerator : public CreationAlgorithm {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  CreationFromGenerator();

  ~CreationFromGenerator() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  template <typename Func> void setShapeFunctor(Func func) {
    createShapes = [&]() { func(*this); };
  }
  ///
  void create() override;
  ///
  void create(AggregateParticles &) override;
  ///
  void addShape(Real &, Vector &, Vector &, Vector &);
  ///
  void distribute() override;

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  std::function<void()> createShapes;

private:
  std::vector<Vector> global_forces;

  std::vector<Vector> global_velocity;
};

}

#endif
