#ifndef __TAKE_PLACE_ALGORITHM__HH__
#define __TAKE_PLACE_ALGORITHM__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "evolution_algorithm.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */

class TakePlaceAlgorithm
  : public EvolutionAlgorithm {

public:
  ///
  TakePlaceAlgorithm() = default;
  ///
  ~TakePlaceAlgorithm() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods: Private                                                         */
  /* ------------------------------------------------------------------------ */

private:
  
  /* ------------------------------------------------------------------------ */
  /* Methods: Public                                                          */
  /* ------------------------------------------------------------------------ */

public:
  ///
  void evolve(AggregateParticles &, Dumper &) override;
  ///
  bool evolve(AggregateParticles &, Dumper &, UInt &, UInt &) override;
  ///
  void evolvePerStep(AggregateParticles &, UInt &, UInt &) override;

  
public:
  XPACE_ACCESSOR(gamma   , Real, Gamma);
  
private:
  /// rotates particle around anotehr aprticle to find a new location
  template<typename T>
  void rotate(T &, UInt &, UInt &);

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

private:
  /// 
  Real gamma;
  ///
  Real epsilon = 0.;

};

/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */

#endif
