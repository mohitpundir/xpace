/**
 *  @file
 *  @section LICENSE
 *
 *  Copyright (©) 2016-19 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef __EVOLUTION_ALGORITHM__HH__
#define __EVOLUTION_ALGORITHM__HH__
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "particles.hh"
#include "compute.hh"
#include "dumper.hh"
#include "system_statistics.hh"
#include "compute_time_integration.hh"
/* -------------------------------------------------------------------------- */


namespace xpace {


class EvolutionAlgorithm{

public:
  /// Constructor
  EvolutionAlgorithm() = default;
  ///
  ~EvolutionAlgorithm()= default;

public:

  /// evolve the entire system
  virtual void evolve(AggregateParticles &, Dumper &) = 0;

  /// evolve the system for a given number of particles
  virtual bool evolve(AggregateParticles &, Dumper &, UInt &, UInt &) = 0;
  
  /// evolves per step
  virtual void evolvePerStep(AggregateParticles &, UInt &, UInt &) = 0;
  
  /// particles loop on user-defiend functor
  template <typename Functor>
  static void particleLoop(AggregateParticles & particles, Functor func) {
    UInt size = particles.size();
    for (UInt p1 = 0; p1 < size; ++p1) {
      func(p1);
    }
  }

  /// particles loop on user-defiend functor
  template <typename Functor>
  static void particleLoop(AggregateParticles & particles, UInt & start, UInt & end, Functor func) {
    for (UInt p1 = start; p1 < end; ++p1) {
      func(p1);
    }
  }
  
  /// Add compute to the list of computes
  inline void addCompute(std::shared_ptr<Compute> compute) {
    computes.push_back(std::move(compute));
  }

  /// Add boundary to the algorithm
  inline void addBoundary(std::shared_ptr<Compute> boundary) {
    this->boundary = std::move(boundary);
  }

  /// Add time integration scheme to the algorithm
  inline void addTimeIntegration(std::shared_ptr<ComputeTimeIntegration> integration) {
    this->integration = std::move(integration);
  }


  /// compute force interaction from each force field
  inline Real computeForceInteraction(AggregateParticles & particles, UInt & start, UInt & end) {

    // first of all reset forces and energy to zero
    for (UInt i = 0; i < end; ++i) {
      particles.getForce(i) *= 0;
      particles.getEnergy(i) *= 0;
    }

    for (auto & interaction : this->computes) {
      interaction->compute(particles, start, end);
    }
  }

  /// compute boundary interaction
  inline Real computeBoundaryInteraction(AggregateParticles & particles, UInt & start, UInt & end) {
    this->boundary->compute(particles, start, end);
  }
  
public:
  XPACE_ACCESSOR(spatial_dimension, UInt,                SpatialDimension);
  XPACE_ACCESSOR(dimensions,        Vector,              Dimensions);
  XPACE_ACCESSOR(nsteps,            UInt,                Nsteps);
  XPACE_ACCESSOR(dt,                Real,                TimeStep);
  XPACE_ACCESSOR(coordination,      Real,                Coordination);
  XPACE_ACCESSOR(contact_tol,       Real,                Tolerance);
  XPACE_ACCESSOR(damping_ratio,     Real,                DampingRatio);
  XPACE_ACCESSOR(damping_freq,      UInt,                DampingFreq);
  XPACE_ACCESSOR(criteria,          ConvergenceCriteria, ConvergenceCriteria);  
  XPACE_ACCESSOR(layers,            std::vector<UInt>,   Layers);

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

protected:
  /// spatial dimension
  UInt spatial_dimension;
  
  /// list of all force computations
  std::vector<std::shared_ptr<Compute>> computes;

  /// boundary computation
  std::shared_ptr<Compute> boundary;

  /// time integration scheme
  std::shared_ptr<ComputeTimeIntegration> integration;

  /// maximum number of steps
  UInt nsteps;

  /// timestep for time integration
  Real dt;

  /// minimum coordination number to reach convergence
  Real coordination;

  /// damping ratio for velocity
  Real damping_ratio{1.0};

  /// damping frequency fot dampening velocity
  UInt damping_freq{1};

  /// 
  Real contact_tol;

  /// 
  ConvergenceCriteria criteria;

  /// dimenions of the system or box
  Vector dimensions;

  ///  
  std::vector<UInt> layers;
};


}


#endif
