#include "dynamic_algorithm.hh"
#include "compute_boundary.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
void DynamicAlgorithm::evolve(AggregateParticles &particles, Dumper &dumper) {

  UInt start = 0;
  UInt end = particles.size();
  
  std::cerr << " ----- Placing particles from : " << start << std::endl;
  std::cerr << " ----- Placing particles to   : " << end << std::endl;

  bool converged = this->evolve(particles, dumper, start, end);
}

/* -------------------------------------------------------------------------- */
bool DynamicAlgorithm::evolve(AggregateParticles &particles, Dumper &dumper,
                              UInt &start, UInt &end) {

  bool converged = false;

  UInt iiter = 0;
  UInt begin = 0;

  while (iiter < this->nsteps and !converged) {

    this->evolvePerStep(particles, start, end);

    dumper.dumpToTxt(particles, dimensions);

    if (iiter % damping_freq == 0) {
      for (UInt index = start; index < end; ++index) {
	
	//particles.getVelocity(index) *= damping_ratio;
      }
    }

    iiter++;
  }

  return converged;
}

/* -------------------------------------------------------------------------- */
void DynamicAlgorithm::evolvePerStep(AggregateParticles &particles, UInt &start,
                                     UInt &end) {

  Vector increment(0., 0., 0.);
  for (UInt index = start; index < end; ++index) {

    particles.getVelocity(index) +=
        (particles.getForce(index) / particles.getMass(index)) * (0.5 * this->dt);

    increment = particles.getVelocity(index) * this->dt;

    particles.getShape(index)->translate(increment);
  }

  this->computeBoundaryInteraction(particles, start, end);
  this->computeForceInteraction(particles, start, end);

  for (UInt index = start; index < end; ++index) {
    particles.getVelocity(index) +=
        (particles.getForce(index) / particles.getMass(index)) *
        (0.5 * this->dt);
  }

  /*if (kill_velocity) {
    this->updateVelocityToZero(particles, end);
    }*/
}

/* -------------------------------------------------------------------------- */
void DynamicAlgorithm::updateVelocityToZero(AggregateParticles &particles,
                                            UInt &end) {

  for (UInt i = 0; i < end; ++i) {

    bool intersect = false;
    for (UInt j = 0; j < end; ++j) {

      if (i == j)
        continue;

      Real tolerance;
      intersect =
	particles.getShape(i)->intersect(*particles.getShape(j), this->dimensions);

      if (intersect)
        break;
    }

    if (!intersect)
      particles.getVelocity(i) *= 0.0;
  }
}

/* -------------------------------------------------------------------------- */
template <typename T>
bool DynamicAlgorithm::testConvergence(T &particles, UInt &end) {

  UInt start = 0;
  bool converged = false;

  switch (this->criteria) {
  case ConvergenceCriteria::contact: {
    Real cn = SystemStatistics::coordinationNumber(particles, start, end);
    if (cn <= this->coordination)
      converged = true;

    break;
  }
  default:
    converged = false;
    break;
  }

  return converged;
}

/* -------------------------------------------------------------------------- */

}
