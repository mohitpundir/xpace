#ifndef __HYBRID_ALGORITHM_HH__
#define __HYBRID_ALGORITHM_HH__
/* -------------------------------------------------------------------------- */
#include "evolution_algorithm.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class HybridAlgorithm
  : public EvolutionAlgorithm {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  HybridAlgorithm() = default;

  ~HybridAlgorithm() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  void evolve(AggregateParticles &, Dumper & ) override;
  ///
  bool evolve(AggregateParticles &, Dumper &, UInt &, UInt &) override;
  ///
  void evolvePerStep(AggregateParticles &, UInt &, UInt &) override;

private:
  /// check whether convergence is reached
  template<typename T>
  bool testConvergence(T &, UInt &);
};

}

#endif
