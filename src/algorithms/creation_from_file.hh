#ifndef __CREATION_FROM_FILE_HH__
#define __CREATION_FROM_FILE_HH__

/* -------------------------------------------------------------------------- */
#include <fstream>
#include <sstream>
#include <string>
/* -------------------------------------------------------------------------- */
#include "creation_algorithm.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


class CreationFromFile
  : public CreationAlgorithm {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */

public:
  CreationFromFile();

  ~CreationFromFile() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  void create() override;
  ///
  void create(AggregateParticles &) override;
  ///
  //void distribute() override;

public:

  XPACE_ACCESSOR(filename , std::string, Filename);

  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

private:

  std::string filename;

  std::vector<Vector> global_forces;

  std::vector<Vector> global_velocity;
  
};


/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */

#endif
