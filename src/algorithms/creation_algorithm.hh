#ifndef __CREATION_ALGORITHM__HH__
#define __CREATION_ALGORITHM__HH__

/* -------------------------------------------------------------------------- */
#include "particles.hh"
#include "shape.hh"
#include "shape_factory_interface.hh"
#include "vector.hh"
#include "xpace.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class CreationAlgorithm {

public:
  CreationAlgorithm() = default;

  CreationAlgorithm(ArgMap attributes) {
    this->spatial_dimension = std::any_cast<UInt>(attributes["spatial_dimension"]);
  }

  ~CreationAlgorithm() = default;

public:
  XPACE_ACCESSOR(spatial_dimension, UInt, SpatialDimension);
  XPACE_ACCESSOR(dimensions, Vector, Dimensions);
  XPACE_ACCESSOR(origin, Vector, Origin);
  XPACE_ACCESSOR(global_radii, std::vector<Real>, ParticleRadii);
  XPACE_ACCESSOR(radii_levels, std::vector<UInt>, RadiiLevels);

  
public:
  inline UInt size() { return global_shapes.size(); }

  /* --------------------------------------------------------------------------
   */
  inline void addShape(Real &radius, Vector &center, Vector &force,
                       Vector &velocity) {

    auto shape = ShapeFactoryInterface::getInstance().createShape();
    shape->create(radius, center);

    global_shapes.push_back(std::move(shape));

    global_forces.push_back(force);
    global_velocity.push_back(velocity);
  }

  
  /* -------------------------------------------------------------------------- */
  virtual void create() {

  }
  
  /* --------------------------------------------------------------------------
   */
  virtual void create(AggregateParticles &particles) {
    for (UInt i = 0; i < global_shapes.size(); ++i) {

      Real mass;
      Real energy = 0.0;
      Vector force = global_forces[i];
      Vector velocity = global_velocity[i];

      auto shape = std::move(global_shapes[i]);
      auto radius = shape->getRadius();
      mass = shape->getVolume();
      particles.push(std::move(shape), std::move(force), std::move(velocity),
                     std::move(mass), std::move(energy));
    }
  }

public:
  /// spatial dimension
  UInt spatial_dimension;
  
  /// radii of each particle
  std::vector<Real> global_radii;
  /// local radii of each particle
  std::vector<Real> local_radii;
  ///
  std::vector<std::unique_ptr<Shape>> global_shapes;
  ///
  std::vector<std::unique_ptr<Shape>> local_shapes;
  ///
  std::vector<Vector> global_forces;
  ///
  std::vector<Vector> global_velocity;
  /// radii levels
  std::vector<UInt> radii_levels;
  /// dimension of teh sample
  Vector dimensions;
  /// origin of the global system
  Vector origin;
};

}

#endif
