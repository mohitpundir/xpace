#include "creation_from_file.hh"
/* -------------------------------------------------------------------------- */
namespace xpace {


/* -------------------------------------------------------------------------- */
CreationFromFile::CreationFromFile()
  : CreationAlgorithm() {
}



/* ---------------------------------------------------------------------------*/
void CreationFromFile::create() {

  std::ifstream is(filename.c_str());
  std::string line;

  if (is.is_open() == false) {
    std::cerr << "cannot open file " << filename << std::endl;
    throw;
  }

  UInt nb_particles = 0;
  
  while (is.good()) {
    getline(is, line);

    if (line[0] == '#' || line.size() == 0)
      continue;
    
    std::stringstream sstr(line);
    
    Real radius;
    Vector center;
    Vector force;
    Vector velocity;
    
    sstr >> center;
    sstr >> radius;
    sstr >> force;
    sstr >> velocity;
      
    auto shape = ShapeFactoryInterface::getInstance().createShape();
    shape->create(radius, center);

    global_shapes.push_back(std::move(shape));
    global_forces.push_back(force);
    global_velocity.push_back(velocity);

    nb_particles++;
  }

  auto in_descending = [](const std::unique_ptr<Shape> & s1,
			  const std::unique_ptr<Shape> & s2) {
    return s1->getRadius() > s2->getRadius(); 
  };
  
  std::sort(global_shapes.begin(), global_shapes.end(), in_descending);
  
  is.close();
}

/* -------------------------------------------------------------------------- */

void CreationFromFile::create(AggregateParticles & particles) {

  for (UInt i = 0; i < global_shapes.size(); ++i) {
    
    Real   mass;
    Real   energy = 0.0;
    Vector force = global_forces[i];
    Vector velocity = global_velocity[i];

    auto shape = std::move(global_shapes[i]);
    auto radius = shape->getRadius();
    mass = shape->getVolume();

    particles.push(std::move(shape), std::move(force), std::move(velocity), std::move(mass), std::move(energy));
  }
}

/* -------------------------------------------------------------------------- */

//void CreationFromFile::distribute() {


//}

}
