#ifndef __CREATION_FULLER_ALGORITHM__HH__
#define __CREATION_FULLER_ALGORITHM__HH__

/* -------------------------------------------------------------------------- */
#include <algorithm>
/* -------------------------------------------------------------------------- */
#include "creation_algorithm.hh"
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */
namespace xpace {
/* -------------------------------------------------------------------------- */


/// Class to create particles based on Fuller method
class CreationFullerAlgorithm
  : public CreationAlgorithm {

public:

  CreationFullerAlgorithm();

  CreationFullerAlgorithm(ArgMap);

  ~CreationFullerAlgorithm() = default; 

public:
  /// 
  void create() override;
  ///
  void create(AggregateParticles &) override;
    
public:

  XPACE_ACCESSOR(packing   , Real,              PackingDensity);  
  XPACE_ACCESSOR(diameters , std::vector<Real>, Diameters);
  
private:

  /// fuller curve implementation
  Real fuller(Real &, Real &);
  /// computes the segment volume between 2 diameters
  Real getSegmentVolume(Real &, Real &);
  ///
  Real getParticleVolume(Real &);
  ///
  void computeCenter(Vector &, Real &, Vector &, Vector &);
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */

private:
  
  /// packing density of sample
  Real packing;
  /// volume of the sample
  Real volume;
  /// density of aggregate
  Real density;
  /// range of diameters 
  std::vector<Real> diameters;

};

/* -------------------------------------------------------------------------- */
}
/* -------------------------------------------------------------------------- */

/* -------------------------------------------------------------------------- */


#endif
