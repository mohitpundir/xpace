#include "take_place_algorithm.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* ---------------------------------------------------------------------------*/
void TakePlaceAlgorithm::evolve(AggregateParticles & particles, Dumper & dumper) {
  
  UInt start = 0;
  UInt end = particles.size();
  this->evolve(particles, dumper, start, end);
    
}

/* -------------------------------------------------------------------------- */
bool TakePlaceAlgorithm::evolve(AggregateParticles & particles, Dumper & dumper,
				UInt & start, UInt & end) {

  EvolutionAlgorithm::particleLoop(particles, start, end, [&](UInt & p1) {

      std::cerr << "Placing particle : " << p1 << std::endl;
      
      UInt iter = 0;
      while (iter < this->nsteps) {
	bool intersect = false;
	
	for (UInt p2 = 0; p2 < p1; ++p2) {
	  intersect = particles.getShape(p1)->intersect(*particles.getShape(p2), this->gamma);
	  
	  if (intersect) {
	    this->rotate(particles, p1, p2);
	    break;
	  }
	}

	if (!intersect) {
	  break; 
	}
	
	iter++;
      }

      if (iter >= this->nsteps) {
	std::cerr << "Did not converge : " << iter << std::endl;
      }
    });

}


/* -------------------------------------------------------------------------- */
void TakePlaceAlgorithm::evolvePerStep(AggregateParticles & particles, UInt & start, UInt & end) {


}


/* ---------------------------------------------------------------------------*/
template<typename T>
void TakePlaceAlgorithm::rotate(T & particles, UInt & p1, UInt & p2) {
  
  Real min_dist = particles.getShape(p1)->getRadius()
                + particles.getShape(p2)->getRadius()
                + this->epsilon;

  Real offset   = (1.0 + this->gamma) * particles.getShape(p1)->getRadius();
  
  Real minimum[] = {-this->dimensions[0]/2 + offset,
		    -this->dimensions[1]/2 + offset,
		    -this->dimensions[2]/2 + offset};
  Real maximum[] = { this->dimensions[0]/2 - offset,
		     this->dimensions[1]/2 - offset,
		     this->dimensions[2]/2 - offset};

  std::random_device random_seed;
  std::mt19937 gen(random_seed());
  
  UInt iter = 0;
  while (iter < this->nsteps) {
    
    std::uniform_real_distribution<> distribution_theta(0, 2*M_PI);
    std::uniform_real_distribution<> distribution_phi  (0,   M_PI);

    Real theta = distribution_theta(gen);
    Real phi   = distribution_phi(gen);

    Vector vector;
    
    switch (spatial_dimension) {
    case 2: {
      vector[0] = min_dist * cos(theta);
      vector[1] = min_dist * sin(theta);
      vector[2] = 0.0;
      break;
    }
    case 3: {
      vector[0] = min_dist * cos(theta) * sin(phi);
      vector[1] = min_dist * sin(theta) * cos(phi);
      vector[2] = min_dist * cos(phi);
      break;
    }
    default:
      break;
    }
   
    Vector new_center;
    new_center = particles.getShape(p1)->getCenter() + vector;

    // check whether new center is within the allowed space
    UInt count = 0.;
    for (UInt i = 0; i < spatial_dimension; ++i) {
      if (new_center[i] >= minimum[i] && new_center[i] <= maximum[i]) {
	count++;
      }
    }

    if (count == spatial_dimension) {
      particles.getShape(p1)->translate(vector);
      break;
    }

    iter++;
  } 
}


}
/* -------------------------------------------------------------------------- */
