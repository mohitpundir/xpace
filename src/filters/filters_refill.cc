#include "filters_refill.hh"
#include "sphere.hh"
/* -------------------------------------------------------------------------- */
#include <list>
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
FiltersRefill::FiltersRefill(Vector & origin, Vector & dimensions, UInt & nb_grid)
  : Filters(dimensions), origin(origin), nb_grid(nb_grid) {

}

/* -------------------------------------------------------------------------- */
void FiltersRefill::apply(AggregateParticles & particles) {

  auto size = particles.size();

  Real x, y, z;
  Real i, j, k;
  Vector grid = this->dimensions/this->nb_grid;

  auto get_point = [&](Vector & point, std::tuple<UInt, UInt, UInt> index) {
    std::tie(i, j, k) = index;
    x = i * grid[0] + this->origin[0] + grid[0]/2.;
    y = j * grid[1] + this->origin[1] + grid[1]/2.;
    z = k * grid[2] + this->origin[2] + grid[2]/2.;
    point[0] = x;
    point[1] = y;
    point[2] = z;
  };

  // Creating grid points not contained inside the particles
  
  Vector point;
  std::vector<Vector> all_points;

  for (UInt i = 0; i < nb_grid; ++i) {
    for (UInt j = 0; j < nb_grid; ++j) {
      for (UInt k = 0; k < nb_grid; ++k) {

	get_point(point, std::make_tuple(i,j,k));
	all_points.push_back(point);
      }
    }
  }
  
  std::vector<Vector> points;
  std::list<UInt> grid_index;

  Real tolerance = 1.0;
  
  Sphere sphere;
  for (i = 0; i < all_points.size(); i++) {
    bool is_contained = false;

    
    sphere.create(tolerance, all_points[i]);
    for (UInt index = 0; index < size; ++index) {
      //if (particles.getShape(index)->contains(all_points[i], tolerance)) {
      if (particles.getShape(index)->intersect(sphere, this->dimensions)) {
	is_contained = true;
	break;
      }
    }
    if (!is_contained) {
      grid_index.push_back(i);
      //points.push_back(all_points[i]);
    }  
  }

  //all_points.clear();

  std::cerr << grid_index.size() << std::endl;
  
  std::random_shuffle(points.begin(), points.end());

  // refilling operation
  std::cerr << "--- Refilling system ---- " << std::endl;

  Real min[] = {-this->dimensions[0]/2 ,
		-this->dimensions[1]/2 ,
		-this->dimensions[2]/2 };
  Real max[] = { this->dimensions[0]/2 ,
		 this->dimensions[1]/2 ,
		 this->dimensions[2]/2 };

  std::vector<UInt> outside;
  for (UInt index = 0; index < size; ++index) {
    auto & position = particles.getShape(index)->getCenter();
       
    bool is_outside = false;
    for (UInt d = 0; d < 3; ++d) {
      if (position[d] < min[d] || position[d] > max[d]) {
	outside.push_back(index);
	break;
      }
    }
  }


  std::vector<UInt> outside_sphere;
  for (auto index : outside) {
    auto & position = particles.getShape(index)->getCenter();
       
    std::cerr << "Refilling particle : " << index << std::endl; 

    for (auto j : grid_index) {
      
      position = all_points[j];
      
      bool is_valid = true;

      for (auto outside_index : outside_sphere) {
	bool intersects = particles.getShape(index)->intersect(*particles.getShape(outside_index), this->dimensions);
	if (intersects) {
	  is_valid = false;
	  break;
	}
      }

      if (is_valid) {
	particles.getShape(index)->computePeriodicCenters(this->dimensions);
	grid_index.remove(j);
	std::cerr << grid_index.size() << std::endl;
	outside_sphere.push_back(index);
	break;
      }
    }
  }
}

}
