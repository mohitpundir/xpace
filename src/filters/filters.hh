#ifndef __FILTERS__HH__
#define __FILTERS__HH__
/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "vector.hh"
#include "particles.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class Filters {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destrcutors                                                 */
  /* ------------------------------------------------------------------------ */
public:

  Filters() = default;

  Filters(Vector & dimensions) : dimensions(dimensions) {}
  
  virtual ~Filters() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  virtual void apply(AggregateParticles &) = 0;

  XPACE_ACCESSOR(dimensions, Vector, Dimensions);
  
protected:

  Vector dimensions;
  
};


}


#endif
