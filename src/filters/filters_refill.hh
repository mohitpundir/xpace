#ifndef __FILTERS_REFILL__HH__
#define __FILTERS_REFILL__HH__
/* -------------------------------------------------------------------------- */
#include "filters.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class FiltersRefill
  :public Filters {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  FiltersRefill(Vector & origin, Vector & dimensions, UInt & nb_grid);
  
  ~FiltersRefill() = default;
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  void apply(AggregateParticles &) override;

private:

  Vector origin;

  UInt nb_grid;
};



}

#endif
