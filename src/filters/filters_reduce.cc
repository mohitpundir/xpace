#include "filters_reduce.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


/* -------------------------------------------------------------------------- */
FiltersReduce::FiltersReduce()
  : Filters() {
  
}


/* -------------------------------------------------------------------------- */
void FiltersReduce::apply(AggregateParticles & particles) {
  auto size = particles.size();

  for (UInt p1 = 0; p1 < size; ++p1) {

    for (UInt p2 = p1 + 1; p2 < size; ++p2) {
      if (p1 == p2 ) continue;

      Vector v_r;
      v_r = particles.getShape(p1)->getCenter()
	- particles.getShape(p2)->getCenter();

      auto r_2 = v_r.squaredNorm();
      auto r = std::sqrt(r_2);

      Real penetration = particles.getShape(p1)->getRadius()
	               + particles.getShape(p2)->getRadius() - r;

      if (penetration > 0) {
	if ( penetration < particles.getShape(p1)->getRadius() and 
	    particles.getShape(p1)->getRadius() <= particles.getShape(p2)->getRadius()) {
	  particles.getShape(p1)->getRadius() -= penetration;
	}
	else if ( penetration < particles.getShape(p2)->getRadius() and
		  particles.getShape(p2)->getRadius() <= particles.getShape(p1)->getRadius()){
	  particles.getShape(p2)->getRadius() -= penetration;
	}
      }
      
    }
  }
}


}
