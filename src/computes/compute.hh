#ifndef __COMPUTE__HH__
#define __COMPUTE__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "vector.hh"
#include "particles.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

//! Base class for all compute
class Compute {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  //! Virtual destructor needed because we have subclasses
  virtual ~Compute() = default;
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  /// Compute is pure virtual
  virtual void compute(AggregateParticles &, UInt , UInt ) = 0;
  
  /// Computes interaction between 2 particles
  virtual void computeInteraction(AggregateParticles& , UInt &, UInt &) = 0;
  
};

}

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE__HH__
