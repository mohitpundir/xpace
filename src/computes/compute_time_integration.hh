#ifndef __COMPUTE_TIME_INTEGRATION__HH__
#define __COMPUTE_TIME_INTEGRATION__HH__

/* -------------------------------------------------------------------------- */
#include "xpace.hh"
#include "vector.hh"
#include "particles.hh"
/* -------------------------------------------------------------------------- */


namespace xpace {


class ComputeTimeIntegration {
  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeTimeIntegration() = default;

  ComputeTimeIntegration(Real &) ;

  ~ComputeTimeIntegration() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  template<Integration quantity>
  void integrate(AggregateParticles&, UInt, UInt);
 
public:
  /// velocity verlet integration part 1
  /// update of position at t + delta t
  inline void integratePosition(AggregateParticles & particles, UInt & start, UInt & end, Real & dt) {

    Vector increment(0., 0., 0.);
    for (UInt index = start; index < end; ++index) {

      dt = 1.0/particles.getShape(index)->getRadius();
    
      particles.getVelocity(index) += (particles.getForce(index)
				    /  particles.getMass(index)) * (0.5 * dt);

      increment = particles.getVelocity(index) * dt;

      if (increment.norm() > particles.getShape(index)->getRadius()) {
	Vector direction = particles.getVelocity(index)
	                 / particles.getVelocity(index).norm();
	increment = direction * particles.getShape(index)->getRadius();
      }
	
      particles.getShape(index)->translate(increment);  
    }
  }

  /// velocity verlet integration part 2
  /// update of velocity at t + delta t
  inline void integrateVelocity(AggregateParticles & particles, UInt & start, UInt & end, Real & dt) {

    for (UInt index = start; index < end; ++index) {

      dt = 1.0/particles.getShape(index)->getRadius();

      particles.getVelocity(index) += (particles.getForce(index)
				    /  particles.getMass(index)) * (0.5 * dt);
    }
  }

  /* ------------------------------------------------------------------------ */
  /* Data Accessors                                                           */
  /* ------------------------------------------------------------------------ */
public:
  XPACE_ACCESSOR(dt,     Real,              Time);
  
private:

  Real dt;
};


}




#endif
