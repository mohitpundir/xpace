#ifndef __COMPUTE_NEWTON__HH__
#define __COMPUTE_NEWTON__HH__
/* -------------------------------------------------------------------------- */
#include "compute.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


//! Compute interaction using newtonian forces
class ComputeNewton : public Compute {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeNewton(Real &);

  ~ComputeNewton() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  void compute(AggregateParticles &, UInt , UInt ) override;
  ///
  void computeInteraction(AggregateParticles &, UInt &, UInt &) override;
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
protected:

  Real penalty;

};

}


#endif
