#ifndef __COMPUTE_BOUNDARY__HH__
#define __COMPUTE_BOUNDARY__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

//! Compute interaction with simulation box
class ComputeBoundary : public Compute {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeBoundary() = default;

  ComputeBoundary(UInt & , Vector &, Boundary);

  ~ComputeBoundary() = default;
  
  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  void compute(AggregateParticles &, UInt , UInt ) override;
  ///
  void computeInteraction(AggregateParticles&, UInt&, UInt&) override;

  /* ------------------------------------------------------------------------ */
  /* Data Accessors                                                           */
  /* ------------------------------------------------------------------------ */
  XPACE_ACCESSOR(spatial_dimension,   UInt,   SpatialDimension); 
  XPACE_ACCESSOR(dimensions, Vector,   Dimensions);
  XPACE_ACCESSOR(type,       Boundary, Type);

private:
  /// 
  template<typename T>
  void computeInteraction(T&, UInt &);
  ///
  template<typename T>
  void computeInteractionPeriodic(T&, UInt &);

  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
protected:
  
  UInt spatial_dimension;

  /// Dimensions of the system
  Vector dimensions;

  /// Type of boundary condition
  Boundary type;
};


}


#endif
