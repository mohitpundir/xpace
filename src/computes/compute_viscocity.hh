#ifndef __COMPUTE_VISCOCITY__HH__
#define __COMPUTE_VISCOCITY__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class ComputeViscocity :
  public Compute {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeViscocity() = default;

  ComputeViscocity(Real &);

  ~ComputeViscocity() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///
  void compute(AggregateParticles&, UInt , UInt ) override;
  ///
  void computeInteraction(AggregateParticles&, UInt&, UInt&) override;

private:
  ///
  template<typename T>
  void computeInteraction(T&, UInt &);

  /* ------------------------------------------------------------------------ */
  /* DATA ACCESSOR                                                            */
  /* ------------------------------------------------------------------------ */
public:
  XPACE_ACCESSOR(eta,   Real,   Eta);
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
private:
  
  Real eta;

};

}

#endif
