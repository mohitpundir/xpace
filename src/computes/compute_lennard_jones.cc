#include "compute_lennard_jones.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
ComputeLennardJones::ComputeLennardJones(Real &gamma, Vector &dimensions,
                                         Boundary type)
    : Compute(), gamma(gamma), dimensions(dimensions) {

  this->type = type;
}

/* -------------------------------------------------------------------------- */
void ComputeLennardJones::compute(AggregateParticles &particles, UInt start,
                                  UInt end) {

  for (UInt p1 = start; p1 < end; ++p1) {
    std::cerr << "particle : " << p1 << std::endl;
    for (UInt p2 = p1 + 1; p2 < end; ++p2) {
      this->computeInteraction(particles, p1, p2);
    }
  }
}

/* -------------------------------------------------------------------------- */
void ComputeLennardJones::computeInteraction(AggregateParticles &particles,
                                             UInt &p1, UInt &p2) {

  Vector distance =
      particles.getShape(p1)->getCenter() - particles.getShape(p2)->getCenter();

  switch (this->type) {
  case _periodic: {
    for (UInt i = 0; i < 3; ++i) {
      distance[i] = distance[i] - this->dimensions[i] *
                                      modulo(distance[i], this->dimensions[i]) *
                                      sign(distance[i]);
				      }
    break;
  }
  default:
    break;
  }

  this->gamma = particles.getShape(p1)->getRadius() + particles.getShape(p2)->getRadius();

  Vector force = this->computeForce(distance);
  particles.getForce(p2) -= force/particles.getShape(p2)->getRadius();
  particles.getForce(p1) += force/particles.getShape(p1)->getRadius();

  //Real energy = this->computePotential(distance);
  //particles.getEnergy(p2) += energy;
  //particles.getEnergy(p1) += energy;
}

/* -------------------------------------------------------------------------- */
/*Vector ComputeLennardJones::computeForce(Vector &distance) {

  Real r_2 = distance.squaredNorm();

  Real r_cut = this->dimensions[0] / 2.;
  Real r_cut_2 = r_cut * r_cut;

  Vector force = distance;

  auto lennard_force = [&](Real &distance) {
    Real beta = this->gamma / distance;
    //return 12.0 * (std::pow(beta, 12.0)) * (1 - 1. / std::pow(beta, 6.0)) /
    //       distance;
    return 24.0 * (std::pow(beta, 12.0)) * (2. - 1. / std::pow(beta, 6.0)) /
           distance;
  };

  if (r_2 <= r_cut_2) {

    Real r_ij = std::sqrt(r_2);
    force /= r_ij;
    force *= (lennard_force(r_ij) - lennard_force(r_cut));
  } else {
    force *= 0.0;
  }
  
  return force;
  }*/

/* -------------------------------------------------------------------------- */
Real ComputeLennardJones::computePotential(Vector &distance) {

  Real r_2 = distance.squaredNorm();

  Real r_cut = this->dimensions[0] / 2.;
  Real r_cut_2 = r_cut * r_cut;

  auto potential_energy = [&](Real &distance) {
    Real beta = this->gamma / distance;
    return 2.0 * (0.5 * (std::pow(beta, 12.0)) - (std::pow(beta, 6.0)));
  };

  Real energy = 0.0;

  if (r_2 <= r_cut_2) {
    Real r_ij = std::sqrt(r_2);
    energy = potential_energy(r_ij) - potential_energy(r_cut);
  }

  return energy;
}

/* -------------------------------------------------------------------------- */

}
