#include "compute_boundary.hh"
#include "vector.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
ComputeBoundary::ComputeBoundary(UInt & spatial_dimension,  Vector & dimensions, Boundary type)
  : Compute(), dimensions(dimensions), spatial_dimension(spatial_dimension) {
  this->type = type;
}


/* -------------------------------------------------------------------------- */
void ComputeBoundary::compute(AggregateParticles & particles, UInt start, UInt end) {
  
  for (UInt p1 = start; p1 < end; ++p1) {
    this->computeInteraction(particles, p1, p1);    
  }
}

/* -------------------------------------------------------------------------- */
void ComputeBoundary::computeInteraction(AggregateParticles & particles, UInt & p1, UInt & p2) {

  switch (this->type) {
  case _periodic: {
    this->computeInteractionPeriodic(particles, p1);
    break;
  }
  default:
    this->computeInteraction(particles, p1);
    
    break;
  }
}


/* -------------------------------------------------------------------------- */
template<typename T>
void ComputeBoundary::computeInteraction(T & particles, UInt & p1) {
  
  Real min[] = {-this->dimensions[0]/2 ,
		-this->dimensions[1]/2 ,
		-this->dimensions[2]/2 };
  Real max[] = { this->dimensions[0]/2 ,
		 this->dimensions[1]/2 ,
		 this->dimensions[2]/2 };

  auto & position = particles.getShape(p1)->getCenter();
  auto & radius = particles.getShape(p1)->getRadius();

  /*Vector origin(-50., -50., -50.);
  
  Real x_min = origin[0] + radius;
  Real x_max = origin[0] + dimensions[0] - radius;
  
  Real y_min = origin[1] + radius;
  Real y_max = origin[1] + dimensions[1] - radius;
      
  Real z_min = origin[2] + radius;
  Real z_max = origin[2] + dimensions[2] - radius;

  std::uniform_real_distribution<> distribution_x(x_min, x_max);
  std::uniform_real_distribution<> distribution_y(y_min, y_max);
  std::uniform_real_distribution<> distribution_z(z_min, z_max);*/
  
  //std::random_device random_seed;
  //std::mt19937 gen(random_seed());

  for (UInt i=0 ; i < this->spatial_dimension; ++i) {
      if (position[i] - radius <= min[i] || position[i] + radius >= max[i]) {
      	particles.getVelocity(p1)[i] *= 0.0;
	particles.getForce(p1)[i] *= -1.0;
      }
  }

  
  /*switch (this->type) {
  case _soft_aperiodic: {
    for (UInt i=0 ; i < spatial_dimension; ++i) {
      if (position[i] - radius <= min[i] || position[i] + radius >= max[i]) {
      	particles.getVelocity(p1)[i] *= 0.0;
	particles.getForce(p1)[i] *= -1.0;

	if (position[i] - radius <= min[i]) {
	  position[i] = min[i] + radius;
	}

	if (position[i] + radius >= max[i]) {
	  position[i] = max[i] - radius;
	}

      }
    }
    break;
  }
  default:
    for (UInt i=0 ; i < spatial_dimension; ++i) {
      if (position[i] - radius < min[i] || position[i] + radius > max[i]) {
	position[0] = distribution_x(gen);
	position[1] = distribution_y(gen);
	position[2] = distribution_z(gen);
	break;
      }
      }
    break;
  }*/

}


/* -------------------------------------------------------------------------- */
template<typename T>
void ComputeBoundary::computeInteractionPeriodic(T & particles, UInt & p1) {
  
  auto & position = particles.getShape(p1)->getCenter();
  
  Vector min(-this->dimensions[0]/2 ,
             -this->dimensions[1]/2 ,
	     -this->dimensions[2]/2 );
  Vector max( this->dimensions[0]/2,
	      this->dimensions[1]/2,
	      this->dimensions[2]/2);

  for (UInt i=0 ; i < 3; ++i) {
    if (position[i] < min[i]) {
      Real factor = ceil(abs(abs(position[i]) - abs(max[i]))/dimensions[i]);
      position[i] += dimensions[i] * factor;
    }

    else if (position[i] > max[i]) {
      Real factor  = ceil(abs(abs(position[i]) - abs(min[i]))/dimensions[i]);
      position[i] -= dimensions[i] * factor;
    }
  }

}


}
