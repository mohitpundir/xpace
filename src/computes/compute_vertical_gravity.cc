#include "compute_vertical_gravity.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
ComputeVerticalGravity::ComputeVerticalGravity(Vector & direction, Real & gravity)
  : Compute(), direction(direction), gravity(gravity) {

}

/* -------------------------------------------------------------------------- */
void ComputeVerticalGravity::compute(AggregateParticles & particles, UInt start, UInt end) {
 
  for (UInt p1 = start; p1 < end; ++p1) {
    this->computeInteraction(particles, p1);
  }

}


/* -------------------------------------------------------------------------- */
void ComputeVerticalGravity::computeInteraction(AggregateParticles & particles, UInt & p1, UInt & p2) {
  
  this->computeInteraction(particles, p1);
}

/* -------------------------------------------------------------------------- */
template<typename T>
void ComputeVerticalGravity::computeInteraction(T & particles, UInt & p1) {
    
  auto & force = particles.getForce(p1);
  for (UInt i = 0; i < 3; ++i) {
    force[i] += gravity * particles.getMass(p1) * direction[i];
  }
  
}

}
