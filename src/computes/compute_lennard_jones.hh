#ifndef __COMPUTE_LENNARD_JONES__HH__
#define __COMPUTE_LENNARD_JONES__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "functional"
/* -------------------------------------------------------------------------- */

namespace xpace {

//! Compute interaction using lennard jones potential
class ComputeLennardJones : public Compute {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeLennardJones(){
      /*computeGamma = [&](*this, Real r1, Real r2) {
        this->gamma = r1 + r2;
      };*/
  };

  ComputeLennardJones(Real &, Vector &, Boundary);

  ~ComputeLennardJones() = default;

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  ///  computes forces and energy for a system of particles
  void compute(AggregateParticles &, UInt, UInt) override;

  /// computes the interaction forces between 2 particles
  void computeInteraction(AggregateParticles &, UInt &, UInt &) override;


public:
  /// computes the potential energy for a given distance
  Real computePotential(Vector &);

  /// computes the interaction force for a given distance
  inline Vector computeForce(Vector & distance) {

    Real r_2 = distance.squaredNorm();

    Real r_cut = this->dimensions[0] / 2.;
    Real r_cut_2 = r_cut * r_cut;

    Vector force = distance;

    auto lennard_force = [&](Real &distance) {
      Real beta = this->gamma / distance;
       return 12.0 * (std::pow(beta, 12.0)) * (1 - 1. / std::pow(beta, 6.0)) /
             distance;
      //return 24.0 * (std::pow(beta, 12.0)) * (2. - 1. / std::pow(beta, 6.0)) /
      //       distance;
    };

    if (r_2 <= r_cut_2) {

      Real r_ij = std::sqrt(r_2);
      force /= r_ij;
      force *= (lennard_force(r_ij) - lennard_force(r_cut));
    } else {
      force *= 0.0;
    }

    return force;
  }

  /// modulo operator for periodic boundary conditions
  inline int modulo(Real &dx, Real &length) { return round(abs(dx / length)); }

  /// sign operator for periodic boundary conditions
  inline int sign(Real &dx) {
    if (dx < 0) {
      return -1;
    } else if (dx >= 0) {
      return 1;
    }
  }

  /* ------------------------------------------------------------------------ */
  /* DATA ACCESSOR                                                            */
  /* ------------------------------------------------------------------------ */
public:
  XPACE_ACCESSOR(gamma, Real, Gamma);
  XPACE_ACCESSOR(dimensions, Vector, Dimensions);
  XPACE_ACCESSOR(type, Boundary, Type);

  // Members
private:
  /// Neutral distance betwen 2 particles
  Real gamma;

  ///
  std::function<Real(Real, Real)> computeGamma;

  /// Dimensions of the system
  Vector dimensions;

  /// Type of boundary condition
  Boundary type;
};

}

#endif
