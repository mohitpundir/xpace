#include "compute_viscocity.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {


/* -------------------------------------------------------------------------- */
ComputeViscocity::ComputeViscocity(Real & eta)
  : Compute(), eta(eta) {
}

/* -------------------------------------------------------------------------- */
void ComputeViscocity::compute(AggregateParticles & particles, UInt start, UInt end) {

  for (UInt p1 = start; p1 < end; ++p1) {
    this->computeInteraction(particles, p1);
  }

}


/* -------------------------------------------------------------------------- */
void ComputeViscocity::computeInteraction(AggregateParticles & particles, UInt & p1, UInt & p2) {
  
  this->computeInteraction(particles, p1);
}

/* -------------------------------------------------------------------------- */
template<typename T>
void ComputeViscocity::computeInteraction(T & particles, UInt & p1) {
  
  Real beta = this->eta * 6.0;
  beta *= particles.getShape(p1)->getRadius();
  particles.getForce(p1) -= particles.getVelocity(p1) * beta;
}



}
