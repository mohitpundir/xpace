#ifndef __COMPUTE_VERTICAL_VISCOCITY__HH__
#define __COMPUTE_VERTICAL_VISCOCITY__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

class ComputeVerticalGravity :
  public Compute {

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  ComputeVerticalGravity(Vector &, Real &);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  /// computes force interaction for a given system
  void compute(AggregateParticles&, UInt , UInt ) override;

  /// computes force interaction between 2 particles
  void computeInteraction(AggregateParticles&, UInt &, UInt &) override;

private:
  ///
  template<typename T>
  void computeInteraction(T &, UInt &);

  /* ------------------------------------------------------------------------ */
  /* DATA ACCESSOR                                                            */
  /* ------------------------------------------------------------------------ */
public:
  XPACE_ACCESSOR(gravity,    Real,     Gravity);
  XPACE_ACCESSOR(direction,  Vector,   Direction);
  
  /* ------------------------------------------------------------------------ */
  /* Members                                                                  */
  /* ------------------------------------------------------------------------ */
private:
  /// Gravity value
  Real gravity;

  /// Direction vector  
  Vector direction;
};


}

#endif
