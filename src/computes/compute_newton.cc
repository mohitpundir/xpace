#include "compute_newton.hh"
/* -------------------------------------------------------------------------- */

namespace xpace {

/* -------------------------------------------------------------------------- */
ComputeNewton::ComputeNewton(Real & penalty)
  : Compute() {

  this->penalty = penalty;
}

/* -------------------------------------------------------------------------- */
void ComputeNewton::compute(AggregateParticles & particles, UInt start, UInt end) {
  
  // first of all reset forces to zero
  for (UInt i = start; i < end; ++i)
    particles.getForce(i) *= 0;

  for (UInt p1 = start; p1 < end; ++p1) {
    for (UInt p2 = p1 + 1; p2 < end; ++p2) {
      this->computeInteraction(particles, p1, p2); 
    }
  }
  
}

/* -------------------------------------------------------------------------- */
void ComputeNewton::computeInteraction(AggregateParticles & particles, UInt & p1, UInt & p2) {

  Vector force(0., 0., 0.);
  
  Vector v_r(0., 0., 0.);

  Vector v_nr(0., 0., 0.);

  Real r_2 = 0.;
  
  Real r_ij = 0.;

  v_r = particles.getShape(p1)->getCenter()
      - particles.getShape(p2)->getCenter();
  r_2 = v_r.squaredNorm();

  r_ij = std::sqrt(r_2);

  v_nr = v_r;
  v_nr /= r_ij;
      
  Real penetration = particles.getShape(p1)->getRadius()
                   + particles.getShape(p2)->getRadius() - r_ij;

  if (penetration > 0) {
    force = v_nr;
    force *= penalty * penetration;

    particles.getForce(p1) += force;
    particles.getForce(p2) -= force;
  }
  
  
}


}
