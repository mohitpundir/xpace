#include "point.hh"
#include "xpace.hh"

#include <complex>
#include <gtest/gtest.h>

using namespace xpace;

const UInt dim = 3;
const Real tolerance = 1e-10;

TEST(PointTest, Distance) {

  Point<dim> p1(1., 2., 3.);
  Point<dim> p2(1., 2., 3.);
  Real distance = p1.distance(p2);

  ASSERT_NEAR(distance, 0., tolerance);
}

TEST(PointTest, Norm) {

  Point<dim> p1(1., 1., 1.);
  Real norm       = p1.norm();
  Real analytical = sqrt(3.);

  ASSERT_NEAR(norm, analytical, tolerance);
}
