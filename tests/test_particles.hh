#include "particles.hh"
#include "sphere.hh"
#include <gtest/gtest.h>

using namespace xpace;

/* -------------------------------------------------------------------------- */
class Particle : public ::testing::Test {
protected:
  void SetUp() override {
    
    Real radius = 1.0;
    Real   mass;
    Vector force(    0, 0, 0);
    Vector center(   0, 0, 0);
    Vector velocity( 0, 0, 0);

    Shape * shape = new Sphere;
    shape->create(radius, center);
    mass = shape->getVolume();

    particles.push_back(shape, force, velocity, mass);
  }

  AggregateParticles particles;
};


TEST_F(Particle, Translate) {
  Vector increment(1.0, 1.0, 1.0);
  particles.getShape(0)->translate(increment);

  auto center = particles.getShape(0)->getCenter();
  ASSERT_EQ(center[0], 1.0);
  ASSERT_EQ(center[1], 1.0);
  ASSERT_EQ(center[2], 1.0);
}


TEST_F(Particle, UpdateVelocity) {
  Vector increment(1.0, 1.0, 1.0);
  particles.getVelocity(0) += increment;

  auto center = particles.getVelocity(0);
  ASSERT_EQ(center[0], 1.0);
  ASSERT_EQ(center[1], 1.0);
  ASSERT_EQ(center[2], 1.0);
}
