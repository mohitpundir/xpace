#include "xpace.hh"
#include "vector.hh"
#include "plane.hh"
#include <gtest/gtest.h>

using namespace xpace;

const UInt dim = 3;

TEST(PlaneTest, PointAbove) {

  Plane plane;
  const Real height  = 0.0;
  const Real degree  = 0.0;
  plane.create<_z>(degree, height);

  Vector point(0., 0., 2.);

  ASSERT_TRUE(plane.above(point));
}
