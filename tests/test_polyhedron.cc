#include "xpace.hh"
#include "vector.hh"
#include "plane.hh"
#include "polyhedron.hh"
#include "dumper.hh"
#include "surface_generator.hh"
#include <gtest/gtest.h>

using namespace xpace;

const UInt dim = 3;

/* -------------------------------------------------------------------------- */
class PolyhedronTest : public ::testing::Test {
protected:
  void SetUp() override {
    
    Vector center(0., 0., 0.);
    std::vector<Real> points = {-0.5, -0.5, -0.5,
			 	 0.5, -0.5, -0.5,
				 0.5,  0.5, -0.5,
				-0.5,  0.5, -0.5,
				-0.5, -0.5,  0.5,
				 0.5, -0.5,  0.5,
				 0.5,  0.5,  0.5,
				-0.5,  0.5,  0.5};
    Real radius = 1.0;
    poly.setPoints(points);
    poly.create(radius, center);
  }

  Polyhedron poly;
};

/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, IntersectsBoundingBox) {

  Polyhedron cube;

  Real radius = 1.0;
  Vector center(0.5, 0., 0.);
  std::vector<Real> points = { 0., -0.5, -0.5,
			       1., -0.5, -0.5,
			       1.,  0.5, -0.5,
			       0.,  0.5, -0.5,
			       0., -0.5,  0.5,
			       1., -0.5,  0.5,
			       1.,  0.5,  0.5,
			       0.,  0.5,  0.5};
  
  cube.setPoints(points);
  cube.create(radius, center);
  
  ASSERT_TRUE(poly.intersectsBoundingBox(cube));
}


/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, DoNotIntersectBoundingBox) {

  Polyhedron cube;

  Real radius = 1.0;
  Vector center(0.5, 0., 0.);
  std::vector<Real> points = { 2., -0.5, -0.5,
			       3., -0.5, -0.5,
			       3.,  0.5, -0.5,
			       2.,  0.5, -0.5,
			       2., -0.5,  0.5,
			       3., -0.5,  0.5,
			       3.,  0.5,  0.5,
			       2.,  0.5,  0.5};
  
  cube.setPoints(points);
  cube.create(radius, center);
  
  ASSERT_TRUE(!poly.intersectsBoundingBox(cube));
}

/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, InsideFacet) {

  /*auto & qhull    = poly.getQhull();
  auto   facets   = qhull->facetList().toStdVector();
  
  Vector query(0., 0.25, 0.5);

  orgQhull::QhullFacet f;
  
  for (auto & facet : facets) {
    
    if (poly.insideFacet(facet, query)) {
      f = facet;
    }
  }

  auto normal = f.hyperplane().coordinates();
  ASSERT_EQ(normal[2], 1);*/  
}


/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, QhullReComputation) {
  /*auto & qhull    = poly.getQhull();
  auto   facets   = qhull->facetList().toStdVector();

  const Vector vector(3., 3., 3.);

  auto facet  = facets[0];
  auto offset = facet.hyperplane().offset();
  
  poly.translate(vector);
  poly.reCompute();

  auto facet_translate  = facets[0];


  auto offset_translate = facet_translate.hyperplane().offset();


  
  ASSERT_EQ(offset, offset_translate);*/
}

/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, IntersectPlane) {

  Plane plane;
  const Real height  = 0.0;
  const Real degree  = 0.0;
  plane.create<_z>(degree, height);

  ASSERT_TRUE(poly.intersect(plane));
}


/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, DonNotIntersectPlane) {

  Plane plane;
  const Real height  = 10.0;
  const Real degree  = 0.0;
  plane.create<_z>(degree, height);

  ASSERT_TRUE(!poly.intersect(plane));
}

/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, TranslateMax) {

  const Vector vector(3., 0., 0.);
  poly.translate(vector);

  auto max = poly.getMax();
  
  ASSERT_EQ(max[0], 3.5);
}

/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, TranslateCenter) {

  const Vector vector(3., 0., 0.);
  poly.translate(vector);

  auto center = poly.getCenter();
  
  ASSERT_EQ(center[0], 3.0);
}


/* -------------------------------------------------------------------------- */
TEST_F(PolyhedronTest, Contains) {

  Vector point(0., 0., 0.);

  bool is_inside      = false;
  UInt index;

  std::tie(is_inside, index) = poly.contains(point);
  ASSERT_TRUE(is_inside);
}
