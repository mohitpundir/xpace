#include "grid_data.hh"
#include <gtest/gtest.h>

using namespace xpace;

/* -------------------------------------------------------------------------- */
class GridTest : public ::testing::Test {
protected:
  void SetUp() override {

    UInt size = 10;
    griddata.resize(size);
  }

  GridData griddata;
  Real min_x = 0.0;
  Real min_y = 0.0;
  Real dx = 1.0;
  Real dy = 1.0;
};

/* -------------------------------------------------------------------------- */

TEST_F(GridTest, IndexedGrid) {
  
  for (auto && grid: griddata.indexedGrid()) {
    UInt i = std::get<0>(grid);
    UInt j = std::get<1>(grid);
    std::get<2>(grid) = min_x + dx*i;
    std::get<3>(grid) = min_y + dy*j;
  }

  Vector vec;
  UInt index = 2;
  griddata.getGridPoint(index, vec);
  
  ASSERT_EQ(2.0, vec[0]);
  ASSERT_EQ(0.0, vec[1]);
}


/* -------------------------------------------------------------------------- */

TEST_F(GridTest, IndexedField) {
  
  for (auto && data: griddata.indexedField()) {
    UInt i = std::get<0>(data);
    UInt j = std::get<1>(data);
    std::get<2>(data) = min_x + dx*i;
  }

  ASSERT_EQ(5.0, griddata[5]);
}


/* -------------------------------------------------------------------------- */
TEST_F(GridTest, GridDataIterator) {
  
  for (auto && grid: griddata.indexedGrid()) {
    UInt i = std::get<0>(grid);
    UInt j = std::get<1>(grid);
    std::get<2>(grid) = min_x + dx*i;
    std::get<3>(grid) = min_y + dy*j;
  }

  
  for (auto && data: griddata.indexedField()) {
    UInt i = std::get<0>(data);
    UInt j = std::get<1>(data);
    std::get<2>(data) = min_x + dx*i;
  }

  for (auto && points: griddata) {
    auto & x = std::get<0>(points);
    auto & y = std::get<1>(points);
    auto & z = std::get<2>(points);
    ASSERT_EQ(x, z);
  }

  for (auto && points: griddata) {
    auto & x = std::get<0>(points);
    auto & y = std::get<1>(points);
    auto & z = std::get<2>(points);
    z = 678.0;
  }

  ASSERT_EQ(678.0, griddata[5]);
}
