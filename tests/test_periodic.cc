#include "particles.hh"
#include "sphere.hh"
#include <gtest/gtest.h>
#include "compute_lennard_jones.hh"
#include "dumper.hh"
#include "compute_boundary.hh"

using namespace xpace;

/* -------------------------------------------------------------------------- */
class PeriodicParticles : public ::testing::Test {
protected:
  void SetUp() override {
    Real radius = 0.05;
    Real   mass;
    Real   energy = 0.0;
    Vector force(    0, 0, 0);
    Vector velocity( 0, 0, 0);

    Real length = 1.0;
    
    Vector nx(3, 3, 1);
    
    Vector dx(length/nx[0], length/nx[1], length/nx[2]);
    Vector initial(-dx[0], -dx[1], -dx[2]);

    Vector increment;
    for (UInt i = 0; i < nx[0]; ++i) {
      for (UInt j = 0; j < nx[1]; ++j) {
	for (UInt k = 0; k < nx[2]; ++k) {

	  Vector position(0., 0., 0.);
	  
	  increment[0] = i * dx[0];
	  increment[1] = j * dx[1];
	  increment[2] = k * dx[2];
	  
	  position = initial + increment;
	  
	  auto shape = std::make_unique<Sphere>();
	  shape->create(radius, position);
	  mass = shape->getVolume();

	  particles.push(std::move(shape), std::move(force), std::move(velocity),
			 std::move(mass), std::move(energy));
	}
      }
    }
        
    
    Vector dimensions(1.0, 1.0, 1.0);
    interaction.setGamma(1.0);
    interaction.setDimensions(dimensions);
    interaction.setType(_periodic);

    boundary.setDimensions(dimensions);
    boundary.setType(_periodic);
  }

  AggregateParticles particles;
  ComputeLennardJones interaction;
  ComputeBoundary boundary;
  Dumper dump;
};

TEST_F(PeriodicParticles, Periodic_force) {
  auto size = particles.size();
  interaction.compute(particles, 0, size);
  auto force_p1 = particles.getForce(0);
  auto force_p2 = particles.getForce(4);
  auto force_p3 = particles.getForce(7);

  dump.setBaseNameToDumper("particle-periodic-force");
  dump.dumpToTxt(particles);
  
  Real tolerance = 1e-7;
  ASSERT_NEAR(force_p1[0], 0.0, tolerance);
  ASSERT_NEAR(force_p1[1], 0.0, tolerance);
  ASSERT_NEAR(force_p1[2], 0.0, tolerance);

  ASSERT_NEAR(force_p2[0], 0.0, tolerance);
  ASSERT_NEAR(force_p2[1], 0.0, tolerance);
  ASSERT_NEAR(force_p2[2], 0.0, tolerance);

  ASSERT_NEAR(force_p3[0], 0.0, tolerance);
  ASSERT_NEAR(force_p3[1], 0.0, tolerance);
  ASSERT_NEAR(force_p3[2], 0.0, tolerance);
}



TEST_F(PeriodicParticles, Periodic_boundary) {
  auto size = particles.size();

  auto center_p1 = particles.getShape(0)->getCenter();
  auto center_p2 = particles.getShape(4)->getCenter();
  auto center_p3 = particles.getShape(7)->getCenter();

  auto & new_center_p1 = particles.getShape(0)->getCenter();
  auto & new_center_p2 = particles.getShape(4)->getCenter();
  auto & new_center_p3 = particles.getShape(7)->getCenter();

  new_center_p1[0] -= 10.0;

  new_center_p2[0] += 10.0;
  new_center_p2[1] -= 10.0;
  
  new_center_p3[0] += 10.0;
  new_center_p3[1] += 10.0;

  boundary.compute(particles, 0, size);
  
  dump.setBaseNameToDumper("particle-periodic-boundary");
  dump.dumpToTxt(particles);

  Real tolerance = 1e-8;
  ASSERT_NEAR(center_p1[0] - new_center_p1[0], 0.0, tolerance);
  
  ASSERT_NEAR(center_p2[0] - new_center_p2[0], 0.0, tolerance);
  ASSERT_NEAR(center_p2[1] - new_center_p2[1], 0.0, tolerance);
  
  ASSERT_NEAR(center_p3[0] - new_center_p3[0], 0.0, tolerance);
  ASSERT_NEAR(center_p3[1] - new_center_p3[1], 0.0, tolerance);
}
