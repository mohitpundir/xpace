#include "xpace.hh"
#include "vector.hh"
#include "fft.hh"
#include <gtest/gtest.h>
/* -------------------------------------------------------------------------- */

using namespace xpace;

TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}

/* -------------------------------------------------------------------------- */

TEST(FFT, inverse_transform) {
  
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    std::get<2>(entry) = 0.;
  }

  m(1, 0) = N * N / 2;
  m(N - 1, 0) = N * N / 2;

  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;
  }

  Matrix<complex> res = FFT::itransform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (std::abs(val.real() - cos(k * i)) > 1e-10)
      std::cout << i << "," << j << " = " << val.real() << " " << cos(k * i)
                << std::endl;

    ASSERT_NEAR(val.real(), cos(k * i), 1e-14);
    ASSERT_NEAR(val.imag(), 0, 1e-14);
  }
}
