#include "compute_lennard_jones.hh"
#include "particles.hh"
#include "sphere.hh"
#include "compute_lennard_jones.hh"
#include <gtest/gtest.h>


using namespace xpace;

/* -------------------------------------------------------------------------- */
class Particle  :  public::testing::Test {

protected:
  void SetUp() override {
  }

  ComputeLennardJones interaction;
};


TEST_F(Particle, LennardJones) {

  Vector dimensions(100.0, 100.0, 100.0);

  interaction.setGamma(std::pow(2, 1./6));
  interaction.setDimensions(dimensions);

  Vector distance(1.0, 0., 0.);
  auto force = interaction.computeForce(distance);

  interaction.setGamma(std::pow(2, 1./3));
  auto energy = interaction.computePotential(distance);
  
  Real tolerance = 1e-8;
  ASSERT_NEAR(force[0], 24, tolerance);
  ASSERT_NEAR(energy,   8, tolerance);
}
