#include "surface_statistics.hh"
#include <gtest/gtest.h>

using namespace xpace;

/* -------------------------------------------------------------------------- */
class Surface : public ::testing::Test {
protected:
  void SetUp() override {

    UInt size = 512;
    surface.resize(size);

    for (auto && data: griddata.indexedField()) {
      UInt i = std::get<0>(data);
      UInt j = std::get<1>(data);
      auto & val = std::get<2>(data);
      val = 1.0;
    }
  }

  GridData surface;
};


TEST_F(Surface, RMSHeight) {

}


TEST_F(Surface, HeightDistribution) {

}


TEST_F(Surface, PowerSpectrum2D) {

}

TEST_F(Surface, PowerSpectrum1D) {


}
